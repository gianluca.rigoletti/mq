\documentclass[../mq.tex]{subfiles}
\begin{document}

\newpage
\section{Autovalori e autofunzioni della funzione d'onda}
\subsection{Postulato di espansione}
Il teorema di Fourier stabilisce che qualcunque funzione $ \waf{} $ che soddisfa le condizioni: $$\psi(0) = 0  \qquad \psi(a) = 0$$ può essere sviluppata come:
\begin{equation}
\waf{} = \sum_{n=1}^{\infty} c_n \sin\left(\frac{n\pi}{a}x\right) 
\end{equation}
Più generalmente:

\begin{equation}
\waf{} = \sum A_nu_n(x) \qquad \text{con} \quad u_n \quad \text{autofunzioni}
\end{equation}

Le $ u_n $ costituiscono una base ortonormale completa. I coefficienti $ A_n $ sono i coefficienti  di Fourier del sistema ortonormale completo e si possono calcolare come:
\begin{equation}\label{eq:coefficenti di fourier energia}
A_n = \intf dx\; u^*_n(x)\waf{}
\end{equation}
 Quindi la soluzione di Schr\"odinger completa è una sovrapposizione di onde piane con dovuti coeifficienti e la loro fase:

\begin{equation}\label{eq:espansioneH}
\waf{} = \sum A_n u_n(x)
e^{-iE_nt/\hbar}
\end{equation}

Diamo un'interpretazione fisica ai coefficienti $A_n$:
L'equazione agli autovalori dell'hamiltoniana ci dice che per ogni stato della particella vale:
\begin{equation}
Hu_n(x) = E_nu_n(x)
\end{equation}

Il valore medio dell'energia è:

\begin{align}
<H> &= \int_{0}^{a}dx\; \psi^*H\psi = \int_{0}^{a}dx\; \psi^* H \sum A_n u_n \\&= \int_{0}^{a}dx\; \psi^* \sum A_n E_n u_n = \sum A_n E_n \int_{0}^{a} dx\,\psi^*\, u_n\\
&= \sum A_n  A^{*}_{n} E_n \\&= \sum|A|^2 E_n  \; \rightarrow \;\sum_n p_n E_n
\end{align}

L'interpretazione di questa relazione è che ogni valore dello spettro dell'energia ha una probabilità di essere misurato che dipende dal modulo quadro del coefficiente dell'espansione.  Il postulato è noto anche come \emph{Postulato di espansione} ed è un postulato fondamentale della meccanica quantistica. Le implicazioni sono molteplici, ma ce n'è una in particolare: supponiamo di eseguire una misura dell'energia di un sistema e di ottenere l'autovalore $ E_{n_{0 }} $. Se rieseguo la misura dell'energia mi aspetto di ritrovare lo stesso autovalore $ E_{n_{0}} $ (non c'è indeterminazione sull'energia in questo caso). ciò significa che una volta eseguita la misura il sistema è collassato sull'autostato relativo $ u_{n_{0}}(x) $. Di fatto, la misura proietta lo stato iniziale del sistema nell'autostato $ u_{n_{0}}(x) $  dell'osservabile energia. La probabilità che collassi in uno stato piuttosto che in un altro è data appunto dalla quantità $ p_n $

\subsection{Autofunzioni dell'operatore momento}

Scriviamo l'equazione agli autovalori del momento, indicando con $ \hat{p} $ l'operatore, con $ p $ gli autovalori e con $ u_{p}(x) $ gli autostati:

\begin{equation} \label{eq:autovalori di p}
\hat{p}u_{p}(x) = pu_{p}(x)
\end{equation}

Sappiamo che 

\begin{equation}
\hat{p} = -i\hbar\pdv{x}{}{}
\end{equation}
d
Risolvendo quindi l'equazione \vref{eq:autovalori di p} otteniamo:

\begin{equation}
u_{p}(x) = ce^{ipx/\hbar}
\end{equation}

Scriviamo la condizione di ortonormalità tra due autostati $ u_{p} $ e $ u'_{p} $:

\begin{gather}
\intf sx\;u^{*}_{p}(x)u'_{p}(x) = |c|^2 \int dx;e^{-i(p-p')x/\hbar} = |c|^2\delta(p-p')2\pi\hbar \nonumber \\
c = \frac{1}{\sqrt{2\pi\hbar}}
\end{gather}

Quindi al posto del Delta di kronecker utilizzato nel caso discreto dell'energia, abbiamo il Delta di Dirac. Lo spettro delle autofunzioni dell'operatore momento è quindi uno spettro continuo e non discreto come lo spettro dell'Hamiltoniana. 

Sviluppiamo in termini di Fourier la funzione d'onda:

\begin{equation}
\waf{} = \frac{1}{2\pi\hbar}\intf dp\;\phi(p) e^{ipx/\hbar}
\end{equation}
Possiamo notare che si tratta della stessa espansione operata per l'energia in equazione \vref{eq:espansioneH}. In questo caso  $u_n(x) = e^{ipx/\hbar}$ e $A_n = \phi(p)$. Il coefficiente di ogni autofunzione non è più un valore definito $A_n$ ma una funzione di $p$ perchè lo spettro in questo caso è continuo.

Analogamente al caso \vref{eq:coefficenti di fourier energia}, ma con indici continui, si può trovare il coefficiente $\phi(p)$:

\begin{equation}
\phi(p) = \intf dx\; \frac{e^{-ipx/\hbar}}{\sqrt{2\pi\hbar}}\psi(x)
\end{equation}

Utilizzando la stessa interpretazione che abbiamo dato nel caso dell'espansione sulle autofunzioni dell'hamiltoniana, vediamo che $\phi(p)$ è la probabilità di misurare il momento $p$ in una pacchetto $\waf{}$. Quindi la nostra interpretazione di $\phi(p)$ come della funzione d'onda nel dominio dei momenti ha senso.

La probabilità di ottenere un impulso compreso tra $ p $ e $ p + dp $ è dunque

\begin{equation}
P(p) = |\phi(p)|^2
\end{equation}

\subsection{Degenerazione di stati}
Prendiamo un generico operatore lineare (anche hermitiano?) $ A $. Se esiste una funzione $ \phi $ tale che

\begin{equation}
A\phi = a\phi \qquad a \in \mathbb{C}
\end{equation}

e inoltre esiste una funzione $ \rchi $ per cui

\begin{equation}
A\rchi = a\rchi 
\end{equation}

Allora l'autovalore $ a $ è detto \emph{degenere}. Se vi sono più funzioni si dice che $ a $ è -degenere. In questo caso si può osservare che la combinazione lineare delle due autofunzioni è a sua volte un'autofunzione, cosa non vera in generale:

\begin{align}
A(C_1\phi + C_2\rchi) &= C_1 A(\phi) + C_2 A(\rchi) = \nonumber \\
&= C_1 a\phi + C_2 a\rchi = (C_1\phi + C_2\rchi)a 
\end{align}
Tratteremo l'argomento della degenerazione in modo più generale in seguito.


\subsection{Operatore parità} \label{sec:Operatore di parità}
Il concetto di Parità è legato ad una trasformazione discreta\footnote{Con trasformazione discreta si intende una trasformazione non infinitesima, ma che passa direttamente da uno stato all'altro} che manda

\begin{equation}
x \rightarrowtail -x
\end{equation}

\'E possibile definire l'operatore di parità $ P $:

\begin{equation}
P(\waf{}) = \psi(-x,t)
\end{equation}

L'operatore di parità, per definizione, agisce solo sulla posizione e non sul tempo. Il fattore di fase di una funzione d'onda $ e^{-iE_n t/\hbar} $ non influenza quindi la parità della funzione stessa.
Notiamo inoltre che se $ \waf{} $ è pari:

\begin{equation}
P\psi = 1\psi \qquad \lambda = 1
\end{equation}

Se invece è dispari

\begin{equation}
P\psi = -1\psi \qquad \lambda = -1
\end{equation}

$ P^2 $ è un operatore idempotente e si valori sono ovviamente soltanto $ \pm 1 $. D'ora  in poi assoceremo al termine di parità positiva (+) il concetto di funzione pari e al termine di parità negativa (-) quello di funzione dispari. Sappiamo che una qualsiasi funzione può essere decomposta in una funzione pari più una funzione dispari:

\begin{align}
\psi(x) &= \frac{1}{2}\left[ \psi(x) + \psi(-x) \right] + \frac{1}{2}\left[ \psi(x) - \psi(-x) \right] \nonumber \\
	    &= \left[\frac{1}{2}(P+1) + \frac{1}{2}(P-1)\right] \psi \nonumber \\
	    &= \psi^{(+)} + \psi^{(-)}
\end{align}

Cioè, abbiamo decomposto una funzione in termini delle autofunzioni dell'operatore $ P $, analogamente al caso delle funzioni dell'energia che sono combinazioni lineari delle autofunzioni $ u_n(x) $. L'importanza dell'operatore P sta nella non degenerazione: le funzioni $ \cos(n\pi x/a) $ e $ \sin(n\pi x/a) $ sono autofunzioni dell'energia con lo stesso autovalore (quindi siamo in un caso degenere) e sono anche autofunzioni dell'operatore di parità, relativo - però - a due autovalori distinti ($ \lambda = +1 $ e $ \lambda = -1 $).
Ha quindi molta importanza la ricerca di eventuali simmetrie nell'hamiltoniana nel problema da esaminare ed una scelta appropriata di coordinate in modo da renderle evidenti.
Quello che vogliamo fare è determinare sotto quali condizioni una funzione mantiene la sua parità nel tempo. Prendiamo una funzione d'onda pari $ \psi(x,0) = \psi(-x,0) $ e vediamo come si evolve nel tempo:

\begin{equation}
i\hbar\pdv{t}{\waf{}}{} = H\waf{} \nonumber \\
\end{equation}

Applichiamo l'operatore di parità $ P $:

\begin{equation}
i\hbar P \pdv{t}{\waf{}}{} = PH\waf{}
\end{equation}

Se il potenziale $ V(x) $ è pari, allora l'operatore H è pari e quindi posso invertire l'ordine:

\begin{equation}
i\hbar \pdv{t}{P\waf{}}{} = HP\waf{}
\end{equation}

Abbiamo in sostanza ricavato che la parità non dipende dal tempo e che la relazione tra $ H $ e $ P $ è:

\begin{align}
HP\waf{} = PH\waf{} \nonumber \\
HP\waf{} - PH\waf{} = 0 \nonumber \\
[H,P] = 0
\end{align}

In generale, quando il commutatore di un operatore non dipendente dal tempo con l'hamiltoniana è nullo, è una costante del moto.

\subsubsection{Esercizio sugli autostati}

\textbf{Problema:} Al tempo $ t=0 $ sia data

\begin{equation}
\psi(x,0) = \frac{1}{\sqrt{2}}\left( u_1(x) + u_2(x) \right)
\end{equation}

Con $ u_1 $ e $ u_2 $ autofunzioni relativa ai valori $ E_1 $ ed $ E_2 $ dell'energia.

\begin{itemize}
\item
\textbf{\'E normalizzata?}

\begin{align}
|\psi|^2 = \intf \psi^* \psi dx &= \frac{1}{2} \int |u_1|^2 + |u_2|^2 + u^*_1 u_2 + u^*_2 u_1 dx \nonumber \\
&= \frac{1}{2} 2 = 1
\end{align}

Quindi è normalizzata.

\item
\textbf{è autostato dell'hamiltoniana?}

In generale abbiamo visto che la combinazione lineare di autostati non è autostato, a meno che le autofunzioni apparengano ad un autovalore degenere. Quindi la risposta è no.

\item
\textbf{Come si evolve la funzione del tempo?}

Bisogna tenere conto del fattore temporale:

\begin{equation}
\waf{} = \frac{1}{\sqrt{2}}\left[ e^{-iE_1 t/\hbar}u_1(x) + e^{-i E_2 t /\hbar} u_2(x) \right]
\end{equation}

Questa rappresenta in generale la funzione d'onda nel tempo con la dipendenza temporale.

\item
\textbf{Come si calcola e quanto vale <E>?}

Ci sono due modi per farlo:

\begin{enumerate}
\item
Usare la definizione di valor medio di un operatore:

\begin{align}
<E> &= \intf \psi^* H \psi = \frac{1}{2} \intf (u^*_1 + u^*_2) H (u_1 + u_2)\; dx \nonumber \\
&= \frac{1}{2} \intf (u^*_1 + u^*_2)(E_1 u_1 + E_2 u_2)\; dx \nonumber \\
&= \frac{1}{2} [E_1 + E_2] \nonumber \\
&= \frac{E_1 + E_2}{2}
\end{align}

\item
Usare il postulato di espansione:

In questo caso 

\begin{equation}
|A_1|^2  = \intf u_1^* \psi \;dx = \frac{1}{2} \qquad |A_2|^2  = \intf u_2^* \psi \;dx = \frac{1}{2} 
\end{equation}

Ricordando che 

\begin{align}
<E> &= \sum p_n E_n = \sum |A_n|^2 E_n  \nonumber \\
&= \frac{E_1 + E_2}{2}
\end{align}
\end{enumerate}

\item
\textbf{Qual è la probabilità di ottenere da una misura dell'energia un valore di}
\begin{equation}
\frac{7}{15}(E_1 + E_2)
\end{equation}

La probabilità è nulla; infatti o ottengo $ E_1 $ oppure ottengo $ E_2 $. Un caso particolare potrebbe essere quando una combinazione lineare è uguale al valore di uno dei due autovalori. In quel caso la probabilità è quella associata all'autovalore trovato. Supponiamo di aver effettuato la misura e di aver trovato $ E_1 $. Se rimisuriamo l'energia la probabilità di trovare $ E_1 = 1 $ mentre la probabilità di trovare qualsiasi altro valore è nulla, perchè la funzione d'onda è collassata su quell'autovalore.

\item
\textbf{Quanto vale <x> all'istante t=0 ?}

\begin{align}
&<x>(t=0)  =\frac{1}{2}\intf \psi^* x \psi dx = \nonumber \\
 &= \frac{1}{2}\left[ \intf u_1^* x u_1 + \intf u_2^* x u_2 + \intf u_1^* x u_2 + \intf u_2^* x u_1 \right] \nonumber\\
 &= \frac{a}{2} + \frac{a}{2} + \frac{1}{2}\frac{2}{a} \intf x \sin\left( \frac{\pi x}{a} \right) \sin\left( \frac{2\pi x}{a} \right) dx + \frac{1}{2}\frac{2}{a}\intf x \sin\left( \frac{\pi x}{a} \sin\left( \frac{2\pi x}{a} \right) \right) \nonumber \\
 & = \frac{a}{2} - \frac{16 a}{9\pi^2}
\end{align}

\item
\textbf{Quanto vale <x> nel tempo?}

\begin{align}
<x>(t) &= \frac{1}{2}\left[ \frac{a}{2} + \frac{a}{2} -\frac{16a}{9\pi^2} \left( e^{i(E_1 - E_2)t/\hbar} - e^{-i(E_1 - E_2)t/\hbar} \right) \right] \nonumber \\
<x>(t) &= \frac{a}{2} \frac{16a}{9\pi^2} \left( \frac{1}{2}\left[\cos(\dots) + i\sin(\dots)\right] + \frac{1}{2}\left[ \cos(\dots) - i\sin(\dots) \right] \right)\nonumber \\
&= \frac{a}{2} -\frac{16a}{9\pi^2}\cos\left( \frac{(E_1 - E_2)t}{\hbar} \right)
\end{align}

Quindi il valor medio oscilla nel tempo.

\end{itemize}

\end{document}