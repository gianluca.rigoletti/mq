\documentclass[../mq.tex]{subfiles}
\begin{document}
\newpage

\newpage

\section{Equazione di Schroedinger}
\subsection{Derivazione dell'equazione}
Richiamiamo alcune formule derivate dall'ipotesi di Planck e DeBroglie:

\begin{align}
k &= \frac{2\pi}{\lambda} \qquad \lambda = \frac{h}{p}  \qquad k = \frac{p}{\hbar} \\ \nonumber
\omega &= 2\pi\nu \qquad E = h\nu \qquad E = \frac{p^2}{2m}
\end{align}

Se nella funzione d'onda $ \waf{} $ sostituisco $ p,E $ al posto di $ k,\omega $ ottengo:

\begin{equation}
\waf{} = \frac{1}{\sqrt{2\pi\hbar}}\int_{-\infty}^{\infty}\Phi(p)e^{i(px-Et)/\hbar}dp
\end{equation}

Considero adesso la quantità

\begin{align}
-i\hbar\pdv{t}{\waf{}}{} = \frac{1}{\sqrt{2\pi\hbar}}\int \Phi(p)Ee^{i(px-Et)/\hbar}dp \\ \nonumber
\frac{\hbar}{i}\pdv{x}{\waf{}}{} = \frac{1}{\sqrt{2\pi\hbar}}\int \Phi(p)pe^{i(px-Et)/\hbar}dp \\ \nonumber
\Rightarrow \frac{\hbar}{i}\frac{\hbar}{i} \pdv{x}{}{}\pdv{x}{\waf{}}{}  = \frac{1}{\sqrt{2\pi\hbar}}\int \Phi(p)p^2e^{i(px-Et)/\hbar}dp
\end{align}

Ricordando che $ E = p^2/2m $ posso notare che la prima equazione è uguale alla terza a meno di un fattore. Arriviamo così all'eq. di Schroedinger:

\begin{equation} \label{eq:Schoredinger}
\boxed{
i\hbar\pdv{t}{\waf{}}{} = - \frac{\hbar^2}{2m} \pdv{x}{\waf{}}{2} + V(x)\waf{}
}
\end{equation}

Che esiste anche per $ \waf{}^* $ coniugando l'equazione:

\begin{equation} \label{eq:Schroedinger coniugata}
\boxed{
-i\hbar\pdv{t}{\waf{}^*}{} = - \frac{\hbar^2}{2m} \pdv{x}{\waf{}^*}{2} + V(x)\waf{}^*
}
\end{equation}

\'E un'equazione differenziale che definisce l'evoluzione temporale e spaziale della funzione d'onda. L'equazione in t è del primo ordine, quindi ho bisogno di una sola condizione iniziale $ \psi(x,0)$, diversamente dalla classica equazione differenziale che descrive le propagazioni ondulatorie.
Questa conclusione è coerente con l'interpretazione probabilistica di $\waf{}$, infatti non avrebbe alcun senso richiedere di conoscere la derivata prima.


Ricordiamo che la probabilità associata alla funzione d'onda non è altro che 

\begin{equation}
P(x,t) = |\waf{}|^2
\end{equation}

e in particolare, essendo una p.d.f. deve valere che 

\begin{equation}
\int_{-\infty}^{+\infty}|\waf{}|^2dx = 1
\end{equation}

Inoltre, $ \psi $ dev'essere derivabile e $ \psi' $ deve essere continua per motivi di regolarità della soluzione.

Le osservabili fisiche possono essere calcolate utilizzando il valore di aspettazione rispetto alla p.d.f. $\waf{}$. Ad esempio, il valore d'aspettazione dell'osservabile posizione è 

\begin{equation}
<x> = \int  \waf{}^*\;x\;\waf{} dx
\end{equation}
Vedremo in seguito (sezione: \vref{sec:momento}) in modo più approfondito il significato di questa operazione.

\subsection{Condizioni iniziali e $\phi(p)$}
Abbiamo detto che l'unica condizione iniziale necessaria per risolvere l'eq. di Schroedinger è $\psi(x,0)$. Inoltre la soluzione dipende dalla forma del pacchetto d'onda, cioè da $\phi(p)$. Queste due quantità sono legate esattamente da una trasformazione di Fourier:
\begin{equation} 
\psi(x,0)= \frac{1}{\sqrt{2\pi\hbar}} \int_{-\infty}^{+\infty} dp\, \phi(p)\, e^{ipx/\hbar} 
\end{equation}
Applicando la trasformata inversa:
\begin{equation}
\intf dx\;\psi(x,0)\;e^{-ip'x /\hbar}  = \frac{1}{\sqrt{2\pi\hbar}} \intf dx \; \intf dp\;\phi(p) e^{i(p-p')x/\hbar} 
\end{equation}
\begin{equation}\label{eq:phip}
\boxed{\phi(p) =  \frac{1}{\sqrt{2\pi\hbar}} \intf dx\;\psi(x,0)\;e^{-ipx/\hbar}}
\end{equation}
Usando la trasformata della funzione costante $\intf dx\; e^{i(p-p')x/\hbar} = 2\pi\hbar\delta(p-p')$
Quindi concludiamo osservando che le condizioni iniziali su $\psi(x,0)$ determinando la composizione del pacchetto nel dominio dei momenti. Approfondiremo questa relazione in seguito.

Tornando al confronto tra la forma del pacchetto d'onda e la forma di $\phi(p)$, ipotizzando che i due pacchetti siano gaussiani (come abbiamo visto in sezione \vref{sec:pacchetto_d'onda} ), possiamo utilizzare la nota proprietà della trasformata di Fourier della gaussiana per mostrare come varia la larghezza di un pacchetto al variare della larghezza della funzione modulatrice $\phi(p)$.
In generale vale che $$\Delta x \Delta p \geq const$$ quindi le larghezze delle gaussiane sono inversamente proporzionali.

I due estremi sono:
\begin{description}
	\item [$\Delta p = 0$] Se consideriamo un $\phi(p)= \delta(p-p_0)$, cioè un onda monocromatica, allora il pacchetto d'onda è completamente delocalizzato e ha la stessa ampiezza in tutto lo spazio. 
	\item [$\Delta x=0$] Se considerimo un pacchetto $\waf{} = \delta (x-x_0)$ allora l'ampiezza di $\phi(p)$ è costante in tutto lo spazio e il pacchetto contiene tutte le lunghezze d'onda.
\end{description}



\subsection{Equazione di continuità}
Calcoliamo ora alcune quantità che saranno utili in seguito:

\begin{equation}
\pdv{t}{|\psi|^2}{} = \left(\pdv{t}{\psi^*}{}\right)\psi + \psi^*\left(\pdv{t}{\psi}{}\right)
\end{equation}

Se sostituiamo le derivate rispetto al tempo con le espressioni ricavate dall'equazione di Schoredinger \vref{eq:Schroedinger coniugata} otteniamo:

\begin{align}
\pdv{t}{|\psi|^2}{} &= \frac{1}{i\hbar}\left[\frac{\hbar^2}{2m} \left( \pdv{x}{\psi^*}{2} \right)\psi - \psi^*\frac{\hbar^2}{2m} \left( \pdv{x}{\psi}{2} \right) \right] \\ \nonumber
&= -\frac{\hbar^2
	}{2mi}\pdv{x}{}{}\left( \psi^*\pdv{x}{\psi}{} - \psi\pdv{x}{\psi^*}{} \right)
\end{align}
La variazione nel tempo della densità di probabilità assomiglia molto a una divergenza (soprattuto se la calcolassimo in 3 dimensioni).
Chiamo con $ \rho $ e $ J(x,t) $ le quantità:

\begin{align}
\rho &= |\psi|^2 \\ \nonumber
J(x,t) &= \frac{\hbar}{2im}\left(\psi^*\pdv{x}{\psi}{} - \psi\pdv{x}{\psi^*}{}\right)
\end{align}

Possiamo allora riscrivere:

\begin{equation}
\pdv{t}{\rho}{} =-\pdv{x}{}{}\left( J(x,t) \right)
\end{equation}

Consideriamo un volume generico V ed eseguiamo il calcolo della probabilità totale all'interno del volume.
\begin{equation}\label{eq:continuità}
\pdv{t}{}{} \int_V |\psi|^2\;dV = - \int_V \vec{\nabla}\cdot \vec{J}\;dV = -\int_{S(V)} \vec{J}\cdot \hat{n}\; dS
\end{equation}
dove abbiamo usato il teorema della divergenza e abbiamo considerato $J$ come una \emph{corrente di probabilità}. Vediamo quindi l'analogia con l'equazione dei continuità del vettore densità di corrente dell'elettrostatica: La variazione nel tempo della densità di probabilità contenuta in un generico volume deve essere uguale al flusso della corrente di probabilità attraverso quel volume. Si tratta di una legge di \emph{conservazione} della probabilità.

Ora mandiamo all'infinito la superficie in considerazione, includendo tutto l'universo. Poichè la funzione d'onda $\waf{}$ deve essere una funzione continua e appartenente a $\mathcal{L}_2$, all'infinito tende a 0 più velocemente di qualsiasi potenza di $x^n$.
Quindi il termine a destra nell'eq. \ref{eq:continuità} espandendo la superficie all'infinito tende a 0. Questo dimostra che la probabilità totale nell'universo si \emph{conserva}, in analogia con il principio di conservazione della carica.
Notiamo come nel calcolo la corrente di probabilità non dipenda dal potenziale $ V(x) $: questo è dovuto al fatto che si è assunto che il potenziale fosse reale. Se così non fosse possiamo scrivere

\begin{align}
V(x) = U(x) + iW(x) \nonumber \\
\pdv{t}{\rho}{} + \frac{dJ}{dt} = 2i\psi^*W(x)\psi
\end{align}

La probabilità non si conserva; possiamo interpretare il risultato come una sorgente in grado di creare o distruggere le particelle, anche se in questo corso non verrà approfondito.


\end{document}