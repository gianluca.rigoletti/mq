\documentclass[../mq.tex]{subfiles}
\begin{document}

\newpage
\section{Atomo di idrogeno reale}

Fino a questo punto abbiamo trattato l'atomo di idrogeno basandoci sull'hamiltoniana più
semplice possibile, derivante da considerazioni classiche:
\begin{equation}
H_0 = \frac{p^2}{2m_e}- \frac{Ze^2}{4\pi \epsilon_0}\frac{1}{r}
\end{equation}
A questa descrizione vanno aggiunte diverse correzioni che tengono conto delle correzioni relativistiche al moto dell'elettrone. La teoria delle perturbazioni (vedi \vref{sec:teoria_perturbazioni}) è lo strumento che utilizzeremo per analizzare l'atomo di idrogeno reale, infatti le correzioni possono essere considerate contributi piccoli rispetto all'energia potenziale coulombiana. Vedremo come la degenerazione dell'atomo di idrogeno imperturbato verrà rotta dalle correzioni reali.

\subsection{Energia cinetica relativistica}
La prima correzione relativistica riguarda l'energia cinetica dell'elettrone.
In meccanica relativistica si ha il quadrivettore momento/energia:
\begin{equation}
p^\mu = (E,\vec{p}c) \qquad ||p^\mu|| = E^2 -p^2c^2 - m^2c^4
\end{equation}
Si definisce energia cinetica, l'energia totale meno l'energia di massa a riposo ($mc^2$):
\begin{equation}
K = \sqrt{p^2c^2 - m^2c^4} -mc^2
\end{equation}
Il moto dell'elettrone però è lontano dal regime relativistico, poichè l'energia di massa a riposo è molto più grande dell'energia orbitale dell'elettrone, quindi possiamo sviluppare in serie $K$ considerando $p/c \ll \epsilon$:
\begin{align}
K &= mc^2 \sqrt{\frac{p^2}{m^2c^2}+1}-mc^2\nonumber  \\
&= mc^2 (1+ \frac{p^2}{2m^2c^2} -\frac{1}{8} \frac{p^4}{m^3c^2}) -mc^2 
\end{align}
\begin{equation}
\boxed{K=\frac{p^2}{2m} - \frac{1}{2}\left(\frac{p^2}{2m}\right)^2\frac{1}{mc^2} }
\end{equation}
Abbiamo quindi trovato la correzione al primo ordine da applicare all'hamiltoniana imperturbata. 
Notiamo che l'energia dell'atomo di idrogeno nello stato fondamentale è dell'ordine di $E_0 \propto \alpha^2 m c^2$ mentre la correzione è proporzionale a:
\begin{equation}
\left(\frac{p^2}{2m}\right)^2 \frac{1}{mc^2} \approx \alpha^2 \frac{p^2}{2m}
\propto \alpha^4
\end{equation} 
Quindi la correzione è di ordine di $\alpha^2$ rispetto all'energia imperturbata. Essendo $\alpha^2 \approxeq 10^{-4}$ ha senso utilizzare la teoria delle perturbazioni.

Riscriviamo l'hamiltoniana perturbativa in forma più comoda:
\begin{equation}
H_1 = -\frac{1}{2m_ec^2}\left(\frac{p^2}{2m_e}\right)^2 =-\frac{1}{2m_ec^2} \left(H_0 + \frac{Ze^2}{4\pi \epsilon_0 r}\right)^2
\end{equation}

La perturbazione è già diagonale sulla base $\ket{n,l,m}$, poichè dipende solamente da $r$.
\begin{align}
\avg{\phi_{nlm}}{H_1} & = -\frac{1}{2m_ec^2} \avg{\phi_{nlm}}{H_0^2 + 2H_0\left(\frac{Ze^2}{4\pi\epsilon r}\right) + \left(\frac{Ze^2}{4\pi\epsilon r}\right)^2}\nonumber \\
&= -\frac{1}{2m_ec^2}\left[ E_n^2 + 2E_n\avg{\phi_{nlm}}{\frac{Ze^2}{4\pi\epsilon r}}+ \avg{\phi_{nlm}}{\left(\frac{Ze^2}{4\pi\epsilon r}\right)}\right]\nonumber \\
&= -\frac{1}{2m_ec^2} \left\{\left[ \frac{m_ec^2(Z\alpha)^2}{2n^2}\right]^2 -2 \left[\frac{m_ec^2(Z\alpha)^2}{2n^2}\right]\frac{Ze^2}{4\pi\epsilon_0}\left(\frac{Z}{a_0n^2}\right) \nonumber \right.  \\
& \left.+ \left(\frac{Ze^2}{4\pi\epsilon_0}\right)^2 \frac{2Z^2}{a_0^2n^3(2l+1)} \right\}  \\
&= -\ume m_e c^2 (Z\alpha)^2 \left[\frac{2(Z\alpha)^2}{n^2(2l+1)} - \frac{3(Z\alpha)^2}{4n^4} \right]
\end{align}
Dove abbiamo utilizzato le formule note per $\langle 1/r \rangle$ (\vref{formulario:1/r}) e $\langle 1/r^2 \rangle$ (\vref{formulario:1/r^2}).

Possiamo osservare che, a causa di questa perturbazione, gli stati con $n$ uguali, ma $l$ diverso non sono più degeneri.

\subsection{Accoppiamento spin-orbita}
Finora abbiamo considerato la carica dell'elettrone solo per calcolare la sua energia potenziale nel potenziale elettrico generato dai protoni del nucleo. Una carica che si muove in un campo elettrico però sente anche un campo magnetico che si ricava applicando correttamente le trasformazioni di Lorentz al campo elettromagnetico.  Questo campo magnetico, effetto relativistico, interagisce con lo spin dell'elettrone, generando una perturbazione dell'hamiltoniana.
\begin{equation}
\mathbf{B}= - \frac{\vec{v} \times \mathbf{E}}{c^2} = - \frac{\vec{v} \times (-\nabla\phi(r))}{c^2} = \frac{1}{c^2}\,\vec{v} \times \frac{\vec{r}}{r} \frac{d\phi(r)}{dr}
\end{equation}
dove $\phi(r)$ è il potenziale elettrostatico del nucleo. Solamente il momento magnetico generato dallo spin interagisce con questo campo magnetico, infatti stiamo immaginando di essere nel sistema di riferimento dell'elettrone, dove non c'è momento angolare orbitale. Quindi il momento magnetico associato all'elettrone é:
\begin{equation}
\mathbf{\mu} = -\frac{e}{2m_e} g \mathbf{S} = -\frac{e}{m_e} \mathbf{S} \quad\mbox{   g=2 fattore giroscopico} 
\end{equation}

L'hamiltoniana di interazione è:
\begin{align}
H_2= -\mu\cdot B &= \frac{e}{m_ec^2} \mathbf{S} \cdot \vec{v} \times \vec{r}\, \frac{d\phi(r)}{dr} \nonumber \\
&= \frac{e}{m_e^2 c^2} \mathbf{S}\cdot \vec{p} \times  \vec{r}\, \frac{d\phi(r)}{dr} \nonumber \\
&= - \frac{e}{m_e^2c^2} \mathbf{S}\cdot \mathbf{L} \, \frac{1}{r} \frac{d\phi(r)}{dr}
\end{align}
Consideriamo il potenziale elettrico coulombiano e aggiungiamo un fattore $1/2$ dovuto al fatto che l'elettrone non si muove di moto rettilineo nel campo elettrico\footnote{Questo effetto è chiamato fattore di precessione di Thomas, ed è un effetto relativistico. La presenza di questo fattore confermò la correttezza del concetto di spin}:
\begin{equation}
\boxed{H_2 = \frac{Ze^2}{4\pi\epsilon_0 }\frac{1}{2m_e^2c^2} \frac{\mathbf{S}\cdot \mathbf{L}}{r^3}}
\end{equation}

L'ordine di grandezza del rapporto tra la perturbazione e l'hamiltoniana imperturbata è ancora $\alpha^2$, quindi va considerata allo stesso livello della perturbazione per il momento relativistico.

La perturbazione dipende dallo stato di spin dell'elettrone e anche dal suo momento angolare. L'hamiltoniana ha degenerazione $2(2l+1)$ per ogni livello energetico, tenendo conto anche degli stati di spin, quindi dobbiamo utilizzare la teoria delle perturbazioni degenere. Utilizzando la base del \emph{momento angolare totale} la perturbazione risulta già diagonalizzata.

\begin{gather}
S+ L  = J \\
S^2 + 2S\cdot L + L^2 = J^2 \nonumber\\
S\cdot L = \ume (J^2 -L^2 - S^2)
\end{gather}
Quindi possiamo costruire una base di vettori del momento angolare totale con combinazioni lineari di autovettori degeneri di $H_0$, che diagonalizzano la perturbazione. I vettori possibili saranno:
\begin{equation}
\ket{J,M} \mbox{   con  } J= l\pm \ume \mbox{   e   } M = m+ \ume, \ldots ,-m-\ume
\end{equation}
\begin{align}\label{eq:spin_orbita1}
S\cdot L \ket{l+\ume, M} &= \ume (J^2 -L^2 -S^2) \ket{l+\ume, M} \nonumber \\
&=\ume \hbar^2 \left[\left(l+\ume\right) \left(l+ \frac{3}{2}\right) -l(l+1) -\frac{3}{4}  \right]\ket{l+\ume,M} \nonumber \\
&=\ume \hbar^2\,l \ket{l+\ume,M}
\end{align}
\begin{align}
S\cdot L \ket{l-\ume, M} &= \ume (J^2 -L^2 -S^2) \ket{l-\ume, M} \nonumber \\
&=\ume \hbar^2 \left[\left(l-\ume\right) \left(l+ \ume\right) -l(l+1) -\frac{3}{4}  \right]\ket{l-\ume,M} \nonumber \\
&=-\ume \hbar^2(l+1) \ket{l-\ume,M}
\end{align}

Vediamo subito che l'autovalore $M$ non è determinante. Per ogni valore di $l$ nei due casi abbiamo $2(l+1/2)+1$ e $2(l-1/2)+1$ diversi vettori, che subiscono la pertubazione allo stesso modo: la degenerazione non è completamente rimossa. I gruppi di vettori degeneri sono stati solamente riorganizzati dall'azione dell'accoppiamento spin-orbita.

\begin{equation}
\avg{\phi_{nlm}}{H_2} = \frac{1}{2m_e^2c^2} \frac{Ze^2\hbar^2}{8\pi\epsilon_0} \left\{\begin{matrix}
l \\ -l-1 
\end{matrix}\right\}_{l-1/2}^{l+1/2} \; \int_0^{+\infty} r^2 (R_{nl}(r))^2 \frac{1}{r^3} \,dr
\end{equation}
I due casi sono per $j = l \pm \ume$. Ricordando la media di $1/r^3$ (vedi \vref{formulario:1/r^3}) si ottiene:
\begin{equation}
\Delta E = \frac{1}{2m_e^2}\frac{Z^4 e^2 \hbar^2}{4\pi\epsilon_0 a_0^3} \;\frac{\left\{\begin{matrix}
l \\ -l-1 
\end{matrix}\right\}_{l-1/2}^{l+1/2}}{n^3 l (l+1)(2l+1)}
\end{equation}
Ricordando le espressioni di $\alpha$ (vedi \vref{formula:alpha}) e $a_00$ (\vref{formula:a_0}), di ottiene:
\begin{equation}
\Delta E= \frac{m_e}{2c^2}\frac{Z^4 e^8}{(4\pi\epsilon_0)^4\hbar^4} \{\ldots\} = \ume m_e c^2 (Z\alpha)^4 \;\frac{\left\{\begin{matrix}
	l \\ -l-1 
	\end{matrix}\right\}_{l-1/2}^{l+1/2}}{n^3 l (l+1)(2l+1)}
\end{equation}

Questo risultato è valido per $l \neq 0$. Gli stati con $l=0$ non sono influenzati dallo spin-orbita perchè se $l=0$ allora $j=1/2$ necessariamente ($j$ varia da $l+1/2$ a $|l-1/2|$) e quindi l'effetto va calcolato utilizzando la formula \vref{eq:spin_orbita1} che per $j=0$ risulta $0$.

\subsection{Somma delle perturbazioni}
Essendo entrame le perturbazioni di ordine $\alpha^2$ rispetto a $H_0$, dobbiamo sommarne gli effetti:

\begin{figure}[htb]
	\centering
	\includegraphics[width=\linewidth]{./img/correzioni_H}
	\caption{Correzioni atomo di idrogeno $n=2$}
	\label{fig:correzioni_H}
\end{figure}

\begin{align}
\Delta E &= \ume m_e c^2 (Z\alpha)^4 \;\frac{\left\{\begin{matrix}
	l \\ -l-1 
	\end{matrix}\right\}_{l-1/2}^{l+1/2}}{n^3 l (l+1)(2l+1)} -\ume m_ec^2(Z\alpha)^4 \left[\frac{2}{n^3(2l+1)} - \frac{3}{4n^4}\right] \nonumber \\
&= \ume m_e c^2 (Z \alpha)^4 \left\{ \frac{\left\{\begin{matrix}
	l \\ -l-1 
	\end{matrix}\right\}_{l-1/2}^{l+1/2}}{n^3 l (l+1)(2l+1)} - \frac{2}{n^3(2l+1)} + \frac{3}{4n^4} \right\}\nonumber \\
&= \ume m_e c^2 (Z \alpha)^4 \begin{cases}
\displaystyle \frac{4ln - 8nl(l+1) + 3l(l+1)(2l+1)}{4n^4 l (l+1)(2l+1)} & j = l+\ume \\\\ \displaystyle
\frac{-4n(l+1) -8nl(l+1) + 3l(l+1)(2l+1)}{4n^4 l (l+1)(2l+1)} & j = l-\ume \\
\end{cases} \nonumber \\
&= \ume m_e c^2 (Z \alpha)^4 \begin{cases}
\displaystyle \frac{6l^2  +9l -8nl -4n +3}{4n^4  (l+1)(2l+1)} & j = l+\ume \\\\ \displaystyle
\frac{6l^3 + 9l^2 + 3l -8nl^2 -4nl -12n}{4n^4 l (l+1)(2l+1)} & j = l-\ume 
\end{cases}\nonumber\\
&=\ume m_e c^2 (Z \alpha)^4 \begin{cases}
\displaystyle  \frac{6j^2 +3j -8nj}{8n^4(j+1/2)j} & \leftarrow l = j -\ume
\\ [\ldots]
\end{cases}
\end{align}

L'effetto complessivo, valido per $j= l \pm \ume$ e anche per $l=0$ è:
\begin{equation}
\Delta E = -\ume m_ec^2(Z\alpha)^4 \,\frac{1}{n^3} \left(\frac{1}{j+1/2} - \frac{3}{4n}\right)
\end{equation}


Osserviamo lo splitting dei livelli degeneri dell'atomo di idrogeno con $n=2$ causato dalle due perturbazioni. Abbiamo utilizzato la notazione $l_{j}^n$. 

La perturbazione spin-orbita distingue i livelli, separando quelli con uguale $j$ ma diversa $l$. Rimane una degenerazione $(2j+1)$ in ognuno di essi. 
La perturbazione relativistica però riporta allo stesso livello di energia gli stati con uguale $j$.

\clearpage

\end{document}