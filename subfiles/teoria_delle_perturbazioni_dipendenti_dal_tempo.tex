\documentclass[../mq.tex]{subfiles}
\begin{document}

\newpage
\section{Teoria delle perturbazioni dipendenti dal tempo}

Nella teoria delle perturbazioni dei capitoli precedenti abbiamo studiato il comportamento degli stati
stazionari, nei casi in cui la perturbazione non dipenda esplicitamente dal tempo. Tuttavia, vi sono molti
casi d'interesse nello studiare le probabilità che una particella transisca da uno stato ad un altro nel corso
del tempo. Questo è l'argomento di interesse di questa sezione.

La strategia che applichiamo è quella di prendere uno stato $ \psi(t) $ dipendente dal tempo che risolve 
l'equazione di Schr\"odinger e di espanderlo in termini di un set completo di autostati dell'hamiltoniana imperturbata. Gli autostati sono tali per cui:

\begin{equation}
H_0 \ket{\phi_n} = E_n^{(0)} \ket{\phi_n}
\end{equation}

L'equazione di Schr\"odinger che invece vogliamo risolvere è:

\begin{equation}\label{eq:di Schrodinger perturbazioni temporali}
i\hbar\frac{d}{dt}\ket{\psi(t)} = (H_0 + \lambda V(t))\ket{\psi(t)}
\end{equation}

Espandiamo ora lo stato $ \ket{\psi(t)} $ sul set $ \ket{\phi_n} $ tenendo conto, però, della dipendenza
esplicita nel tempo, cioè scriviamo:

\begin{equation}
\ket{\psi(t)} = \sum_n c_n(t)e^{-iE_{n}^{(0)}t/\hbar}\ket{\phi_n}
\end{equation}

La prima osservazione da fare è che abbiamo scomposto lo stato utilizzando dei coefficienti $ c_n(t) $; questi
rappresentano il contributo dovuto alla perturbazione temporale $ V(t) $. Se non ci fosse, infatti, i coefficienti non dipenderebbero dal tempo e sarebbero costanti, e quindi otteremmo soltanto un cambio di base.

Ora inseriamo l'espansione all'interno della \vref{eq:di Schrodinger perturbazioni temporali} e otteniamo:

\begin{align}
i\hbar\frac{d}{dt}\left[ \sum_n c_n(t)e^{-iE_{n}^{(0)}t/\hbar}\ket{\phi_n} \right] &= (H_0 + \lambda V(t)) \sum_n c_n(t)e^{-iE_{n}^{(0)}t/\hbar}\ket{\phi_n} \nonumber \\
\sum_n \left[ i\hbar \frac{dc_n(t)}{dt} + E_{n}^{(0)}c_n(t) \right]e^{-iE_n^{(0)}t/\hbar}\ket{\phi_n} &= \sum_n \left[ E_{n}^{(0)} + \lambda V(t) \right] c_n(t)e^{-iE_n^{(0)}t/\hbar}\ket{\phi_n}
\end{align}
Osserviamo che nel lato sinistro e nel lato destro ci sono due termini uguali. Di conseguenza l'equazione si 
riduce a:
\begin{equation}
\sum_n \left[ i\hbar \frac{dc_n(t)}{dt} - \lambda V(t)c_n(t) \right]e^{-iE_n^{(0)}t/\hbar}\ket{\phi_n} = 0
\end{equation}
A questo punto procediamo come abbiamo fatto nella teoria ordinaria, cioè moltiplicando per $ \bra{\phi_m}$ e 
sfruttando l'ortonormalità:
\begin{align} \label{eq:coefficienti dipendenti dal tempo}
\bra{\phi_m} \sum_n \left[ i\hbar \frac{dc_n(t)}{dt} - \lambda V(t)c_n(t) \right]e^{-iE_n^{(0)}t/\hbar}\ket{\phi_n} &= 0 \nonumber \\
i\hbar\frac{dc_m(t)}{dt}e^{-iE_m^{(0)}t/\hbar} - \sum_n \lambda c_m(t) e^{-iE_n^{(0)}t/\hbar} \bra{\phi_m} V(t) \ket{\phi_n} = 0 \nonumber \\
i\hbar\frac{dc_m(t)}{dt} = \lambda \sum_n c_m(t) e^{i(E_m^{(0)}-E_n^{(0)})t/\hbar} \bra{\phi_m} V(t) \ket{\phi_n} \nonumber \\
\end{align}
Per risolvere ulteriormente il sistema imponiamo delle condizioni iniziali: supponiamo che al tempo $ t = 0 $ \footnote{A volte può capitare che come condizione iniziale venga presa quella relativa allo stato iniziale nel passato, cioè $ t \rightarrow -\infty $} lo stato $ \ket{\psi} $ sia in un autostato dell'energia $ \ket{\phi_k} $, cioè $ \psi(0) = \phi_m $. Come detto prima, i coefficienti saranno costanti e visto che gli 
autostati dell'energia sono ortonormali fra di loro, avremo che:

\begin{equation} \label{eq:condizioni iniziali coefficienti dipendenti dal tempo}
c_n(0) = \delta_{nk}
\end{equation}
Se il sistema \vref{eq:coefficienti dipendenti dal tempo} soddisfa l'equazione ad un tempo t generico, soddisferà l'equazione anche per il tempo $ t = 0 $ per cui valgono le condizioni sui coefficienti sopra scritte. Inseriamo quindi  \vref{eq:condizioni iniziali coefficienti dipendenti dal tempo}:

\begin{equation}
\boxed{
	i\hbar\frac{dc_m(t)}{dt} = \lambda e^{-i(E_m^{(0)} - E_k^{(0)})t/\hbar}\bra{\phi_m}V(t)\ket{\phi_k}
}
\end{equation}
I coefficienti si ricavano risolvendo l'equazione differenziale:
\begin{equation}
\boxed{
	c_m(t) = \frac{\lambda}{i\hbar}\int_{0}^{t} dt' e^{i(E_m^{(0)} - E_k^{(0)})t'}\bra{\phi_m}V(t)\ket{\phi_k}
}
\end{equation}
Di conseguenza la probabilità di transizione di uno stato $ \ket{\psi(t)} $ ad uno stato $ \ket{\phi_n} $ sarà

\begin{equation}
P_n(t) = |c_n(t)|^2 = |\braket{\phi_n}{\psi(t)}|^2
\end{equation}

\subsection{Perturbazioni armoniche}

Le perturbazioni armoniche sono un particolare sottoinsieme di perturbazioni dipendenti dal tempo
caratterizzate dalla forma

\begin{equation}
V(t) = Me^{\pm i\omega t}
\end{equation}
Con M intendiamo un operatore che non dipende esplicitamente dal tempo. Studiamo i coefficienti e la
probabilità relativa al passaggio da uno stato $ \ket{\phi_k} $ ad uno stato $ \ket{\phi_m} $:

\begin{equation}
c_m(t) = \frac{\lambda}{i\hbar} \bra{\phi_m}M\ket{\phi_k} \int_{0}^{t} dt' e^{i (E_m^{(0)} - E_k^{(0)})}t'/\hbar e^{\pm \omega t'}
\end{equation}
Calcoliamo l'integrale temporale e otteniamo:

\begin{align}
&\int_{0}^{t} dt' e^{i (E_m^{(0)} - E_k^{(0)})t'/\hbar} e^{i\pm \omega t'} \qquad \qquad \frac{(E_m^{(0)} - E_k^{(0)})}{\hbar} := \omega_{mk} \nonumber \\
&= \frac{e^{i(\omega_{mk} \pm \omega)t} - 1}{i(\omega_{mk}- \omega)} \nonumber \\
&= \frac{e^{i(\omega_{mk} \pm \omega)t/2}}{ \frac{ ( \omega_{mk} \pm \omega ) }{ 2 } } \frac{ e^{ i( \omega_{mk} \pm \omega )t/2 } - e^{ -i( \omega_{mk} \pm \omega )t/2 } }{2i} \nonumber \\
&= e^{i(\omega_{mk} \pm \omega)t/2}  \frac{ \sin \omega_{mk} \pm \omega \; t }{ i( \omega_{mk} \pm \omega )/2 }
\end{align}
Il coefficiente sarà quindi:

\begin{equation}
\boxed{
	c_m(t) = \frac{\lambda}{i\hbar} \bra{\phi_m}M\ket{\phi_k} e^{i(\omega_{mk} \pm \omega)t/2}  \frac{ \sin \omega_{mk} \pm \omega \; t }{ i( \omega_{mk} \pm \omega )/2 }
}
\end{equation}
E la probabilità:

\begin{equation}
P_m(t) = |c_m(t)|^2 = \frac{\lambda^2}{\hbar^2} |\bra{\phi_m}M\ket{\phi_k}|^2 \left( \frac{ \sin\omega_{mk} \pm \omega \; t }{ ( \omega_{mk} \pm \omega )/2 } \right)^2
\end{equation}

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{img/perturbazioni-dipendenti-dal-tempo}
\caption{Andamento della funzione al variare di $\Delta$}
\label{fig:perturbazioni-dipendenti-dal-tempo}
\end{figure}

Se chiamiamo con $ \Delta $ la quantità:

\begin{equation}
\Delta = \frac{(\omega_{mk} - \omega)}{2}
\end{equation}

possiamo studiare la funzione:
\begin{equation}
F(\Delta) = \frac{4}{\Delta^2}\sin^2\left( \frac{t\Delta}{2} \right)
\end{equation}
Considerando il tempo come un parametro. Il risultato è mostrato in figura 
\vref{fig:perturbazioni-dipendenti-dal-tempo}. Per tempi sempre più grandi, la funzione diventa sempre più
piccata, fino al limite in cui abbiamo una delta di Dirac. Il procedimento per mostrare ciò non è immediato,
ma ci proviamo lo stesso. Se $ F $ è una funzione infinitamente differenziabile, allora possiamo espanderla
in serie di Taylor attorno all'origine, cioè:

\begin{equation}
F(\Delta) = \sum_{n=0}^{+\infty} x^n \frac{f^{(n)}(0)}{n!}
\end{equation}
%mi manca da capire perchè se t è grande l'approssimazione al primo termine è buona
Consideriamo il seguente integrale:

\begin{equation}
\intf f(\Delta)\frac{4}{\Delta}\sin^2\left( \frac{t\Delta}{2} \right) d\Delta
\end{equation}
Per grandi valori di t arrestiamo lo sviluppo di $ F(\Delta) $ al primo termine, in modo da scrivere:

\begin{align}
\intf f(0)\frac{4}{\Delta}\sin^2\left( \frac{t\Delta}{2} \right) d\Delta &= f(\Delta) \intf\frac{4}{\Delta}\sin^2\left( \frac{t\Delta}{2} \right) d\Delta \nonumber \\
&= 2tf(0)\intf \frac{1}{y^2}\sin^2(y) dy \nonumber \\
&= 2\pi t f(0)
\end{align}
La quantità $ f(0) $ possiamo anche scriverla come:

\begin{equation}
f(0) = \intf \delta(0)f(x) dx
\end{equation}
Quindi, uguagliando l'integrale da cui siamo partiti con questo appena scritto, avremo che:

\begin{equation}
\intf f(\Delta)\frac{4}{\Delta}\sin^2\left( \frac{t\Delta}{2} \right) d\Delta = \intf \delta(0)f(\Delta) d\Delta
\end{equation}
Questo ci dice che nel caso limite in cui t sia infinitamente grande:

\begin{equation}
\frac{4}{\Delta}\sin^2\left( \frac{t \Delta}{2} \right) \rightarrow 2 \pi t \delta(\Delta) = 2 \frac{\pi t}{\hbar} \delta(\Delta E)
\end{equation}
Solitamente anzichè calcolare la probabilità con la dipendenza esplicita dal tempo, si usa calcolare la
probabilità per unità di tempo $ \Gamma_{k\rightarrow m} $ di una transizione da uno stato ad un altro. Questa formula è nota anche come
\emph{Regola d'oro di Fermi}:

\begin{equation}
\boxed{
	\Gamma_{k\rightarrow m} = \frac{2\pi}{\hbar}|\bra{\phi_m}M\ket{\phi_k}|^2 \delta(E_m^0 - E_k^0 \pm \hbar \omega)
	}
\end{equation}

\end{document}