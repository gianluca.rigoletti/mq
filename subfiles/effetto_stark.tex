\documentclass[../mq.tex]{subfiles}

\newcommand{\ee}[1]{\ensuremath{E_{#1}^{(0)}}}

\begin{document}
\newpage
\section{Effetto Stark}

Applichiamo la teoria delle perturbazioni non degenere e degenere al caso di un atomo di idrogeno posto in un campo elettrico esterno costante e diretto lungo l'asse z.

Ricordiamo l'hamiltoniana imperturbata degli atomi idrogenoidi:
\begin{equation}
H_0 = \frac{p^2}{2\mu} - \frac{Ze^2}{4\pi\epsilon_0 r}
\end{equation}
Le autofunzioni non perturbate sono $\phi_{n,l,m}$ (vedi \vref{eq:soluzione_generale_H}).

L'hamiltoniana perturbativa è:
\begin{equation}
\lambda H_1 = e\, \mathbf{\xi}\cdot \mathbf{r} = e\, \xi z
\end{equation}
dove $\xi$ indica il campo elettrico $E$, per non confonderlo con l'energia. La quantità $e\xi$ ha il ruolo di $\lambda$.

\subsection{Perturbazione dello stato fondamentale}
Lo stato fondamentale $\phi_{1,0,0}$ non è degenere, quindi possiamo applicare la teoria delle perturbazioni, la formula \vref{eq:Correziona al primo ordine} e \vref{eq:Correzione al secondo ordine}.

\subsubsection{Primo ordine}
\begin{equation}
E_{100}^{(1)} = e\xi \melem{\phi_{100}}{z}{\phi_{100}} = e\xi \,\int d^3 r |\phi_{100}|^2 z = 0
\end{equation}
L'integrale è 0 su qualsiasi armonica sferica per ragioni di parità. Infatti sappiamo che l'operatore Hamiltoniana commuta con l'operatore di parità nel caso dell'atomo di idrogeno (osserva \vref{eq:hamiltoniana_H_coordrad}), quindi tutti gli autostati sono pari o dispari, hanno parità ben definita.
Ovviamente la media di una coordinata, nel nostro caso $z$, su una funzione d'onda simmetrica da sempre 0.

Fisicamente un sistema che reagisce al primo ordine se sottoposto ad un campo elettrico viene associato a un momento di dipolo elettrico $d$. Il ragionamento fatto fin qui mostra che l'atomo di idrogeno non possiede momento di dipolo elettrico intrinseco. Ovviamente se opportunamente stimolato dal campo elettrico esterno l'atomo potrà polarizzarsi per deformazione, ma non possiede dipolo se isolato.

Questo argomento si può generalizzare: \emph{un sistema in uno stato non degenere non può avere momento di dipolo intrinseco}.

La non-degenerazione è una condizione fondamentale. Infatti se ci troviamo in uno stato degenere dell'hamiltoniana, in genere lo stato è combinazione lineare di autostati della parità: non possiede quindi parità definita. In tal caso la media di una coordinata su quello stato non è necessariamente $0$.
Ad esempio se ci troviamo sull'autostato dell'hamiltoniana:
\begin{equation}
\ket{\psi} = \alpha \ket{\psi_+}+\beta\ket{\psi_-}
\end{equation}
dove $\ket{\psi_\pm}$ sono autostati della parità.
\begin{equation}
\avg{\alpha\psi_++\beta\psi_-}{z} = \alpha^*\beta \melem{\psi_+}{z}{\psi_-}+ \alpha\beta^*\melem{\psi_-}{z}{\psi_+}
\end{equation}
La media non va più a 0. Questo accade in alcune molecole in cui si hanno stati degeneri ma con parità opposta.

\subsubsection{Secondo ordine}
La correzione al secondo ordine si calcola con la formula \vref{eq:Correzione al secondo ordine}.
\begin{equation}\label{eq:pert_2_stark}
E_{100}^{(2)} = e^2\xi^2 \left\{ \sum_{n,l,m} \frac{|\melem{\phi_{nlm}}{z}{\phi_{100}}|^2}{E_1^{(0)}-E_n^{(0)}} + \sum_{k} \frac{|\melem{\phi_k}{z}{\phi_{100}}|^2}{E_1^{(0)}-\hbar^2k^2/2m}\right\}
\end{equation}
Il secondo termire della somma è riferito agli stato non legati dell'atomo dei idrogeno, che vanno inseriti per considerare tutto lo spazio degli stati dell'elettrone. Il problema è difficile da risolvere analiticamente, ma possiamo trovare un limite superiore della perturbazione. Scriviamo più genericamente la formula \ref{eq:pert_2_stark} utilizzando generici autostati di energia $E$, continua o discreta, $\ket{\phi_E}$:
\begin{equation}
E_{100}^{(2)} = e^2\xi^2 \sum_{E \neq E_1^{(0)} } \frac{|\melem{\phi_{100}}{z}{\phi_E}|^2}{E_1^{(0)}-E}
\end{equation}
Sappiamo che $E_1^{(0)}$ è l'energia più bassa perchè è quella del livello fondamentale. 
Quindi 
\begin{equation}
E-E_1^{(0)} \ge E_2^{(0)}- E_1^{(0)} \quad \rightarrow \quad \frac{1}{E-E_1^{(0)}} \le \frac{1}{E_2^{(0)}-E_1^{(0)}}
\end{equation}
Possiamo utilizzare questa disuglianza sulla somma totale:
\begin{equation}
-E_{100}^2 \le e^2\xi^2 \frac{1}{E_2^{(0)}-E_1^{(0)}} \sum_E \melem{\phi_{100}}{z}{\phi_E}\melem{\phi_E}{z}{\phi_{100}}
\end{equation}
Nella somma possiamo anche includere $E=E_1^{(0)}$ poichè il denominatore ora non diverge più e $\avg{\phi_{100}}{z}=0$.
Usiamo ora la relazione di completezza per i vettori $\ket{\phi_E}$. quindi 
\begin{equation}
\sum_E \melem{\phi_{100}}{z}{\phi_E}\melem{\phi_E}{z}{\phi_{100}} = \avg{\phi_{100}}{z^2} 
\end{equation}
Possiamo calcolare facilmente questo valore medio:
\begin{align}
\avg{\phi_{100}}{z^2} &= \avg{\phi_{100}}{x^2} = \avg{\phi_{100}}{y^2} \nonumber \\
\rightarrow\;&= \frac{1}{3}\avg{\phi_{100}}{r^2}
\end{align}
Inoltre sappiamo che:
\begin{equation}
\ee{2} - \ee{1} = -\ume mc^2 \alpha^2 \left(\frac{1}{4} -1\right) = \frac{8}{3} m c^2 \alpha^2
\end{equation}
Quindi troviamo:
\begin{equation}
-E_{100}^{(2)} < \frac{8e^2\xi^2a_0^2}{3mc^2\alpha^2} = \frac{8}{3} (4\pi\epsilon_0 \xi^2)a_0^3
\end{equation}

Dimensionalmente il risultato ha senso, perchè $4\pi\epsilon_0 \xi^2 \times a_0$ ha le dimensioni di un campo elettrico al quadrato per un volume, quindi di un'energia. 
La costante calcolata con analisi analitiche esatte è $\frac{9}{4}$ invece di $\frac{8}{3}$, quindi la nostra approssimazione è buona.

\subsection{Stato $n=2$}
Lo stato $n=2$ dell'atomo di idrogeno possiede 4 stati degeneri:
\begin{equation}
\ket{2,0,0},\;\ket{2,1,1},\;\ket{2,1,0},\;\ket{2,1,-1}
\end{equation}
Per studiare la perturbazione al primo ordine dobbiamo utilizzare la formula \vref{eq:Primo ordine perturbazione degenere} che equivale a diagonalizzare la matrice che rappresenta la perturbazione sugli autovettori degeneri di $H_0$.

L'argomento di parità ci viene in aiuto nel calcolo dei generici $\melem{\phi_{2,l,m}}{z}{\phi_{2,l,m}}$. Infatti tra due stati di uguale parità il valore medio di $z$ è zero. Inoltre la perturbazione $z$ commuta con $L_z$ quindi hanno valore diverso da $0$ solo gli elementi di matrice tra vettori aventi uguali $m$\footnote{Se commutano ricordiamo che un operatore è sempre invariante su un autospazio dell'altro, cioè se applico il secondo operatore su un vettore autostato del primo in generale si ottiene una combinazione di autovettori del primo operatore}.  

\subsubsection{Parità degli autostati di H}
Discutiamo brevemente la parità degli autostati dell'atomo di idrogeno. L'operatore di parità agisce in questo modo:
\begin{align}
\begin{cases}
\vec{r} \quad &\rightarrow \quad - \vec{r}  \\
\theta \quad &\rightarrow \quad \theta' = \pi - \theta \\
\varphi \quad &\rightarrow \quad \varphi' = \varphi + \pi
\end{cases}
\end{align}
Le funzioni d'onda sono espresse esplicitamente grazie ai polinomi di Legendre:
\begin{gather}
Y_{nlm} (\theta,\varphi) = P_l^m (\cos\theta)\,e^{im\varphi} \\
P_k^m (x) = f(l,m)(1-x^2)^{-m/2} \left(\cdv{x}{}{}\right)^{l-m}(1-x^2)^2
\end{gather}
Quindi quando applichiamo la trasformazione di parità:
\begin{equation}
P_m^l(-\cos\theta) \;\underset{\theta' = \pi - \theta}{\rightarrow} \;(-1)^{l-m} P_m^l (\cos\theta)
\end{equation}
e
\begin{equation}
e^{im\varphi} \; \underset{\varphi' = \varphi +\pi}{\rightarrow}\; (-1)^m e^{im\varphi}
\end{equation}
Otteniamo infine:
\begin{equation}
Y_{nlm} (\pi -\theta, \varphi+\pi) = (-1)^l Y_{nlm}(\theta,\varphi)
\end{equation}
Gli autostati con $l$ pari sono pari, quelli con $l$ dispari sono dispari. Quindi tutti gli autostati di $H$ sono anche autostati dell'operatore di parità.

\subsubsection{Primo ordine}
Calcoliamo gli elementi di matrice della hamiltoniana perturbata ricordando che:
\begin{equation}
\avg{\phi_{2,1,\pm1}}{z}=0
\end{equation}
a causa della parità. Possiamo quindi considerare solo la sottomatrice in alto a sinistra, scrivendo l'equazione \vref{eq:Primo ordine perturbazione degenere} in rappresentazione matriciale:
\begin{equation} \label{eq:matrice_stark2}
e\xi \begin{pmatrix}
\avg{\phi_{2,0,0}}{z} & \melem{\phi_{2,0,0}}{z}{\phi_{2,1,0}} \\
\melem{\phi_{2,1,0}}{z}{\phi_{2,0,0}} & \avg{\phi_{2,1,0}}{z}
\end{pmatrix}\colvec{\alpha_1}{\alpha_2} = E^{(1)}\colvec{\alpha_1}{\alpha_2}
\end{equation}
E' evidente che anche la diagonale di questa matrice è 0 per la parità. I due termini rimasti sono uno il complesso coniugato dell'altro, essendo la matrice hermitiana.
Dobbiamo solo calcolare:
\begin{align}
\melem{\phi_{2,0,0}}{z}{\phi_{2,1,0}} = \int_0^{\+infty} r^2 dr \frac{2r}{\sqrt{3}a_0}(2a_0)^{-3} \left( 1- \frac{r}{2a_0}\right) \,e^{-r(a_0)}\,\cdot\nonumber \\
\cdot\, r \sqrt{\frac{4\pi}{3}} \int_0^{+\infty} d \varOmega Y_{00}^* Y_{10}Y_{10}
\end{align}
dove si è utilizzato il fatto che:
\begin{equation}
z = r\,\sqrt{\frac{4\pi}{3}}\;Y_{10}
\end{equation}
Sviluppando il calcolo e utilizzando l'integrale noto:
\begin{equation}
\int_0^{+\infty} x^m e^{-\beta x^n} dx = \frac{\Gamma(\gamma)}{n \beta^\gamma} \quad \text{con} \; \gamma = \frac{m+1}{n}
\end{equation}
e sapendo che $\Gamma(n) = (n-1)!$ se $n$ è intero, calcoliamo:
\begin{align}
&= \frac{1}{16\pi a_0^4} \int dr r^4 \left(1-\frac{r}{2a_0}\right) e^{-r/a_0} \int_{-1}^{1}d\cos\theta \cos^2\theta \int d\varphi \nonumber \\
&= \frac{1}{12a_0^4} a_0^5 \int_0^{+\infty} dx x^4 \left(1-x\right)e^{-x} \nonumber \\
&= \frac{a_0}{12} (\Gamma(5)-\Gamma(6)) \nonumber \\
&= -3a_0
\end{align}

Quindi \vref{eq:matrice_stark2} diventa:
\begin{equation}
e\xi
\begin{pmatrix}
0 & -3a_0 \\
-3a_0 & 0 
\end{pmatrix}\colvec{\alpha_1}{\alpha_2} = E^{(1)}\colvec{\alpha_1}{\alpha_2}
\end{equation}
Diagonalizzandola si ottiene:
\begin{align}
E^{(1)}= +3e\xi a_0  \qquad &  \ket{\psi}= \frac{1}{\sqrt{2}} (\phi_{200} - \phi_{210}) \\
E^{(1)} = -3e\xi a_0 \qquad & \ket{\psi} = \frac{1}{\sqrt{2}} (\phi_{200} + \phi_{210})  \\
E^{(1)} = 0 \qquad & \phi_{2,1,\pm 1}
\end{align}
Abbiamo quindi dei nuovi autovettori base di $n=2$, sui quali abbiamo trovato la perturbazione al primo ordine dell'energia. 

\subsubsection{Osservazioni}
\begin{itemize}
	\item I nuovi autostati non sono più autostati di $\mathbf{L^2}$ ma lo sono ancora di $\mathbf{L_z}$. La ragione di questo fatto è che la hamiltoniana perturbata non commuta con $\mathbf{L^2}$ perchè la perturbazione crea una direzione privilegiata e quindi il sistema non è più invariante per rotazione. Ma siccome questa direzione è parallela all'asse $z$, allora $L_z$ rimane una costante del moto.
	\item In generale quando diagonalizziamo una perturbazione, i nuovi autostati sono la sovrapposizione di autostati della hamiltoniana non perturbata con diversi numeri quantici. Per questo motivo la perturbazione è in grado a volte di separare degli stati degeneri.
	\item Possiamo generalizzare la procedura di diagonalizzazione della perturbazione. Se $H_0$ è diagonale, ma $H_1$ no e i due operatori non commutano, allora non possiamo diagonalizzare $H_1$ senza modificare $H_0$. Ma se lavoriamo solamente su un set di autostati degeneri allora $H_0 = \lambda \mathbf{Id}$ e $H_1$ commuta sicuramente con $\mathbf{Id}$ , quindi possiamo diagonalizzarla separatamente da $H_0$. 
	\item I primi due termini dello sviluppo della perturbazione del livello fondamentale descrivono bene il nostro sistema, ma la serie di tutti i termini dello sviluppo dell'energia diverge. 
\end{itemize}

\end{document}