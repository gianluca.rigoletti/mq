\documentclass[../mq.tex]{subfiles}
\begin{document}


\newpage
\section{Spin}\label{sec:spin}

Lo spin rappresenta un concetto che in meccanica quantistica non ha un'analogia con la meccanica classica. Si tratta di un nuovo grado di libertà che caratterizza le particelle (per ora ci siamo occupati solo di elettroni, ma lo spin caratterizza anche particelle subatomiche) come può esserlo la massa, il momento angolare, l'energia etc. L'evidenza sperimentale dello spin era nota si da quando si facevano passare atomi di elementi alcalini o atomi idrogenoidi (per esempio l'Argento) in campi magnetici e si osservavano dei doppietti relativi al momento angolare, che non erano spiegabili con l'utilizzo di numeri interi nei numeri quantici $ l $ e $ m $. Se osserviamo un doppietto possiamo scrivere, ricordando le relazioni tra i numeri quantici del momento angolare e indicando con $ s $ il nuovo grado di libertà:

\begin{align}
(2s +1) &= 2 \nonumber \\
s &= \frac{1}{2} \nonumber \\
\end{align}

Lo spin, caratterizzato da $ s $, è quindi un grado di libertà che descrive la funzione d'onda di una particella. D'ora in poi avremo:

\begin{equation}
\psi(x,t)_{n,l,m,s}
\end{equation}

Analogamente al momento angolare, possiamo definire gli operatori $ S_x, S_y, S_z $ che seguono le relazioni:

\begin{equation}
[S_i, S_j] = i\hbar\epsilon_{i,j,k}S_k
\end{equation}

Così come ad un fissato $ l $ dell'operatore $ L^2 $ esistevano $ (2l+1) $ valori di $ m $ per l'operatore $ L_z $, così al valore di $ s = 1/2 $ esistono due valori che l'operatore $ S_z $ può assumere che sono $ \pm \hbar\;1/2 $.
Scrivendo l'operatore $ S_z $ in forma matriciale otteniamo:

\begin{equation}
S_z = \hbar 
\begin{pmatrix}
\frac{1}{2} & 0 \\
0           & -\frac{1}{2} \\
\end{pmatrix}
\end{equation} 

E analogamente si possono definire gli operatori $ S_+ $ e $ S_- $ che in forma matriciale vengono rappresentati come:

\begin{equation}
S_+  = \hbar \begin{pmatrix}
0 & 1 \\
0 & 0 \\
\end{pmatrix}
\qquad
S_- = \hbar
\begin{pmatrix}
0 & 0 \\
1 & 0 \\
\end{pmatrix}
\end{equation}

Da cui si possono ricavare, utilizzando le stesse formule \vref{eq:operatori di innalzamento e abbassamento momento angolare}, i valori di $ S_x $ e $ S_y $.

\subsection{Rappresentazione con matrici di Pauli}

Esiste un'ulteriore rappresentazione, molto comoda, che fa utilizzo delle \emph{matrici di Pauli}. Possiamo scrivere che:

\begin{align} \label{eq:Operatore di Spin}
% Se qualcuno riesce a far uscire il grassetto su sigma vince una caramella
\mathbf{S} &= \frac{1}{2}\hbar\boldmath{\sigma} \nonumber \\
S_{x,y,z} &= \frac{1}{2}\hbar\sigma_{x,y,z}
\end{align}
Con:
\begin{equation}
\sigma_x = 
\begin{pmatrix}
0 & 1 \\
1 & 0 \\
\end{pmatrix}
\qquad
\sigma_y = 
\begin{pmatrix}
o & -i \\
i & 0 \\
\end{pmatrix}
\qquad
\sigma_z = 
\begin{pmatrix}
1 & 0 \\
0 & -1 \\
\end{pmatrix}
\end{equation}
Alcune proprietà delle matrici di Pauli che potranno essere utili più avanti sono le seguenti:

\begin{itemize}

\item
Obbediscono alla relazione di commutazione:
\begin{equation}
[\sigma_i,\sigma_j] = 2i\epsilon_{i,j,k}\sigma_k
\end{equation}

\item 
Il loro quadrato è una matrice identità:
\begin{equation}
\sigma_x^2 = \sigma_y^2 = \sigma_z^2 = 
\begin{pmatrix}
1 & 0 \\
0 & 1 \\
\end{pmatrix}
\end{equation}

\item
Obbediscono alla relazione di anticommutatività:
Definito l'operatore anticommutatore come:
\begin{equation}
\{\sigma_i,\sigma_j\} = \sigma_i \sigma_j + \sigma_j \sigma_i
\end{equation}
Allora le matrici di Pauli seguono la regola:

\begin{align}
\{\sigma_x,\sigma_y\} &= 0 \nonumber \\
\{\sigma_y,\sigma_z\} &= 0 \nonumber \\
\{\sigma_z,\sigma_x\} &= 0
\end{align}

\'E importante notare che queste relazioni di anticommutatività valgono nel caso
specifico delle particelle con spin $ 1/2 $ e non in generale.
\end{itemize}

\subsection{Autovalori e Autostati}

Abbiamo quindi visto che particelle come l'elettrone sono caratterizzate da spin semi-intero di valore $ 1/2 $. Esistono tuttavia delle particelle che possiedono spin intero; di conseguenza,
la loro rappresentazione matriciale non sarà più limitata ad una matrice $ 2\times 2 $ ma
al corrispondente valore $ 2s+1 \times 2s+1 $. In queste dispense ci occuperemo quasi esclusivamente 
dell'elettrone come particella, quindi i nostri discorsi saranno sempre riferiti a valori di
spin semi-interi.

Sappiamo che il valore dello spin $ S = 1/2 $ e che di conseguenza i valori che può assumere $ S_z $ sono legati dalla relazione $ -S \leq S_Z \leq +S $, da cui possiamo scrivere che gli
autovalori dell'operatore $ S_z $ sono $ \pm 1/2 \hbar $. 
Per quanto riguarda gli autostati, invece, questi saranno rappresentati da un vettore di due 
componenti, per cui vale l'equazione agli autovalori:

\begin{equation}
S_z\colvec{u}{v} = \pm\frac{\hbar}{2}\colvec{u}{v}
\end{equation}
Risolviamola facendo uso delle matrici di Pauli:
\begin{align}
\frac{\hbar}{2}\sz\colvec{u}{v} &= \pm\frac{\hbar}{2}\colvec{u}{v} \nonumber \\
\sz\colvec{u}{v} &= \pm \colvec{u}{v} \nonumber \\
\colvec{u}{-v} &= \pm\colvec{u}{v}
\end{align}
La soluzione con segno positivo dà:
\begin{equation}
\colvec{u}{-v} = \colvec{u}{v} \qquad \Rightarrow \qquad u = 1,\; v = 0
\end{equation}
Mentre quella con segno negativo: 
\begin{equation}
\colvec{u}{-v} = \colvec{-u}{-v} \qquad \Rightarrow \qquad u = 0,\; v = 1
\end{equation}

Gli autostati dello spin vengono chiamati \emph{autospinori} e vengono rappresentati in diversi modi:

\begin{align}
\colvec{1}{0} = \irchi{}{}_+ = \; \uparrow \nonumber \\
\colvec{0}{1} = \irchi{}{}_- = \; \downarrow \nonumber \\
\end{align}
In questa dispensa utilizzeremo la notazione $ \uparrow \downarrow $ perché più compatta. Ove 
necessario ci serviremo delle altre rappresentazioni.

Abbiamo quindi trovato gli autospinori. Questo ci dice che un generico stato (detto \emph{spinore}) può essere decomposto in termini degli autospinori:

\begin{equation} \label{eq:Decomposizione in autospinori}
\psi(x) = \colvec{\alpha}{\beta} = \alpha\colvec{1}{0} + \beta\colvec{0}{1} = \alpha\uparrow + \beta\downarrow
\end{equation}

Inoltre, gli autospinori rappresentano un sistema ortonormale completo, obbedendo al postulato 
di espansione secondo cui $ |\alpha|^2 $ e $ |\beta|^2 $ rappresentano la probabilità che
una misura di $ S_z $ restituisca $ \pm\hbar/2 $. Vale quindi la relazione:

\begin{equation}
|\alpha|^2 + |\beta|^2 = 1
\end{equation}

Ovviamente, la scelta di $ \uparrow $ e $ \downarrow $ è del tutto arbitraria. Possiamo trovare
anche degli autospinori che non siano canonici, ma che siano comunque una base ortonormale. Un esempio può essere il seguente:

\begin{equation}
\uparrow = \frac{1}{\sqrt{2}}\colvec{e^{-i\phi/2}}{e^{i\phi/2}} \qquad \downarrow = \frac{1}{\sqrt{2}}\colvec{e^{-i\phi/2}}{-e^{i\phi/2}}
\end{equation}

\'E sempre possibile moltiplicare un autospinore per una fase arbitraria $ e^{i\phi} $ senza variarne la sua natura.

\subsection{Valore d'aspettazione}
Abbiamo definito l'operatore di spin come in \vref{eq:Operatore di Spin}. Essendo un operatore,
dato un generico stato $ \bra{\alpha} $, possiamo calcolare il suo valore d'aspettazione 
$\langle\mathbf{S}\rangle $:

\begin{align}
\langle\mathbf{S}\rangle = \bra{\alpha}\mathbf{S}\ket{\alpha} &= \sum_i\sum_j \braket{\alpha}{i}\bra{i}\mathbf{S}\ket{j}\braket{j}{\alpha} \nonumber \\
&= (\alpha^*_-, \alpha^*_+)\mathbf{S}\colvec{\alpha_-}{\alpha_+}
\end{align}

L'operatore $ \mathbf{S} $ è costituito dalle 3 componenti $ S_x, S_y, S_z $. Di conseguenza, per
calcolare il valore d'aspettazione dell'operatore di spin, calcoliamo direttamente 
i valori d'aspettazione delle tre componenti:

\begin{equation}
\langle\mathbf{S}\rangle = (\langle S_x\rangle,\langle S_y\rangle, \langle S_z\rangle)
\end{equation}
\begin{align}
\langle S_x\rangle &= (\alpha^*_-, \alpha^*_+) \frac{1}{2}\hbar \sx \colvec{\alpha_-}{\alpha_+} = \frac{1}{2} \hbar (\alpha_+^* \alpha_- + \alpha_-^* \alpha_+) \nonumber \\
\langle S_y\rangle &= (\alpha^*_-, \alpha^*_+) \frac{1}{2}\hbar \sy \colvec{\alpha_-}{\alpha_+} = -\frac{i\hbar}{2} (\alpha_+^* \alpha_- - \alpha_-^*\alpha_+)   \nonumber \\
\langle S_z\rangle &= (\alpha^*_-, \alpha^*_+) \frac{1}{2}\hbar \sz \colvec{\alpha_-}{\alpha_+}
= \frac{1}{2} \hbar(|\alpha_+|^2 - |\alpha_-|^2)
\end{align}

\subsection{Momento magnetico intrinseco}
Una delle conseguenze dell'esistenza dello spin è l'esistenza di un momento magnetico di dipolo
intrinseco dell'elettrone. Ciò significa che in presenza di un campo esterno, lo spin tende 
ad orientarsi secondo la direzione del campo magnetico. Il motivo di questo comportamento è
da ricercare nell'equazione relativistica di Dirac, che tratteremo più avanti.
Per ora fidiamoci della formula e scriviamo la relazione che lega il momento magnetico $ \mathbf{M} $ con lo spin $ \mathbf{S} $:

\begin{equation}
\mathbf{M} = -\frac{eg}{2m}\mathbf{S}
\end{equation}
Dove $ g $ è una costante detta \emph{rapporto giromagnetico}. Il suo valore è molto vicino a 2.
Calcoliamo l'hamiltoniana dovuta alla sola presenza di un campo magnetico:

\begin{equation} \label{eq:Hamiltoniana spin campo magnetico}
H = -\mathbf{M}\cdot\mathbf{B} = \frac{eg}{2m}\mathbf{S}\cdot\mathbf{B} = \frac{eg\hbar}{4m}\mathbf{\sigma}\cdot\mathbf{B}
\end{equation}
Scriviamo l'equazione di Schr\"odinger per questa hamiltoniana
\begin{align} \label{eq:Schrodinger con momento magnetico}
i\hbar\frac{d}{dt}\psi(t) &= H\psi \nonumber \\
i\hbar\frac{d}{dt}\psi(t) &= \frac{eg\hbar}{4m}\mathbf{\sigma}\cdot\mathbf{B}\psi(t)
\end{align}
Ricordando che possiamo scomporre un generico stato come in \vref{eq:Decomposizione in autospinori} e moltiplicare per una fase arbitraria, scriviamo $ \psi(t) $ come:

\begin{equation}
\psi(t) = e^{i\omega t}\colvec{\alpha_+}{\alpha_-}
\end{equation}
Per comodità, prendiamo il campo magnetico $ \mathbf{B} $ orientato verso l'asse z, in modo da
semplificare il prodotto scalare.
Inseriamo la funzione d'onda in \vref{eq:Schrodinger con momento magnetico}:
\begin{align}
i\hbar\frac{d}{dt}\psi(t) &= \frac{eg\hbar}{4m}\mathbf{\sigma}\cdot\mathbf{B}\psi(t) \nonumber \\
i\hbar\frac{d}{dt}e^{i\omega t}\colvec{\alpha_+}{\alpha_-} &= \frac{eg\hbar}{4m}\mathbf{\sigma}\cdot\mathbf{B}e^{i\omega t}\colvec{\alpha_+}{\alpha_-} \nonumber \\
\omega\colvec{\alpha_+}{\alpha_-} &= \frac{egB}{4m} \sz \colvec{\alpha_+}{\alpha_-}
\end{align}
Osserviamo che si tratta di un'equazione agli autovalori del tipo $ \mathbf{A}v = \lambda v $, 
dove $ \lambda $ da trovare è la pulsazione angolare $ \omega $. Senza bisogno di diagonalizzare
la matrice, si può vedere facilmente che se sostituiamo:

\begin{equation}
\omega_0 = \pm \frac{egB}{4m}
\end{equation}
I corrispondenti autovettori dovranno necessariamente essere:
\begin{equation}
\colvec{\alpha_+}{\alpha_-}_+ = \colvec{1}{0} =\; \uparrow \qquad \colvec{\alpha_+}{\alpha_-}_- = \colvec{0}{1} =\; \downarrow
\end{equation}
Quello che vogliamo dimostrare è che lo spin precede attorno all'asse in cui è orientato il campo
magnetico (l'asse z nel nostro caso), con una precisa frequenza. Lo stato iniziale sarà:
\begin{equation}
\psi(0) = \colvec{a}{b}
\end{equation}
E la sua evoluzione temporale, decomposta secondo gli autospinori trovati, sarà
\begin{equation}
\psi(t) = \colvec{a(t)}{b(t)} = a(t)\uparrow + b(t)\downarrow = ae^{-i\omega_0 t}\colvec{1}{0} + be^{i\omega_0 t}\colvec{0}{1}
\end{equation}
\begin{equation}
\psi(t) = \colvec{ae^{-i\omega_0 t}}{be^{i\omega_0 t}}
\end{equation}
Ora, per far vedere che lo spin precede attorno all'asse z, supponiamo che al tempo $ t = 0 $, 
$ \psi $ sia un autostato di $ S_x $ con autovalore $ \hbar/2 $ e calcoliamone il valore d'aspettazione nel tempo $ \langle S_x \rangle_t $. Diciamo impropriamente che lo spin "punta"
nella direzione dell'asse x.

\begin{align}
\frac{\hbar}{2}\sx \colvec{a}{b} = \frac{\hbar}{2}\colvec{a}{b} \nonumber \\
\sx \colvec{a}{b} = \colvec{a}{b} \nonumber \\
\begin{cases}
b = a \\
a = b
\end{cases}
\end{align}
Di conseguenza l'autovettore normalizzato sarà:
\begin{equation}
\colvec{a}{b} = \frac{1}{\sqrt{2}}\colvec{1}{1}
\end{equation}
E la sua evoluzione temporale, invece:
\begin{equation}
\psi(t) = \frac{1}{\sqrt{2}}\colvec{e^{-i\omega_0 t}}{e^{i\omega_0 t}}
\end{equation}
Calcoliamo dunque il valore d'aspettazione $ \langle S_x \rangle_t $:
\begin{align}
\langle S_x \rangle_t \nonumber &= \frac{\hbar}{2}\frac{1}{\sqrt{2}}(e^{i\omega_0 t}, e^{-i\omega_0 t})\sx \frac{1}{\sqrt{2}}\colvec{e^{-i\omega_0 t}}{e^{i\omega_0 t}} \nonumber \\
&= \frac{\hbar}{4}(e^{i\omega_0 t}, e^{-i\omega_0 t})\colvec{e^{i\omega_0 t}}{e^{-i\omega_0 t}} \nonumber \\
&= \frac{\hbar}{2}\cos(2\omega_0 t)
\end{align}
Analogamente si calcola il valore d'aspettazione $t $ e si ottiene:

\begin{equation}
\langle S_y \rangle_t = \frac{\hbar}{2}\sin(2\omega_0 t)
\end{equation}

Possiamo quindi dedurre che lo spin precede attorno alla direzione del campo $ B $ con una frequenza di $ 2\omega_0 $. 
Tale frequenza è detta \emph{frequenza di ciclotrone}:

\begin{equation}
\omega_c := 2\omega_0 = \frac{egB}{2m}
\end{equation}

Abbiamo fatto i calcoli assumendo che lo stato fosse un autostato di $ S_x $ e quindi immaginando
lo spin giacente sul piano xy. Ovviamente si può calcolare la frequenza di ciclotrone anche nel
caso in cui lo stato non sia diagonale (e quindi lo spin forma un angolo $ \theta $ con il verso
del campo magnetico).

\subsubsection{Momento magnetico con visione di Heisenberg}
Utilizziamo l'equazione di Heisenberg per calcolare l'evoluzione temporale dell'operatore $ \mathbf{S} $:

\begin{equation}
\frac{d\mathbf{S}}{dt} = \frac{i}{\hbar}[H,\mathbf{S}(t)]
\end{equation}
con $ \mathbf{H} $ come in \vref{eq:Hamiltoniana spin campo magnetico}:
\begin{equation}
H = \frac{eg}{2m}\mathbf{S\cdot B} := \gamma \mathbf{S\cdot B}
\end{equation}
A questo punto, siccome
$ \mathbf{S} $ ha tre componenti, calcoliamo il valore della derivata rispetto al tempo componente
per componente, eseguendo il prodotto scalare.

\begin{align}
\frac{dS_x}{dt} &= \frac{i\gamma}{\hbar}[H,\mathbf{S}_x(t)] = \frac{i\gamma}{\hbar}[S_xB_x + S_yB_y + S_zB_z,S_x] \nonumber \\
&=  \frac{i\gamma}{\hbar}( B_x[S_x,S_x] + B_y[S_y,S_x] + B_z[S_z,S_x] ) \nonumber \\
&= \frac{i\gamma}{\hbar}( B_y[S_y,S_x] + B_z[S_z,S_x] ) \nonumber \\
&= \frac{i\gamma}{\hbar}( -i\hbar B_yS_z + i\hbar B_zS_y ) \nonumber \\
&= \gamma(\mathbf{B\times S})_x
\end{align}
Analogamente si può calcolare la componente lungo $ y $ e$ z $. Il risultato più generale dà:
\begin{equation}
\frac{d\mathbf{S}(t)}{dt} = \gamma(\mathbf{B\times S})
\end{equation}
Questa equazione è analoga all' equazione che descrive la precessione del momento angolare associato
ad un dipolo magnetico in un campo uniforme, quando questo non è orientato lungo l'asse in cui è orientato il campo. Da questa visione della precessione possiamo notare che essa è un fenomeno generale e non ristretto al solo caso di particelle con spin 1/2.

\end{document}