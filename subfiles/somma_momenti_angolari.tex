\documentclass[../mq.tex]{subfiles}
%macro for spin-total-vector in the generic form
\newcommand{\sptv}{\ensuremath{\ket{\varepsilon_1\,,\varepsilon_2}}}

\begin{document}
\newpage
\section{Somma di momenti angolari}

\subsection{Momento angolare totale}
Consideriamo ora un sistema costituito da più particelle. La fisica classica afferma che il momento angolare totale rispetto a un punto di riferimento $O$ è dato dalla somma dei momenti angolari delle singole componenti del sistema.
\[L = \sum_{i=1}^{n}L_i\]
La variazione nel tempo di questa quantità è causata dal momento delle forze esterne al sistemo rispetto al punto $O$. Se il sistema è isolato o le forze esterne sono di tipo centrale, il momento angolare totale del sistema si conserva, possiamo dire che commuta con l'Hamiltoniana. 
I singoli momenti angolari delle particelle invece non sono più in generale costanti del moto, ma possono variare a causa del momento applicato dalle altre particelle su di esse.

In meccanica quantistica ci troviamo di fronte allo stesso problema: se consideriamo un sistema fatto da più di una particella in generale il momento angolare delle singole particelle non è più conservato. Infatti, senza entrare nei dettagli, potremmo mostrare che se l'hamiltoniana dipende dall'interazione delle due particelle i singoli operatori di momento angolare non commutano più con l'hamiltoniana totale. 

Definendo, in analogia con il caso classico ma utilizzando gli operatori, un momento angolare totale \[J = L_1 + L_2\] in caso di sistema isolato, esso commuta con l'hamiltoniana ed è una costante del moto.
L'importanza del momento angolare totale sta nel fatto che le basi degli operatori momento angolare delle singole particelle non sono più utili perchè non commutano con l'hamiltoniana, rendendo molto difficili i calcoli. Invece è possibile trovare una base comune per $J^2$,$J_z$,$H$, poichè essi commutano.

Lo scopo di questa sezione è quindi quello di formare un nuovo \emph{complete set of commuting osservable} per il sistema fisico:
\begin{equation}
\{L_1^2\,,L_{1z}\,,L_2^2\,,L_{2z}\} \quad \rightarrow \quad \{L_1^2\,,L_2^2\,,J^2\,,J_z\}
\end{equation}
questo cambio di base ci permetterà di studiare molto più semplicemente i sistemi a più particelle.

La notazione per il momento angolare è stata mantenuta volutamente generica. Infatti considereremo prima la somma di due spin $1/2$, ma lo stesso metodo sarà applicato in seguito alla somma di due momenti angolari generici.

\subsection{Somma di due spin $1/2$}
\subsubsection{Spazio degli stati}
Consideriamo due particelle con spin $1/2$. Lo stato di ognuna di esse appartiene a uno spazio di spin $\xi_s$ costituito da due soli vettori: \[\xi_s = \{\spu\, , \spd \}\] 
Lo stato del sistema costituito dalle due particelle è il prodotto tensoriale dei due spazi:
\begin{equation}
\xi_S = \xi_{s1} \otimes \xi_{s2}
\end{equation}
e la sua base ortonomale sarà data dal prodotto tensoriale delle basi ortonormali dei due spazi di partenza
\begin{equation}\label{eq:base_momento_totale}
\{\,\ket{\varepsilon_1\,,\varepsilon_2}\,\} = \{\, \spuu,\;\spud,\;\spdu,\;\spdd\,\}
\end{equation}
dove $\varepsilon$ può solamente essere $\uparrow$ o $\downarrow$.
Ovviamente questi autovettori sono ancora autostati delle osservabili $S_1^2,\,S_{1z},\,S_2^2,\,S_{2z}$ che formano un set di osservabili commutanti anche sullo spazio $\xi_S$. Gli operatori relativi a differenti particelle \emph{commutano} perchè agiscono su spazi differenti \footnote{Infatti se calcoliamo $S_{1z}\,S_{2z}\sptv = \varepsilon_2 \, S_{1z}\,\sptv = \varepsilon_1 \varepsilon_2 \sptv = \varepsilon_2 \, S_{1z}\,\sptv = S_{2z}\,S_{1z}\, \sptv$. Quindi i due operatori commutano: $\comm{S_{1z}}{S_{z}}=0$ }

\begin{equation}
S_1^2 \sptv = S_2^2 \sptv = \frac{3}{4} \hbar^2 \sptv
\end{equation}
\begin{equation}
S_{1z} \sptv = \varepsilon_1 \frac{\hbar}{2} \sptv
\end{equation}
\begin{equation}
S_{2z} \sptv = \varepsilon_2 \frac{\hbar}{2} \sptv
\end{equation}
Vediamo che sono ancora autovettori. 

\subsubsection{S e regole di commutazione}\label{sec:S_comm}
Consideriamo il momento angolare di spin totale 
\begin{equation}
S = S_1 + S_2
\end{equation}
Studiamo le regole di commutazioni di questo operatore con il vecchio set di operatori $S_1^2,\,S_{1z},\,S_2^2,\,S_{2z}$ e con le sue componenti.
\begin{align}
\comm{S_x}{S_y} &= \comm{S_{1x}+S_{2x}}{S_{1y}+S_{2y}} \nonumber \\
&= \comm{S_{1x}}{S_{1y}} + \comm{S_{2x}}{S_{2y}} \nonumber \\
&= i\hbar S_{1z} + i\hbar S_{2z} \nonumber \\
&= i\hbar S_z
\end{align}
Le componenti del momento totale seguono quindi le consuete regole di commutazioni degli operatori di momento angolare di spin.

Ora calcoliamo $S^2$, ricordandoci che $S_1$ e $S_2$ commutano:
\begin{equation}
S^2 = (S_1+S_2)^2 = S_1^2 +S_2^2 +2S_1\cdot S_2
\end{equation}
calcoliamo tutti i termini:
\begin{align}
S_1\cdot S_2 &= S_{1x}S_{2x} +  S_{1y}S_{2y} + S_{1z}S_{2z} \nonumber \\
&= \frac{1}{4}(S_{1+}+S_{1-})(S_{2+}+S_{2-}) -\frac{1}{4}(S_{1+}-S_{1-})(S_{2+}-S_{2-}) +S_{1z}S_{2z}\nonumber \\
&= \frac{1}{2}(S_{1+}S_{2-}+S_{1-}S_{2+}) + S_{1z}S_{2z}
\end{align}
\begin{align}
S^2 &= S_1^2 + S_2^2 + 2(S_{1x}S_{2x} +  S_{1y}S_{2y} + S_{1z}S_{2z}) \nonumber \\
&= S_1^2 + S_2^2 + 2S_{1z}S_{2z} + S_{1+}S_{2-}+ S_{1-}S_{2+} \label{eq:S2}
\end{align}

Vediamo le regole di commutazione tra il nuovo operatore e i singoli momenti angolari:
\begin{equation}
\comm{S_z}{S_1^2}= \comm{S_{1z}+S_{2z}}{S_1^2}= \comm{S_{1z}}{S_1^2} = 0 = \comm{S_z}{S_2^2}
\end{equation}
poichè sappiamo già che $S_1$ e tutte le sue componenti commutano con $S_1^2$. Inoltre sappiamo che anche $L_{1+}$ e $L_{1-}$ commutano con $S_1^2$ quindi possiamo concludere immediatamente che:
\begin{equation}
\comm{S^2}{S_1^2} = \comm{S^2}{S_2^2}= \comm{S_1^2 +S_2^2 +2 S_{1z}S_{2z} + S_{1+}S_{2-}+ S_{1-}S_{2+}}{S_1^2}= 0 
\end{equation}

E' evidente che $S_z$ commuti con le componenti z dei singoli momenti angolari:
\begin{equation}
\comm{S_z}{S_{1z}} = \comm{S_{1z}+ S_{2z}}{S_{1z}} = 0 = \comm{S_z}{S_{2z}} 
\end{equation}

Infine vediamo come interagiscono momento totale e componente z dei singoli momenti:
\begin{align}\label{eq:comm_S2,S_1z}
\comm{S^2}{S_{1z}} &= \comm{S_1^2+ S_2^2 + 2S_1\cdot S_2 }{S_{1z}} \nonumber\\
&= 2 \comm{S_1\cdot S_2}{S_{1z}} \nonumber \\
&= 2 \comm{S_{1x}S_{2x}+S_{1y}S_{2y}}{S_{1z}} \nonumber \\
&= 2i\hbar (-S_{1y}S_{2x}+S_{1x}S_{2y})
\end{align}
\begin{align}
\comm{S^2}{S_{2z}} &= \comm{S_1^2+ S_2^2 + 2S_1\cdot S_2 }{S_{2z}} \nonumber \\
&= 2 \comm{S_1\cdot S_2}{S_{2z}} \nonumber \\
&= 2 \comm{S_{1x}S_{2x}+S_{1y}S_{2y}}{S_{2z}} \nonumber \\
&= 2i\hbar (-S_{1x}S_{2y}+S_{1y}S_{2x1})
\end{align}

Possiamo ora calcolare facilmente il commutatore tra gli operatori di momenti angolare totale:
\begin{align}
\comm{S^2}{S_z} &= \comm{S^2}{S_{1z}} + \comm{S^2}{S_{2z}} \nonumber\\
&= 2i\hbar (-S_{1y}S_{2x}+S_{1x}S_{2y}) + 2i\hbar (-S_{1x}S_{2y}+S_{1y}S_{2x1}) = 0
\end{align}
Come previsto $S^2$ e $S_z$ si comportano correttamente come operatori di momento angolare.


\subsubsection{Cambio di base}
Se consideriamo l'insieme di operatori
\begin{equation}\label{eq:osservabili_momento}
\{\,S_1^2,\,S_2^2,\,S_{1z},\,S_{2z}\,\}
\end{equation} conosciamo una base di autovettori ortonormali in comune ad essi, \vref{eq:base_momento_totale}.
Ora vogliamo trovare una base per il set completo di operatori 
\begin{equation}\label{eq:osservabili_momento_totale}
\{\,S_1^2,\,S_2^2,\,S^2,\,S_z\,\}
\end{equation}
che non sarà uguale alla precedente perchè alcuni operatori non commutano, vedi \vref{eq:comm_S2,S_1z}. Vogliamo trovare l'espressione della nuova base in funzione ai vettori della vecchia.

Scriveremo la nuova base di autovettori nella forma $\ket{S,\,M}$ \footnote{Non confondiamo $S$ autovalore con $\mathbf{S}$ operatore: in genere il constesto è sufficiente per capire di quale oggetto si tratta.}, senza scrivere esplicitamente gli autovalori di $S_1^2$ e $S_2^2$ perchè sono sempre costanti e pari a $\frac{3}{4} \hbar^2$ poichè stiamo considerando due particelle di spin $1/2$.
Sappiamo inoltre che $S^2$ è un momento angolare quindi ci aspettiamo che l'autovalore $S$ potrà avere solo valori interi o semi-interi, e che $M$ sarà intero e compreso tra $-S$ e $S$. 

\begin{equation}
S^2 \ket{S,M}  = \hbar S(S+1) \ket{S,M} 
\end{equation}
\begin{equation}
S_z \ket{S,M} = \hbar M \ket{S,M}
\end{equation}
\begin{equation}
S_1^2 \ket{S,M} = S_2^2 \ket{S,M} = \hbar^2 \frac{3}{4} \ket{S,M}
\end{equation}

Vediamo subito che tutti i vettori dello spazio \ket{S,M} sono autovettori di $S_1^2$ e $S_2^2$.

\subsubsection{Autovalori di $S_z$} 
Poichè $S_z$ commuta con tutti gli operatori in \vref{eq:osservabili_momento} tutti i vettori della base $\{\sptv\}$ (\vref{eq:base_momento_totale}) devono essere sui autovettori. Verifichiamolo \footnote{Ricordiamo che $\varepsilon$ può essere $\uparrow$ o $\downarrow$ e quindi valere $\pm 1$}:
\begin{equation}
S_z \sptv = (S_{1z}+S_{2z})\sptv = \frac{\hbar}{2} (\varepsilon_1 +\varepsilon_2)\sptv
\end{equation}

Quindi i valore che può assumere $M$ sono:
\begin{equation}
M = \ume (\varepsilon_1 +\varepsilon_2) = \{1,0,-1\}
\end{equation}
Analizziamo meglio gli autovalori e i loro autovettori associati.

\begin{description}
	\item[$\mathbf{M=1}$] Questo valore di $M$ può essere ottenuto solamente con un vettore della base \vref{eq:base_momento_totale}: \spuu. Quindi non c'è degenerazione in questo caso.
	\item[$\mathbf{M=0}$] Questo valore può essere ottenuto in due modi: \spud e \spdu. Quindi c'è degenerazione: due autovettori ortogonali danno lo stesso valore di $M$.
	\item[$\mathbf{M=-1}$] Questo valore si ottiene solo con \spdd, quindi non c'è degenerazione.	
\end{description}
Utilizzando quindi la base $\{\sptv\}$ l'operatore $S_z$ può essere espresso in forma matriciale come:
\begin{equation}
(S_z) = \hbar  \begin{pmatrix}
1 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & -1 
\end{pmatrix} 
\end{equation}

\subsubsection{Autovalori di $S^2$} 
Sappiamo già che $S^2$ non può essere diagonale se espresso nella base $\{\sptv\}$ poichè non commuta con $S_{1z}$ e $S_{2z}$. Troviamo comunque la sua espressione in forma matriciale in questa base per poi diagonalizzarla.

La forma generale di $S^2$ è quella data in \vref{eq:S2}:
\begin{equation}
S^2 = S_1^2 + S_2^2 + 2S_{1z}S_{2z} + S_{1+}S_{2-}+ S_{1-}S_{2+}
\end{equation}
Mostriamo il procedimento completo per il primo calcolo:
\begin{align}
S^2 \spuu &= S_1^2\spuu + S_2^2 \spuu + 2S_{1z}S_{2z}\spuu + S_{1+}S_{2-}\spuu + S_{1-}S_{2+}\spuu \nonumber \\
&= \frac{3}{4}\hbar^2\spuu + \frac{3}{4}\hbar^2\spuu +2(S_{1z}\spu)\otimes(S_{2z}\spu)     + (S_{1+}\spu)\otimes(S_{2-}\spu) +... \nonumber\\
&= \left(\frac{3}{4}\hbar^2 + \frac{3}{4}\hbar^2 \right)\spuu + 2\left(\frac{\hbar}{2}\spu\right)\otimes\left(\frac{\hbar}{2}\spu\right) + 0\otimes(\hbar \spd)+ (\hbar \spd)\otimes 0) \nonumber\\
&= \left(\frac{3}{4}\hbar^2 + \frac{3}{4}\hbar^2 \right)\spuu + \frac{\hbar^2}{2}\spuu \nonumber \\
&= 2 \hbar^2 \spuu
\end{align}
Abbiamo esplicitato nel calcolo il simbolo di prodotto tensore per mostrare come gli operatori che si riferiscono a diverse particelle agiscano solamente sulla propria parte del vettore: \[\ket{\varepsilon_1,\varepsilon_2}= \ket{\varepsilon_1}\otimes\ket{\varepsilon_2}\]

Riassumiamo i risultati per gli altri vettori:
\begin{align}
S^2\spud &= \left(\frac{3}{4}\hbar^2 + \frac{3}{4}\hbar^2 \right)\spud -\frac{\hbar^2}{2}\spud + \hbar^2 \spdu \nonumber \\
&= \hbar^2 [\, \spud + \spdu\,]
\end{align}
\begin{align}
S^2\spdu &= \left(\frac{3}{4}\hbar^2 + \frac{3}{4}\hbar^2 \right)\spdu -\frac{\hbar^2}{2}\spdu + \hbar^2 \spud \nonumber \\
&= \hbar^2 [\, \spdu + \spud\,]
\end{align}
\begin{align}
S^2\spdd &= \left(\frac{3}{4}\hbar^2 + \frac{3}{4}\hbar^2 \right)\spdd +\frac{\hbar^2}{2}\spdd \nonumber \\
&= 2\hbar^2\spdd
\end{align}
La rappresentazione matriciale sulla base $\{\sptv\}$ è quindi:
\begin{equation}
(S^2)= \hbar^2 \begin{pmatrix}
2 & 0 & 0 & 0 \\
0 & 1 & 1 & 0 \\
0 & 1 & 1 & 0 \\
0 & 0 & 0 & 2 \\
\end{pmatrix}
\end{equation}

I vettori \spuu e \spdd, già autovettori non degeneri di $S_z$, sono ovviamente autovettori di $S^2$, ma appartengono allo stesso autospazio degenere caratterizzato da autovalore $2$, cioè $S=1$.
I vettori \spud ,\spdu invece erano autovettori degeneri di $S_z$ e in $S^2$ danno luogo a un blocco di matrice non diagonale. Notiamo comunque che gli zeri della matrice di $S^2$ erano attesi: infatti i due operatori commutano quindi $S^2$ applicato ai vettori del sottospazio degenere di $M=0$ deve rimanere nel sottospazio.

Diagonalizziamo la sottomatrice di $S^2$, relativa ai vettori $\{\spud,\,\spdu\}$
\begin{equation}
(S^2)_0 = \hbar^2 \begin{pmatrix}
1 & 1 \\
1 & 1 
\end{pmatrix}
\end{equation}
L'equazione caratteristica e gli autovalori sono:
\begin{equation}
(1-\lambda)^2 -1 = 0 \quad \rightarrow \quad \lambda= S(S+1)= \begin{cases}
0 \\ 2
\end{cases} \quad
\rightarrow \quad S = \begin{cases}
0 \\ 1
\end{cases}
\end{equation}
Gli autovettori sono:
\begin{equation}
\frac{1}{\sqrt{2}}\,[\,\spud + \spdu\,] \quad \mbox{per autovalore  } 2\hbar^2
\end{equation}
\begin{equation}
\frac{1}{\sqrt{2}}\,[\,\spud - \spdu\,] \quad \mbox{per autovalore  } 0
\end{equation}
Quindi riassumento, $S^2$ ha due possibili autovalori $2\hbar^2$ e $0$. 
\begin{description}
	\item[$\mathbf{S=1}$] \footnote{Ricordiamo che gli autovalori di $S^2$ sono $\hbar^2 S(S+1)= 2\hbar^2$: indichiamo con $S$ numero quantico. } Questo valore è dato da tre autovettori, quindi è degenere: \spuu, \spdd, $\frac{1}{\sqrt{2}}\,[\,\spud + \spdu\,]$. L'operatore $S_z$ risolve ovviamente la degenerazione poichè $M=1,-1,0$ sui tre vettori.
	\item[$\mathbf{S=0}$] Questo valore si ottiene solo per un vettore, quindi non è degenere:
	$\frac{1}{\sqrt{2}}\,[\,\spud - \spdu\,]$. Il valore di $M$ su questo vettore è ovviamente 0 perchè $S=0$.
\end{description}

\subsubsection{Tripletto e singoletto di spin}
Riassumendo abbiamo trovato una base completa di autovettori ortonormali in comune per il set completo di operatori commutanti $\{\,S_1^2,\,S_2^2,\,S^2,\,S_z\,\}$:
\begin{align}
\ket{S,M} = \begin{cases}
\ket{1,1} = \spuu \\
\displaystyle \ket{1,0} = \frac{1}{\sqrt{2}}\,[\,\spud + \spdu\,] \\
\ket{1,-1} = \spdd \\ \\
\displaystyle \ket{0,0} = \frac{1}{\sqrt{2}}\,[\,\spud - \spdu\,]
\end{cases}
\end{align}

I vettori appartenenti allo spazio $S=1$ vengono detti \emph{tripletto} di spin. Il vettori appartenente a $S=0$ viene detto \emph{singoletto}.

\paragraph{Osservazioni}
I vettori appartenenti al tripletto sono \emph{simmetrici} rispetto allo scambio degli indici relativi alle due particelle. Se si scambio $\uparrow$ con $\downarrow$ si rimane nel tripletto. Invece il vettore del singoletto è \emph{antisimmetrico} rispetto allo scambio cioè cambia di segno. Queste conclusioni saranno utili nello studio di sistemi di particelle identiche.

\subsection{Somma di due momenti angolari generici}
\subsubsection{Spazio degli stati}
Generalizziamo il problema risolto per la somma di due sping $1/2$.
Consideriamo un sistema costituito dall'unione di due particelle: per ognuna delle due particelle conosciamo lo spazio degli stati $\xi_i$ e una base ortonormale completa $\{\,\ket{k_i,\,j_i,\,m_i}\,\}$ composta dagli autovalori in comune del momento angolare $J_i^2$ e $J_{iz}$. L'indice $i$ distingue le due particelle: sulla prima particella agiscono gli operatori di momento angolare $J_1^2$ e $J_{1z}$ e sulla secondo $J_2^2$ e $J_{2z}$. Ovviamente gli operatori relativi a spazi diversi commutano tra loro.

In entrambi gli spazi degli stati delle particelle valgono le consuete regole del momento angolare:
\begin{align}\label{eq:relazioni_gen_momento}
J_i^2 \ket{k_i,\,j_i,\,m_i} &= j_i(j_i +1 ) \hbar^2 \ket{k_i,\,j_i,\,m_i} \\
J_{iz} \ket{k_i,\,j_i,\,m_i} &= m_i\hbar\ket {k_i,\,j_i,\,m_i} \\
J_{i\pm} \ket{k_i,\,j_i,\,m_i} &= \hbar \sqrt{j_i(j_i+1)-m_i(m_i \pm 1)}\ket{k_i,\,j_i,\,m_i \pm 1}
\end{align}

Lo spazio degli stati del sistema di due particelle sarà il prodotto tensoriale dei due spazi
\begin{equation}\label{eq:spazio_stati_totale_momento}
\xi = \xi_1 \otimes \xi_2
\end{equation}

Una base per questo spazio  è il prodotto tensoriale delle due basi
\begin{equation}\label{eq:base_momento1}
\ket{k_1,k_2,j_1,j_2,m_1,m_2}  = \ket{k_1,j_1,m_1} \otimes \ket{k_2,j_2,m_2}
\end{equation}

Il numero quantico $k$ è presente perchè in generale $\{J_i^2,J_{iz}\}$ non costituiscono un sistema completo di osservabili commutanti (C.S.C.O.). E' presente un'altra osservabile $A$ (nel caso dell'atomo di idrogeno si utilizzava il numero quantico n, dal quale si ricavava l'autovalore dell'hamiltoniana, vedi \vref{sec:numero_quantici_H}), che commuta con gli operatori del momento e fornisce il numero quantico $k$. Questo significa che coppie $\{j_i,m_i\}$ possono corrispondere a più sottospazi con $k_i$ diverso. 

\paragraph{Spazi $\mathbf{\xi(k,j)}$}\label{sec:spazi_kj}
Per affrontare in generale il problema della somma di momenti angolari è utile considerare i sottospazi dello spazio degli stati: $\xi(k_i,j_i)$. Questo sottospazio è l'autospazio associato agli autovalori $k$ e $j$. Fissati $k$ e $j$, $\xi(k,j)$ ha sempre dimensione pari a $(2j+1)$: infatti la base di questo autospazio è l'insieme degli autovettori con $m$ uguale a $-j \le m \le j$. Lo spazio degli stati complessivo è la somma diretta di questi sottospazi:
\begin{equation}
\xi = \sum_{\otimes} \xi(k,j)   
\end{equation}
Utilizzare questi sottospazi è utile perchè possiede buone proprietà:
\begin{itemize}
	\item $\xi(k,j)$ è invariante rispetto all'azione dell'operatore $\mathbf{J}$ \footnote{Stiamo considerando $J=J_i$ operatore momento associato a una singola particella. Non il momento angolare totale! }. Infatti, controllando le relazioni \vref{eq:relazioni_gen_momento}, $J_z$ applicato a un vettore di $\xi(k,j)$ restituisce un vettore appartenente sempre a quello spazio perchè non modifica l'autovalore $l$ e nemmeno $k$ ovviamente. Anche $J_x$ e $J_y$ sono invarianti perchè lo sono $J_+$ e $J_-$. Di conseguenza ogni funzione $F(\mathbf{J})$ è invariante nello spazio $\xi(k,j)$, ad esempio $J^2$. Questa invarianza è molto utile perchè la rappresentazione matriciale di qualsiasi  $F(\mathbf{J})$ sulla base di $\xi$ costituita dalla somma delle basi di tutti gli $\xi(k,j)$ risulta a blocchi lungo la diagonale: si hanno elementi di matrice diversi da $0$ solamente all'interno dei sottospazi $\xi(k,j)$. Di conseguenza si può studiare la diagonalizzazione di qualsiasi $F(\mathbf{J})$ separatamente per ogni diverso blocco, rendendo il problema più semplice e scomponibile.
	
	\item  All'interno di ogni sottospazio $\xi(j,k)$ gli elementi di matrice di qualsiasi operatore $F(\mathbf{J})$ non dipendono da $k$. Infatti le relazioni \vref{eq:relazioni_gen_momento} mostrano che l'autovalore $k$ non rientra mai nel calcolo.
	Quindi la diagonalizzazione di ogni operatore $F(\mathbf{J})$ non solo può essere fatta a blocchi, ma non dipende dal valore di $k$ e può essere risolta allo stesso modo per ogni spazio con uguale $j$. 
\end{itemize}
Per tutti questi motivi spesso l'indice $k$ viene trascurato e si indicano i sottospazi $\xi(k,j)$ con $\xi(j)$, ricordando però che a seconda del sistema fisico questi sottospazi possono avere una molteplicità data dai diversi valori di $k$ possibili.
\vspace{10pt}

Tornando al problema della somma di momenti, se i due spazi degli stati delle particelle sono:
\begin{align}
\xi_1 &= \sum_\otimes \xi_1(k_1,j_1) \\
\xi_2 &= \sum_\otimes \xi_2(k_2,j_2)
\end{align}
Allora lo spazio totale è scomposto nella somma diretta degli spazi $\xi(k,j)$ totali:
\begin{equation}\label{eq:spazio_totale_momento_totale}
\xi = \sum_\otimes \xi(k_1,k_2,j_1,j_2)
\end{equation} 
\begin{equation}
\xi(k_1,k_2,j_1,j_2) = \xi_1(k_1,j_1)\otimes\xi_2(k_2,j_2)
\end{equation}
La dimensione dei sottospazi $\xi(k_1,k_2,j_1,j_2)$ è $(2J_1+1)(2J_2+1)$. Per le ragioni scritte sopra, questi sottospazi sono invarianti sotto l'azione degli operatori $J_1$ e $J_2$ e quindi sotto l'azione di ogni funzione di essi.

\subsubsection{J e regole di commutazione}
L'operatore di momento angolare totale è definito come:
\begin{equation}
J = J_1 +J_2
\end{equation}
Le relazioni di questo operatore con gli operatori delle singole particelle sono la generalizzazione di quelle viste per la somma di due spin nella sezione \vref{sec:S_comm}.  
Riassumiamole brevemente, anche se i calcoli sono identici al caso della somma di due spin: infatti si sono utilizzate solo relazioni generali del momento angolare.
\begin{equation}
\mathbf{J^2} = J_1^2 + J_2^2 + 2J_{1z}J_{2z} + J_{1+}J_{2-}+ J_{1-}J_{2+}
\end{equation}
Inoltre $J_z$ commuta con $\{J_{1z},J_{2z},J_1^2,J_2^2\}$ mentre $J^2$ commuta solamente 
con $\{J_1^2,J_2^2\}$.

\subsubsection{Cambio di base}
Come per lo spin, per risolvere il problema del momento angolare totale dobbiamo effetturare essenzialmente un cambio di base, cioè un cambio di C.S.C.O \footnote{Complete Set of Commuting Osservables}. 
La base dello spazio degli stati considerata fino ad ora (vedi \vref{eq:base_momento1}) è comoda per lo studio delle osservabili:
\begin{equation}\label{eq:csco_momento_parziale}
\{\;J_1^2,\,J_2^2,\,J_{1z},\,J_{2z}\;\}
\end{equation}
ma non per lo studio del momento angolare totale. Dobbiamo trovare una base completa di autovettori in comune delle osservabili:
\begin{equation}\label{eq:csco_momento_totale}
\{\;J_1^2,\,J_2^2,\,J^2,\,J_z\;\}
\end{equation}
ed esprimerla in funzione dei vettori della base precedente.

Sappiamo che lo spazio degli stati è somma diretta dei $\xi(k_1,k_2,j_1,j_2)$ (vedi \vref{eq:spazio_stati_totale_momento}): questi spazi sono globalmente invarianti rispetto a $J_1$ e $J_2$ e quindi rispetto a $J^2$, $J_z$,$J_\pm$.
Di conseguenza la rappresentazione matriciale degli operatori del nuovo C.S.C.O (\vref{eq:csco_momento_totale}) è a blocchi su $\xi$ e ogni blocco corrisponde a un sottospazio $\xi(k_1,k_2,j_1,j_2)$ di dimensione $(2j_1+1)(2j_2+1)$.  Inoltre il valore di $k$ non rientra nei calcoli relativi al momento angolare, quindi possiamo rimuoverlo dalla notazione: 
\begin{align}
\xi(k_1,k_2,j_1,j_2) &\equiv \xi(j_1,j_2) \\
\ket{k_1,k_2,j_1,j_2,m_1,m_2} &\equiv \ket{j_1,j_2,m_1,m_2} 
\end{align}

Possiamo quindi studiare ogni spazio $\xi(j_1,j_2)$ singolarmente e trovare una base di $\{\;J_1^2,\,J_2^2,\,J^2,\,J_z\;\}$ su di esso. I vettori di tale base si indicheranno come\footnote{Questo è uno dei casi in cui è importante non confondere $\mathbf{J}$ operatore e $J$ autovalore}:
\begin{equation}
\ket{j_1,j_2,J,M} \equiv \ket{J,M}
\end{equation}
tralasciando spesso $j_1$ e $j_2$.

Poichè l'applicazione di $J$ su $\xi(j_1,j_2)$ è invariante, è possibile scomporre $\xi(j_1,j_2)$ in somma diretta di spazi $\xi(k,J)$ (cfr \vref{sec:spazi_kj}): ciò equivale a trovare una base di autovettori di $J^2$ e $J_z$ all'interno dello spazio $\xi(j_1,j_2)$. $\xi(k,J)$ rappresenta l'insieme di tutti i vettori con autovettori $J$ e $k$ e ha dimensione $(2J+1)$. L'indice $k$ è presente perchè non conosciamo la degenerazione di $J$ all'interno di $\xi(j_1,j_2)$: potrebbe essere che ad uno stesso valore di $J$ corrispondano più sottospazi.
\begin{equation}
\xi(j_1,j_2) = \sum_\otimes \xi(k,J)
\end{equation} 

Il problema della somma dei momenti angolare si riduce quindi a trovare in quali sottospazi $\xi(k,J)$ si scompone ogni spazio $\xi(j_1,j_2)$: ciò equivale a trovare i possibili valori di $J$ e la loro degenerazione e di conseguenza una base di autovettori per $J^2$ e $J_z$ espansa sulla base \vref{eq:base_momento1}.


\subsubsection{Autovalori di $J_z$}
Consideriamo uno spazio $\xi(j_1,j_2)$. Assumiamo per ipotesi che $j_1 \ge j_2$.

 I vettori $\ket{j_1,j_2,m_1,m_2}$ sono già autovalori di $J_z$ perchè commuta con gli operatori della base \vref{eq:csco_momento_parziale}:
\begin{align}
J_z\,\ket{j_1,j_2,m_1,m_2} &= (J_{1z}+ J_{2z}) \,\ket{j_1,j_2,m_1,m_2} \nonumber\\
 &= \hbar(m_1+m_2) \,\ket{j_1,j_2,m_1,m_2} \nonumber \\
 & \hbar M \,\ket{j_1,j_2,m_1,m_2}
\end{align}
L'autovalore $M$ potrà quindi assumere i valori:
\begin{equation}
M = m_1+m_2 \quad \rightarrow \quad -(j_1+j_2) \le M \le (j_1+j_2)
\end{equation}

Chiamiamo $g(M_i)$ la degenerazione di $M$, cioè il numero di vettori con autovalore $M_i$.
Un modo molto facile per calcolare $g(M)$ per ogni valore di $M$ è disegnare su un piano cartesiano i possibili valori di $m_1$ e $m_2$, il primo sull'asse $x$ e il secondo sull'asse $y$. I punti del piano caratterizzati da $(x=m_1,\,y=m_2)$ rappresentano i possibili valori di $M$. Vediamo subito che tutti i possibili valori si dispongono regolarmente in un rettangolo di lati $2j_1$ e $2j_2$. Se ora tracciamo il fascio di rette parallele corrispondente all'equazione:
\begin{equation}
m_1 +m_2 = M \;\rightarrow \; x +y = M \; \rightarrow \; y = -x + M
\end{equation}

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{./img/somma_momenti}
\caption{Autovalore M}
\label{fig:somma_momenti}
\end{figure}


I punti intersecati da ognuna di queste rette sono quelli con uguale $M$, quindi per determinare $g(M)$ basta contare quanti punti attraversa ciascuna retta.

\begin{itemize}
	\item Il valore massimo di $M = j_1 +j_2$ è l'unico attraversato dalla prima retta. \begin{equation}
	g(j_1+j_2) = 1
	\end{equation} 
	
	\item Il valore $M=j_1 + j_2 -1 $ è individuato dalla seconda retta che attraversa due punti: $(j_1,j_2 -1),\,(j_1-1,j_2)$.
	\begin{equation}\label{eq:deg_M}
	g(j_1+j_2 -1) = 2
	\end{equation}
	
	\item La degenerazione di $M$ aumenta di 1 ad ogni retta, fino ad arrivare a quella che attraversa il vertice in basso a sinistra del rettangolo $(m_1 = j_1,\,m_2 = -j_2)$. Qui si ha la degenerazione massima. Il numero di punti attraversato da questa retta è:
	\begin{equation}
	g(j_1-j_2) = 2j_2 +1
	\end{equation}
	
	\item Proseguendo verso sinistra la degenerazione rimane uguale a $2j_2 +1$ fino alla retta che corrisponde a $M = -(j_1-j_2)$, quella che interseca il vertice in alto a sinistra del rettangolo $(m_1=-j_1,\,m_2 = j_2)$. 
	\begin{equation}
	g(M)= 2j_2 +1 \quad \mbox{per} \quad -(j_1 -j_2) \le M \le (j_1-j_2)
	\end{equation}
	
	\item Per simmetria poi la degenerazione decresce fino a 1 secondo la relazione:
	\begin{equation}
	g(-M) = g(M) \quad \mbox{per} \quad  -(j-1+j_2) \le M \le -(j_1-j_2)
	\end{equation}
\end{itemize}

Quindi i possibili valori di $M$ sono:
\begin{equation}
M = \{\;j_1+j_2,\;j_1+j_2-1,\;\ldots,\;j_1-j_2\,\;\ldots,;-(j_1-j_2),\;\ldots,\;-(j_1+j_2)\;\}
\end{equation}

\subsubsection{Autovalori di $J^2$ }
Prima di tutto notiamo che $M$ e $J$ sono interi se $j_1$ e $j_2$ sono entrambi semi-interi o entrambi interi.  $M$ e $J$ sono semi-interi se $j_1$ e $j_2$ sono uno intero e uno semi-intero. 

Analizziamo i possibili valori di $J$ osservando i valori possibili di $M$:
\begin{itemize}
	\item Il valore massimo di $M=j_1+j_2$ è associato ovviamente al valore massimo di $J=j_1 +j_2$. Siccome $g(j_1+j_2)=1$ e $M$ massimo non ha degenerazione,  c'è un solo sottospazio $\xi(k,J=j_1+j_2)$.
	
	\item  Passiamo al valore $M=j_1+j_2-1$: questo ha $g(j_1+j_2-1)=2$ grado di degenerazione. Ma uno (e uno solo) dei due vettori con questo valore di $M$ appartiene già allo spazio $\xi(k,J=j_1+j_2)$. Rimane quindi un solo vettore con $M=j_1+j_2-1$ e ad esso possiamo associare $J=j_1+j_2-1$ e quindi un sottospazio $\xi(k,J=j_1+j_2-1)$. 
\end{itemize}

Questo procedimento può essere reiterato per tutti i possibili valori di $M$, ma cerchiamo un metodo generale che ci permetta di capire se l'autovalore $J$ è degenere, cioè se abbiamo bisogno di un ulteriore indice k per distinguere i sottospazi $\xi(k,J)$ che formano lo spazio $\xi(j_1,j_2)$.

Indichiamo con $p(J)$ la degenerazione dell'autovalore $J$: questo valore indica quanti diversi indici $k$ servono per distinguere gli autospazi $\xi(k,J)$ con $J$ fissato.
$p(J)$ e $g(M)$ sono legati in modo molto semplice. Consideriamo un certo $M$, a questo autovalore corrisponde un vettore in ogni sottospaio $\xi(k,J)$ con $J\ge M$, quindi possiamo scrivere:
\begin{equation}
g(M) = p(J=|M|) + p(J=|M|+1)+ p(J=|M|+2) + \ldots
\end{equation}
Notiamo che se vogliamo trovare la degenerazione di $M+1$ a questo corrisponde un vettore in ogni spazio $\xi(k,J)$ con $J\ge |M|+1$. 
\begin{equation}
g(M+1) = p(J=|M|+1)+ p(J=|M|+2) + \ldots
\end{equation}
Quindi vediamo che:
\begin{align}
g(M) &= p(J=|M|) + g(M+1) \nonumber \\
\rightarrow \quad p(J=|M|) &= g(M)- g(M+1) \nonumber 
\end{align}
\begin{equation}
\boxed{p(J) = g(M=J) - g(M= J+1)}
\end{equation}
Applichiamo questo risultato generale per trovare la degenerazione dei vari valori di $J$ nella somma di momenti.
\begin{description}
	\item[$\mathbf{J>j_1+j_2}$]  per questi valori di $J$ ovviamente $g(M>j_1+j_2) = 0$ e quindi $p(J)=0$ e non si sono sottospazi con tale valore di $J$.
	\item[$\mathbf{J=j_1+j_2}$] Si ha che:
	\begin{equation}
	p(J=j_1+j_2) = g(j_1+j_2) - g(j_1+j_2+1) = g(j_1+j_2) = 1
	\end{equation}
	
	\item[$\mathbf{j_1-j2 \le J \le j_1+j_2}$] la forma generale dà sempre 1, perchè la degenerazione di $M$ aumenta di 1 quando $J$ diminuisce di 1. (Vedi \vref{eq:deg_M} e seguenti)
	
	\item[$\mathbf{J< j_1-j_2}$] per questi valore di $J$ sia ha che la degenerazione di $M$ è massima e costante quindi:
	\begin{equation} 
	p(J< j_1+j_2) = 2j_2 +1  - (2j_2 +1) = 0
	\end{equation}
	Non si hanno spazi $\xi(k,J)$ al di sotto di $J = j_1 - j_2$. Osserviamo che non andiamo nei valori negativi di $M$\footnote{Ricordiamo che abbiamo preso per convenzione $j_1 \ge j_2$} perchè $J$ è auvalore di $\mathbf{J^2}$ e può assumure solamente valori positivi, mentre $M$ va da $J$ a $-J$. 
\end{description}
I valori possibili per l'autovalore $J$ sono quindi:
\begin{equation}
J = \{\;j_1+j_2,\; j_1+j_2-1,\; \ldots ,\; |j_1-j_2|\;\}
\end{equation}
dove abbiamo considerato $j_1$ e $j_2$ generici. 

La nostra analisi della degenerazione di $J$ ha mostra che tutti i sottospazi $\xi(k,J)$ di $\xi(j_1,j_2)$ hanno dimensione 1. Quindi l'indice $k$ non è rilevante in questa discussione. Gli autovalori $J$ e $M$ sono sufficiente per individuare ogni vettore in $\xi(j_1,j_2)$, quindi $\mathbf{J^2}$ e $J_z$ costituiscono un C.S.C.O in $\xi(j_1,j_2)$

\subsection{Autofunzioni di $J_z$ e $J^2$}
Dopo aver trovato i possibili valori di $J$ e $M$, dobbiamo trovare l'espressione della base $\ket{J,M}$ sulla base $\ket{j_1,j_2,m_1,m_2}$. 
Invece di diagonalizzare la matrice di $J^2$ utilizziamo un metodo più operatoriale.

Sappiamo che lo spazio $\xi(j_1,j_2)$ si scompone in somma diretta sugli spazi $\xi(J)$:
\begin{equation}
\xi(j_1,j_2) = \xi(j_1+j_2) \otimes \xi(j_1+ j_2 -1) \otimes \ldots \otimes \xi(|j_1 - j_2|)
\end{equation}

Consideriamo il primo spazio $\xi(j_1+j_2)$. L'autovettore associato a $M=j_1+j_2$ è $\ket{j_1,j_2,m_1=j_1,m_2=j_2}$ ma $J^2$ commuta con $J_z$ e questo valore di $M$ non è degenere quindi questo autovettore è autovettore anche di $J^2$ e $J=j_1+j_2$. Quindi:
\begin{equation}
\ket{J = j_1+j_2,\,M = j_1+j_2}  = \ket{j_1,j_2,m_1,m_2}
\end{equation} 

Ora applicando l'operatore $J_-$ possiamo trovare tutti i vettori dello spazio $\xi(j-1+j_2$. Infatti questo operatore mantiene $J=j-1+j_2$ e fa variare $M$ come al solito.
\begin{align}
\ket{j_1+j_2,j_1+j_2-1} &= \frac{1}{\hbar \sqrt{2(j_1+j_2)}} \,J_-\, \ket{j_1+j_2, j_1+j_2}  \nonumber \\
&= \frac{1}{\hbar \sqrt{2(j_1+j_2)}}\,(J_{1z}+ J_{2z}) \ket{j_1,j_2, j_1,j_2}  \nonumber \\
&=  \frac{1}{\hbar \sqrt{2(j_1+j_2)}} \, [\hbar \sqrt{2j_1} \ket{j_1,j_2,j_1-1,j_2} + \hbar \sqrt{2j_2}\ket{j_1,j_2,j_1,j_2 -1}]
\end{align}
Quindi:
\begin{equation}\label{eq:autovet1}
\ket{j_1+j_2,j_1+j_2-1} = \sqrt{\frac{j_1}{j_1+j_2}} \ket{j_1,j_2,j_1-1,j_2} + \sqrt{\frac{j_2}{j_1+j_2}}\ket{j_1,j_2,j_1,j_2-1}
\end{equation}
Il procedimento può essere reiterato fino ad arrivare a $M=-(j_1+j_2)$.

Consideriamo ora lo spazio supplementare a $\xi(j_1+j_2)$ in $\xi(j_1,j_2)$.
\begin{equation}
\zeta(j_1+j_2) = \xi(j_1+j_2-1)\otimes \xi(j_q+j_2-2)\otimes \ldots
\end{equation} 

Consideriamo $M=j_1+j_2-1$, questo vettore ha $g(M)=2$, ma un vettore è già nello spazio $\xi(j_1+j_2)$, quindi rimane non degenere. Possiamo associare a $M=j_1+j_2-1$ il vettore con $J = j_1+j_2-1$.
\begin{equation}
\ket{j_1+j_1-1,\,j_1+j_2-1} 
\end{equation} 
Espandiamo questo vettore sulla base iniziale: a causa del valore di $M$ l'unica espansione possibile è:
\begin{equation}
\ket{j_1+j_1-1,\,j_1+j_2-1} = \alpha \ket{j_1,j_2,j_1,j_2-1}+\beta\ket{j_1,j_2,j_1-1,j_2}
\end{equation}
Questo vettore deve essere ortogonale a $\ket{j_1+j_2,\,j_1+j_2-1}$ (vedi \vref{eq:autovet1}) perchè appartiene allo spazio $\xi(j_1+j_2)$. 
Da questa condizione e dalla normalizzazione $|\alpha|^2+|\beta|^2 = 1$ e scegliendo la fase in modo tale che $\alpha$ e $\beta$ siamo reali e che $\alpha$ sia positivo, si ottiene:
\begin{equation}
\ket{j_1+j_1-1,\,j_1+j_2-1} =\sqrt{\frac{j_1}{j_1+j_2}} \ket{j_1,j_2,j_1,j_2-1}+\sqrt{\frac{j_2}{j_1+j_2}}\ket{j_1,j_2,j_1-1,j_2}
\end{equation}
Applicando successivamente $J_-$ possiamo trovare tutti i vettori del sottospazio $\xi(j_1+j_2-1)$.

Questo procedimento si può reiterare fino a $\xi(|j_1-j_2|)$. Si cerca la scomposizione del vettore di $J$ massimo in ogni spazio utilizzando la condizione di ortonormalità con i precedenti, poi si utilizza $J_-$ per trovare tutti gli altri vettori del sottospazio. Questo è il metodo più generico per trovare una base di autovettori per $J^2$ e $J_z$.



\end{document}