\documentclass[../mq.tex]{subfiles}
\begin{document}

\newpage
\section{Metodo Operatoriale}
In questa sezione introduciamo quella che è nota come \emph{notazione di Dirac} che fornisce un semplice ed elegante sistema di manipolazione degli stati e degli operatori nella meccanica quantistica.

Iniziamo con la nozione di stato: dai capitoli predenti abbiamo compreso che il termine può individuare l'n-esimo autostato di una particella in una scatola o una particella che si muove verso una barriera, etc. 
Gli stati sono descritti come vettori in uno spazio lineare e questi vettori vengono indicati con un \textbf{ket}: $ \ket{u_n} $ o $ \ket{p} $ oppure, se ci sono più stati di operatori che commutano tra di loro si può scrivere $ \ket{a,b} $. Ad ogni \textbf{ket} è associato un \textbf{bra}, che è il relativo vettore coniugato. 
Ad esempio, se abbiamo $ \ket{\psi} = \waf{} $ avremmo anche $ \bra{\psi} =\waf{}^* $. Definiamo il prodotto scalare tra un \textbf{bra} e un \textbf{ket} di due stati come:

\begin{equation}
\braket{\phi}{\psi} = \intf \phi(x,t)^*\waf{} \;dx
\end{equation}
Imponiamo inoltre le condizioni di sesquilinearità:
\begin{equation}
\braket{\phi}{\psi}^* = \braket{\psi}{\phi}
\end{equation}
\begin{equation}
\braket{\phi}{\alpha\psi_1 + \beta\psi_2} = \alpha\braket{\phi}{\psi_2} + \beta\braket{\phi}{\psi_2}
\end{equation}
Quando un generico operatore $ A $ agisce su un ket, restituisce un nuovo ket, cioè:
\begin{equation}
A\ket{\psi} = \ket{A\psi}
\end{equation}
Dove il membro a sinistra indica un operatore che agisce sullo stato iniziale, mentre la quantità a destra rappresenta il nuovo stato.
Questo ci permette di scrivere che:
\begin{equation}
\braket{\phi}{A\psi} = \bra{\phi}A\ket{\psi} = \intf \phi^*(x,t)A\waf{} \; dx
\end{equation}
In meccanica viene definito l'hermitiano coniugato $ A^\dagger $ dell'operatore $ A $ come quell'operatore per cui vale:
\begin{equation}
\intf (A\phi(x,t))^*\waf{} \; dx = \intf \phi^*(x,t)A^\dagger\waf{} \; dx
\end{equation}
Utilizzando la notazione di Dirac scriveremo che l'hermitiano coniugato è quell'operatore per cui vale:
\begin{equation}
\braket{A\phi}{\psi} = \bra{\phi}A^\dagger\ket{\psi}
\end{equation}
E seguirà ovviamente che
\begin{equation}
\bra{\phi}A^\dagger\ket{\psi}^* = \braket{A\phi}{\psi}^* = \braket{\psi}{A\phi} = \bra{\psi}A\ket{\phi}
\end{equation}
Concentriamoci adesso sul postulato di espansione della meccanica quantistica.
Abbiamo scoperto che, è possibile rappresentare un qualsiasi stato come combinazione lineare degli autostati relativi ad un operatore hermitiano. Un esempio è quello in cui abbiamo espanso lo stato di una particella in una scatola infinita in termini delle autofunzioni dell'operatore hamiltoniana. 
Le autofunzioni costituivano un set ortonormale. Se ci si sofferma un attimo il caso è analogo a quello di una base ortonormale in uno spazio vettoriale; infatti è possibile rappresentare qualsiasi vettore come una combinazione lineare dei vettori della base canonica. 
Dato un set ortonormale e completo, possiamo quindi rappresentare un generico stato $ \ket{\psi} $ come combinazione lineare degli autostati appartenenti al set ortonormale che indichiamo con $ \ket{u_n} $. Cioè:

\begin{equation} \label{eq:stato come combinazione lineare di sonc}
\ket{\psi} = \sum\limits_{n} C_n \ket{u_n}
\end{equation}
Per capire che quantità rappresentano i $ C_n $ osserviamo che vale dalla condizione  di ortonormalità:
\begin{equation}
\braket{u_n}{u_m} = \delta_{m,n}
\end{equation}
Segue che:
\begin{equation}\label{eq:coefficienti c_n}
\braket{u_m}{\psi} = \braket{u_m}{\sum C_n u_n} = \sum C_n \braket{u_m}{u_n} = C_n \delta_{m,n} = C_m
\end{equation}
Quindi i $ C_n $ sono i coefficienti che rappresentano la proiezione dello stato $ \psi $ su uno stato del set ortonormale $ u_n $. Se sostituiamo \vref{eq:coefficienti c_n} nella \vref{eq:stato come combinazione lineare di sonc} Otteniamo:

\begin{equation}
\ket{\psi} = \sum_n \ket{u_n}C_n = \sum_n \ket{u_n}\braket{u_n}{\psi}
\end{equation}
Siccome quest'equazione vale per un qualsiasi $ \ket{\psi} $, segue che:
\begin{equation}
\sum_n \ket{u_n}\bra{u_n} = \mathbf{1}
\end{equation}
Dove \textbf{1} è l'operatore unitario, cioè quell'operatore che agendo su uno stato non lo altera in alcun modo.
Questa relazione è nota come \emph{Relazione di completezza}.

Il discorso che abbiamo fatto finora è relativo ad un set numerabile di autostati $ u_n $. In meccanica quantistica, però, sappiamo che la posizione, essendo un'osservabile, è rappresentata dall'operatore hermitiano $ X $ che non avrà però un set numerabile di autostati, in quanto la posizione è una variabile continua. 
Questo ci dice che $ X $ avrà un set continuo di autostati $ \ket{x} $ i cui autovalori saranno indicati con $ x $:

\begin{equation}
X\ket{x} = x\ket{x}
\end{equation}
Ragionando come nel caso numerabile, possiamo espandere un generico stato $ \ket{\psi} $  come:
\begin{equation} \label{eq:espansione con indici continui}
\ket{\psi} = \intf C(x)\ket{x}\;dx
\end{equation}
Osserviamo che al posto di una somma abbia un integrale, visto che $ x $ varia in modo continuo.
La condizione di ortonormalità è:
\begin{equation}
\braket{x}{x'} = \delta(x-x')
\end{equation}
Per trovare i $ C(x') $ prendiamo la \vref{eq:espansione con indici continui} e moltiplichiamo per $ \bra{x'} $:

\begin{align}
\braket{x'}{\psi} &= \bra{x'}\intf C(x)\ket{x}\;dx = \intf C(x)\braket{x'}{x}\; dx = \nonumber \\
				  &= \intf C(x)\delta(x-x')\; dx \nonumber \\
				  &= C(x')
\end{align}
Sappiamo, dal postulato di espansione, che il modulo quadrato dei coefficienti rappresenta la probabilità di trovare lo stato proiettato nel autostato relativo ai coefficienti. Detto in maniera più semplice, i $ |C(x)|^2 $ rappresentano la probabilità di trovare $ \waf{} $ in posizione $ x $. Ma ciò che abbiamo appena detto coincide con la definizione di $ |\waf{}|^2 $. Questo significa quindi che:

\begin{equation}
\waf{} = C(x) = \braket{x}{\psi}
\end{equation}
Lo stesso discorso vale per il momento
\begin{equation}
\phi(p) = \braket{p}{\psi}
\end{equation}
Abbiamo scritto $ \phi(p) $ solo per motivi di consistenza con la notazione usata nei primi capitoli.

\subsection{Operatori di proiezione}

Torniamo un attimo al caso numerabile, come quello della particella nella scatola. Finora abbiamo scoperto che 
\begin{equation}
\ket{\psi} = \sum_n \ket{u_n}\braket{u_n}{\psi}
\end{equation}
Definiamo l'\emph{operatore di proiezione} $ P_n $ come quell'operatore per cui:
\begin{equation}
P_n = \ket{u_n}\bra{u_n}
\end{equation}
Le sue proprietà principali sono:
\begin{itemize}
\item 
Normalità: dati $ m,n $ con $ m \neq n$ allora
\begin{equation}
P_m P_n = \ket{u_m}\braket{u_m}{u_n}\bra{u_n} = 0
\end{equation}
\item 
Idempotenza:
\begin{equation}
P^2_n = P_n P_n = P_n \ket{u_n}\bra{u_n} = \ket{u_n}\underbrace{\braket{u_n}{u_n}}_{= 1}\ket{u_n} = \ket{u_n}\bra{u_n} = P_n
\end{equation}
\item
Completezza:
\begin{equation}
\sum_n P_n \; \ket{\psi} = \sum_n \ket{u_n}\braket{u_n}{\psi} = \mathbf{1}\ket{\psi}
\end{equation}
\begin{equation}
\sum_n P_n = \mathbf{1}
\end{equation}
\end{itemize}
Questi operatori vengono chiamati operatori di proiezione perchè proiettano uno stato $ \ket{\psi} $ nello stato n-esimo $ \ket{u_n} $ con una determinata probabilità data dal coefficiente $ \braket{u_n}{\psi} $. L'idempotenza ci dice che una volta proiettato in uno stato n, proiettarlo un'altra volta in tale stato non cambia nulla.

\subsection{Degenerazione e autofunzioni in comune}\label{sec:Degenerazioni e autofunzioni in comune}
Studiamo le condizione necessarie affinchè due operatori abbiano contemporaneamente gli stessi autostati.

Ipotizziamo che lo stato \(u_1(x)\) sià contemporaneamente autofunzione dell'operatore \(A\) e dell'operatore \(B\) con due diversi autovalori:
\begin{align}
A\,u_1(x) &= a\,u_1(x) \\\nonumber
B\,u_1(x) &= b\,u_1(x)
\end{align}
Se applico i due operatori in cascata noto che:
\begin{align}
ABu_1(x) = A(bu_1(x)) = b(Au_1(x)) = abu_1(x) \nonumber \\
BAu_1(x) = B(au_1(x)) = a(Bu_1(x)) = abu_1(x) \nonumber \\
\rightarrow \quad (AB-BA)u_1(x) = 0
\end{align}
Se un intero set completo di autofunzioni è contemporaneamente autostato dei due operatori allora possiamo esprimere più chiaramente la relazione precedente come:
\begin{align}
\sum_{n} C_n (AB-BA)u_n(x) &= (AB-BA)\sum C_n u_n(x) \nonumber \\
\nonumber &= (AB-BA) \,\psi(x) = 0 
\end{align}
\begin{equation}
[A,B]=0
\end{equation}
Quindi se due operatori convidono un set completo di autofunzioni allora essi \emph{commutano}.

Supponiamo ora che i due operatori commutino e cerchiamo di capire se vale l'inverso della conclusione precedente. Consideriamo \(u_1(x)\) autofunzione di \(A\).
Se commutano allora vale che:
\begin{equation}
AB u_1(x) = BA u_1(x) = B au_1(x) = aB u_1(x)
\end{equation}
Quindi \(Bu_1(x)\) è autofunzione di \(A\) con autovalore $a$. Ora abbiamo due casi: la dimensione dell'autospazio associato ad $a$ ha dimensione 1 o maggiore di 1.

Se c'è ad $a$ è associato un unico autostato allora necessariamente:
\begin{equation}
Bu_1(x) = b u_1(x)
\end{equation}
Lo stato $Bu_1(x)$ sarà proporzionale all'autofunzione di $A$: quindi $u_1(x)$ è autofunzione anche di $B$ con autovalore $b$.

Questo è il caso della commutazione tra Hamiltoniana e operatore parità nel caso della particella nella scatola infinita. 

Quando un operatore ha stati \emph{degeneri}, cioè autostati diversi ma associati allo stesso autovalore $a$, la situazione si complica.
\begin{align}
A u_1(x) &= a u_1(x) \\ \nonumber
A u_2(x) &= a u_2(x) 
\end{align}
Come prima possiamo sempre concludere che $Bu_1(x)$ e $B u_2(x)$ in generale appartengono all'austospazio relativo ad $a$ infatti vale ancora:
\begin{align}
 AB u_1(x) = BA u_1(x) &= B au_1(x) = aB u_1(x) \nonumber \\
 AB u_2(x) = BA u_2(x) &= B au_2(x) = aB u_2(x)
\end{align}
Però l'autospazio ora ha dimensione 2, quindi in generale  $Bu_1(x)$ e $B u_2(x)$ saranno combinazioni lineari di $u_1(x)$ e $u_2(x)$.
\begin{align}
B u_1(x) = b_{11} u_1(x) + b_{12}u_2(x) \nonumber \\
B u_2(x) = b_{21} u_1(x) + b_{22}u_2(x)
\end{align}
Diagonalizziamo ora questa relazione e troviamo due autostati $v_1(x)$ e $v_2(x)$ combinazione lineare di $u_1(x)$ e $u_2(x)$ tali che:
\begin{align}
Bv_1(x) = b_1 v_1(x) \\ \nonumber
Bv_2(x) = b_2 v_2(x)
\end{align}
Inotre $v_1(x)$ e $v_2(x)$ sono ancora autostati di $A$ con autovalore $a$ poichè sono combinazione lineare di autostati appartenti allo stesso autospazio.

Abbiamo quindi trovato due autofunzioni $v_1(x)$ e $v_2(x)$ in comune per i due operatori che commutano. Entrambe le autofunzioni hanno autovalore $a$ per l'operatore $A$ e hanno due diversi autovalori per l'operatore $B$: abbiamo risolto la degenerazione distinguendo i due autovalori.

In generale potrebbe accadere di avere tre autofunzioni degeneri per $A$ e che l'operatore $B$ abbia anch'esso degenerazione. In tal caso è necessario un terzo operatore $C$ che commuti con $A$ e $B$ che risolva la degenerazione.

Questo processo è del tutto generale. L'insieme degli operatori \emph{mutually commuting} è chiamato \emph{insieme completo di osservabili commutanti}.
L'insieme degli autovalori di questi operatori è la quantità maggiore di informazione che possiamo avere del sistema \emph{in contemporanea} e con precisione arbitraria. 

Infatti appena due operatori non commutano, sappiamo dal principio di indeterminazione di Heisenberg in forma generale (vedi eq. \vref{eq:heisenberg_generale}) che le due osservabili non possono essere misurate contemporaneamente con precisione arbitraria: la misura di una influenza l'altra. Questo non accade quando i due operatori commutano: in tal caso possono essere misurati indipendentemente con la precisione desiderata.



\end{document}