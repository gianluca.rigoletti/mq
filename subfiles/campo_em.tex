\documentclass[../mq.tex]{subfiles}
\begin{document}
\newpage
\section{Particelle In Campo Elettromagnetico}

\subsection{Introduzione}
Il proceso alla base della meccanica quantistica è la \emph{prima quantizzazione}, che consiste in:
\begin{enumerate}
\item descrivere le particelle come funzoni d'onda
\item descrivere i campi classicamente, non quantizzati,  quindi considerarli come oggetti fissati esterni al sistema. Sono quindi delle funzioni dello spaziotempo, non sono operatori.
\end{enumerate}
Si ricorda che 
\begin{align}
\vec {E} &= \vec {E}\left(\vec {x},t\right) \\
\vec {B} &= \vec {B}\left(\vec {x},t\right) 
\end{align}

\subsection{Meccanica Classica}
Lo scopo di questa sezione è studiare come i campi intervengno nell'Hamiltoniana del sistema.

\subsubsection{Unità di misura}
In base al sistea di misura le equazioni appaiono con delle costanti diverse.
In particolare l'espressione della forza di Lorenz nel Sistema Internazionale assume la forma
\begin{equation}
m \ddva{x} = q\vec{E} + q \dva{x} \times \vec {B}
\end{equation}
mentre nel sistema C.G.S.
\begin{equation}
m\ddva{x} = q \vec {E} + \frac{q}{c}\dva{x} \times \vec {B}
\end{equation}

\subsubsection{Fisica è determinata dai campi, meccanica classica}
La traiettoria $\left(\vec{x} , \dva{x}\right)$ può dipendere solo dai campi. Tuttavia è comodo studiare i sistemi per mezzo della loro Hamiltoniana, dentro cui compaiono i potenziali, non i campi. Riscrivo quindi i campi in termini dei potenziali
\begin{align}
\vec{E} &= - \grad{U} - \pd{t}{\vec{A}} \\
\vec{B} &= \curl{A}
\end{align}
con 
\begin{align}
U &= U\left(\vec {x},t\right) \\
\vec {A} &= \vec {A}\left(\vec {x},t\right)
\end{align}
Si osserva che $U$ e $\vec{A}$ non sono univocamente determinati: posso compiere una trasformazione e ottenere gli stessi campi. 
\begin{defin}[Trasformazione di gauge]
La trasformazione che operata sui potenziali lascia invariati i campi prende il nome di \emph{trasformazione di gauge}, e ha la forma 
\begin{align}
U' &= U - \pd {t}{\Lambda} \\
\vec{A'} &= \vec{A} + \grad{\Lambda} 
\end{align}
con 
\begin{equation}
\Lambda=\Lambda\left(\vec{x},t\right)
\end{equation}
\end{defin}
Il fatto che i potenziali non siano univocamente determinati non mi preoccupa, dal momento che i potenziali non sono misurabili. Le traiettorie $\left(\vec{x} , \dva{x}\right)$ dipendono dai campi, non dai potenziali, quindi non dal gauge. Segue che la scelta del gauge è arbitraria e non influenza la fisica.

\subsubsection{Hamiltoniana del campo elettromagnetico}
\begin{sis}
Viene considerato il seguente sistema classico:
\begin{itemize}
\item particella carica $q$ e massa $m$
\item campo elettromagnetico esterno alla particella
\end{itemize}
\end{sis}
Dal corso di meccanica analitica ri ottiene la forma della Lagrangiana elettromagnetica, da cui, applicando la trasformata di Legendre, si ottiene la Hamiltoniana del campo elettromagnetico.
\begin{equation}\label{ham-elettromag}
H = H_{em} = \frac{\left(\vec{p} - q\vec{A}\right)^2}{2m} - qU = \underbrace{\frac{\left(p_j - qA_j\right) \left(p_j - qA_j\right)}{2m}}_\text{Notazione di  Einstein} - qU
\end{equation}

\begin{defin}[Accoppiamento minimale]
Si osserva che il potenziale vettore interviene nell'Hamiltoniana modificando il termine cinetico, ovvero
\begin{equation}
\vec{p} \rightarrow \vec{p}-q\vec{A}
\end{equation}
Questo fatto prende il nome di \emph{accoppiamento minimale}.
\end{defin}

\subsubsection{Hamiltoniana restituisce l'equazione di Lorentz}
Controllo che l'Hamiltoniana elettromagnetica definita in \vref{ham-elettromag} restituisca l'equazione di Lorentz, che è l'equazione di Newton associata al campo elettromagnetico.
Si comincia scrivendo le equazioni di Hamilton
\begin{align}
\dot{x}_i &= \pd {p_i}{H} = \frac{p_i - qA_i}{m} \label{velocità}\\
\dot{p}_i &= - \pd {x_i}{H} = - \underbrace{\frac{1}{m}\left(p_j-qA_j\right)}_{\dot{x}_j}\left(-q\pd {x_i}{A_j}\right) -q\pd {x_i}{U}
\end{align}
ricordando che
\begin{align}
A_j &= A_j \left(\vec{x},t\right) \\
U &= U \left(\vec{x},t\right) \\
\pd {x_i}{p_j} &= 0 \quad \forall i,j \in \N
\end{align}
Segue che 
\begin{align}
m\ddot{x}_i &= \dot{p}_i-q\dot{A}_i = \dot{p}_i-q \cd{t}{A_i} \\
 &= q\dot{x}_j\pd {x_i}{A_j} - q\pd {x_i}{U} - q \left(\pd {x_j}{A_i}	\underbrace{\pd {t}{x_j}}_{\dot{x}_j} + \pd {t}{A_i}\right) \\
 &= q\dot{x}_j\underbrace{\left(\pd {x_i}{A_j} - \pd {x_j}{A_i}\right)}_{\epsilon_{ijk}B_k} - q\underbrace{\left(\pd {x_i}{U} + \pd {t}{A_i}\right)}_{-E_i} \\
 &= q\dot{x}_j\epsilon_{ijk}B_k + qE_i = qE_i + q\left(\dva{x}\times\vec{B}\right)_i
\end{align}

Osservando \vref{velocità}, si nota infine che  
\begin{equation}
p_i = m\dot{x}_i + qA_i
\end{equation}
ovvero che il momento non ha solamente il termine di momento cinetico, ma anche un termine dipendente dal potenziale vettore.
Quindi le traiettorie nello spazio delle fasi $\left(\vec{x},\vec{p}\right)$ dipendono dalla scelta del gauge, a causa dell'accoppiamento minimale. Fortunatamente $\vec{p}$, che è il momento coniugato a $\vec{x}$ ed è un oggetto matematico, non fisico, non è misurabile! Si misura invece $\dva{x}$, e  $\left(\vec{x} , \dva{x}\right)$ dipende dai campi, quindi non dalla scelta del gauge.

\subsection{Meccanica Quantistica}
\subsubsection{Equazione di Schr\"odinger}
Si scrive l'equazione di Schr\"odinger con l'Hamiltoniana del campo elettroomagnetico:
\begin{equation}
i\hbar \dot{\ket{\psi}} = \hat{H}_{em}\ket{\psi}
\end{equation}
Si osservi che nell'equazione di Schr\"odinger l'Hamiltoniana compare come operatore. Bisogna quindi fare attenzione a trasformare l'Hamiltoniana classica nell'operatore dela meccanica quantisitca. In virtù della prima quantizzazione le osservabili che descrivono la  particella diventano operatori, mentre i potenziali mantengono la forma funzionale classica. Nel caso in cui un campo sia funzione di una variabile che è stata promossa a operatore, allora il campo diventa dipendente dall'operatore, ricordandosi che questo non significa quantizzare il campo, rimanendo consistenti con la prima quantizzazione.

Ad esempio, se classicamente si ha $\vec{A}\left(\vec{x},t\right)$ e $U\left(\vec{x},t\right)$, allora quanstisticamente diventa $\vec{A}\left(\opvec{x},t\right)$ e $U\left(\opvec{x},t\right)$. Talvolta si può anche trovare la notazione $\opvec{A}\left(t\right) = \vec{A}\left(\vec{x},t\right)$ e $\hat{U}\left(t\right) = U\left(\opvec{x},t\right)$.

Quindi l'Hamiltoniana risulta essere
\begin{equation}
\hat{H}_{em} = \hat{H}_{em}\left(\opvec{x},\opvec{p},\vec{A}\left(\opvec{x},t\right),U\left(\opvec{x},t\right)\right)
\end{equation}
con 
\begin{align}
\vec{x} &\rightarrow \opvec{x} \quad \text{ovvero} \quad 
\begin{pmatrix} x\\ y\\ z \end{pmatrix} \rightarrow \begin{pmatrix} \op{x}\\ \op {y}\\ \op {z} \end{pmatrix} \\
\vec{p} &\rightarrow \opvec{p} = -i\hbar \grad{} \quad \text{ovvero} \quad 
\begin{pmatrix} p_x\\ p_y\\ p_z \end{pmatrix} \rightarrow  \begin{pmatrix} -i\hbar\frac{d}{dx}\\ -i\hbar\frac{d}{dy}\\ -i\hbar\frac{d}{dz} \end{pmatrix}
\end{align}
Anche nel caso della meccanica quantistica si ha l'accoppiamento minimale:
\begin{equation}
\left(\opvec{p}\right)^2 \rightarrow \left(\opvec{p}-q\vec{A}\left(\opvec{x},t\right)\right)^2
\quad \text{ovvero} \quad 
\left(-i\hbar\grad{}\right)^2 \rightarrow \left(-i\hbar\grad{}-q\vec{A}\left(\vec{x},t\right)\right)^2
\end{equation}
Da cui segue l'equazione di Schr\"odinger
\begin{equation}
i\hbar\dot{\ket{\psi}} = \left(\frac{\left(\opvec{p}-q\vec{A}\left(\opvec{x},t\right)\right)^2}{2m}+qU\left(\opvec{x},t\right)\right)\ket{\psi}
\end{equation}
che può anche essere riscritta esplicitando gli operatori e la funzione d'onda
\begin{equation}
i\hbar\pd {t}{\psi\left(\vec{x},t\right)} = \frac{1}{2m} \left(-i\hbar\grad{}-q\vec{A}\left(\vec{x},t\right)\right)^2\psi\left(\vec{x},t\right) + qU\left(\vec{x},t\right)\psi\left(\vec{x},t\right)
\end{equation}

\subsubsection{Fisica è determinata dai campi, meccanica quantistica}
Noto che anche in meccanica quantistica i potenziali entrano nell'equazione di Schr\"odinger. È lecito quindi chiedersi se anche in meccanica quantistica la fisica è indipendente dal gauge. 

Considero la trasformazione di gauge
\begin{align}
U' &= U - \pd {t}{\Lambda} \\
\vec{A} &= \vec{A} +\grad{\Lambda}
\end{align}
con
\begin{align}
\psi &= \psi\left(\vec {x},t\right) \\
\psi' &= \psi'\left(\vec {x},t\right)
\end{align}
e
\begin{align}
\hat{U}\left(t\right) &= U\left(\opvec {x},t\right) \\
\opvec{A}\left(t\right) &= \vec {A}\left(\opvec {x},t\right) \\
\hat{U'}\left(t\right) &= U'\left(\opvec {x},t\right) \\
\opvec {A'}\left(t\right) &= \vec {A'}\left(\opvec {x},t\right) \\
\hat{\Lambda}\left(t\right) &= \Lambda\left(\opvec {x},t\right) 
\end{align}

Richiedo che l'equazione di Schr\"odinger trasformata sia invariante di gauge, quindi:
\begin{equation}
i\hbar\pd {t}{\psi'} = \frac{1}{2m} \left(-i\hbar\grad{}-q\vec{A'}\right)^2\psi' + qU'\psi'
\end{equation}
da cui ottiene
\begin{equation}
\psi'=e^{\frac{i}{\hbar}q\Lambda}\psi
\end{equation}
Ovvero: la soluzione dell'equazione di  Schr\"odinger trasformata è univocamente determinata a partire dall'equazione di  Schr\"odinger di partenza, mediante la funzione che determina il gauge. In particolare le soluzioni differiscono per una fase.

\begin{teo}[Condizione per $\ket{\psi}$ tale che l'equazione di Schr\"odinger sia invariante di gauge]
Data la trasformazione di gauge
\begin{align}
U' &= U - \pd {t}{\Lambda} \\
\vec{A} &= \vec{A} +\grad{\Lambda}
\end{align}
e l'equazione di Schr\"odinger trasformata
\begin{equation}\label{eq-schro'}
i\hbar\pd {t}{\psi'} = \frac{1}{2m} \left(-i\hbar\grad{}-q\vec{A'}\right)^2\psi' + qU'\psi'
\end{equation}

Allora 
\begin{equation}
\psi'=e^{\frac{i}{\hbar}q\Lambda}\psi
\end{equation}
è tale che l'euqazione di Schr\"odinger originaria
\begin{equation}
i\hbar\pd {t}{\psi} = \frac{1}{2m} \left(-i\hbar\grad{}-q\vec{A}\right)^2\psi + qU\psi
\end{equation}
è ancora valida.
\end{teo}
\begin{proof}
Si comincia considerando il primo membro di \vref{eq-schro'}:
\begin{align}
i\hbar\pd {t}{\psi'} &= i\hbar\pd {t}{}\left(e^{\frac{i}{\hbar}q\Lambda}\psi\right) = i\hbar\left(e^{\frac{i}{\hbar}q\Lambda} \pd {t}{\psi} + \psi e^{\frac{i}{\hbar}q\Lambda} \frac{iq}{\hbar}\pd {t}{\Lambda}\right)\\
&= e^{\frac{i}{\hbar}q\Lambda} \left(i\hbar \pd {t}{\psi} - q\psi\pd {t}{\Lambda}\right) \label{primo-termine}
\end{align}
Si posegue considerando il secondo membro di \vref{eq-schro'}:
\begin{multline}\label{secondo-termine}
\frac{1}{2m}\left(\opvec{p}-q\vec{A'}\right)^2\psi'+qU'\psi' = \\ \frac{1}{2m} \left(\opvec{p}-q\vec{A'}\right)\underbrace{\left(\opvec{p}-q\vec{A'}\right)e^{\frac{i}{\hbar}q\Lambda}}_{\text {operatore che agisce su $\psi$}}\psi+q\underbrace{\left(U-\pd {t}{\Lambda}\right)e^{\frac{i}{\hbar}q\Lambda}}_{\text {commutano, dipendono solo da $\opvec{x}$}}\psi
\end{multline}
Commuto $\left(\opvec{p}-q\vec{A'}\right)$ con $e^{\frac{i}{\hbar}q\Lambda}$:
\begin{align}
\left(\opvec{p}-q\vec{A'}\right)e^{\frac{i}{\hbar}q\Lambda} &= \left(\opvec{p}-q\vec{A}-q\grad{\Lambda}\right)e^{\frac{i}{\hbar}q\Lambda} \\
&= \opvec{p}e^{\frac{i}{\hbar}q\Lambda} - q\vec{A}e^{\frac{i}{\hbar}q\Lambda} -q\grad{\Lambda}e^{\frac{i}{\hbar}q\Lambda} \label{contiargh}
\end{align}
Guardo l'ultima espressione termine a termine:
\begin{align}
\opvec{p}e^{\frac{i}{\hbar}q\Lambda} &= e^{\frac{i}{\hbar}q\Lambda}\opvec{p} + \comm{\opvec{p}}{e^{\frac{i}{\hbar}q\Lambda}}  \\
&= e^{\frac{i}{\hbar}q\Lambda}\opvec{p} - i\hbar\frac{iq}{\hbar}e^{\frac{i}{\hbar}q\Lambda}\grad{\Lambda} \\
&= e^{\frac{i}{\hbar}q\Lambda} \left(\opvec{p}+q\grad{\Lambda}\right)
\end{align}
si è usata la proprietà del commutatore di $\opvec{p}$: $\comm{\opvec{p}}{f(\opvec{x})} = -i\hbar\grad{f(\vec{x})}$. 

Negli altri due termini cambio l'ordine dei prodotti. Posso farlo perchè sono operatori dipendenti solo da $\opvec{x}$:
\begin{align}
q\vec{A}e^{\frac{i}{\hbar}q\Lambda} &= e^{\frac{i}{\hbar}q\Lambda}q\vec{A} \\
q\grad{\Lambda}e^{\frac{i}{\hbar}q\Lambda} &= e^{\frac{i}{\hbar}q\Lambda}q\grad{\Lambda}
\end{align}
Sostituisco i tre termini in \vref{contiargh}:
\begin{align}
\left(\opvec{p}-q\vec{A'}\right)e^{\frac{i}{\hbar}q\Lambda} &= e^{\frac{i}{\hbar}q\Lambda} \left(\opvec{p}+q\grad{\Lambda}\right) -e^{\frac{i}{\hbar}q\Lambda}q\vec{A} -e^{\frac{i}{\hbar}q\Lambda}q\grad{\Lambda} \\
&= e^{\frac{i}{\hbar}q\Lambda} \left(\opvec{p}-q\vec{A}\right) \label{yuppie}
\end{align}
Utilizzo due volte \vref{yuppie} in \vref{secondo-termine}: 
\begin{multline} 
\frac{1}{2m}\left(\opvec{p}-q\vec{A'}\right)^2\psi'+qU'\psi' = \\ \frac{1}{2m} e^{\frac{i}{\hbar}q\Lambda}  \left(\opvec{p}-q\vec{A}\right) \left(\opvec{p}-q\vec{A}\right) \psi + qe^{\frac{i}{\hbar}q\Lambda}\left(U-\pd {t}{\Lambda}\right)\psi
\end{multline}
Eguaglio i due termini della \vref{eq-schro'}, ovvero l'ultima equazione viene posta uguale a \vref{primo-termine}:
\begin{multline}
e^{\frac{i}{\hbar}q\Lambda} \left(i\hbar \pd {t}{\psi} - q\psi\pd {t}{\Lambda}\right) = \\ \frac{1}{2m} e^{\frac{i}{\hbar}q\Lambda}  \left(\opvec{p}-q\vec{A}\right) \left(\opvec{p}-q\vec{A}\right) \psi + qe^{\frac{i}{\hbar}q\Lambda}\left(U-\pd {t}{\Lambda}\right)\psi
\end{multline}
Si semplifica $e^{\frac{i}{\hbar}q\Lambda}$ e si cancellano i termini $q\psi\pd {t}{\Lambda}$ e si ottiene finalmente
\begin{equation}
i\hbar\pd {t}{\psi} = \frac{1}{2m} \left(\opvec{p}-q\vec{A}\right)^2\psi + qU\psi
\end{equation}
\end{proof}
In conclusione: l'equazione di Schr\"odinger è invariante per trasformazione di gauge! La soluzione $\psi'$ è diversa da $\psi$, ma differiscono solo per una fase. 

Si ricorda che in meccanica quantistica la fisica è al livello delle probabilità, non delle funzioni d'onda, quindi, dal momento che 
\begin{equation}
p\left(\vec{x},t\right) = \abs{\psi\left(\vec{x},t\right)}^2 = \abs{\psi'\left(\vec{x},t\right)}^2
\end{equation}
si conclude che la fisica non dipende dal gauge nemmeno in meccanica quantistica.

\subsubsection{Simmetrie}
Posso ripetere lo stesso procedimento, ma con un punto di vista diverso.
In particolare, mi focalizzo sulla simmetria di gauge, che può essere vista come la base per l'elettromagnetismo e la conservazione della carica.
\begin{defin}[Simmetria di fase globale]
Tutte le osservabili non vengono modificate se si effettua una modifica globaledellaf ase della funzione d'onda, che ha la forma 
\begin{equation}
\psi \rightarrow e^{i\lambda}\psi
\end{equation}
\end{defin}
\begin{defin}[Simmtria di fase locale]
È possibile cambiare la fase della funzione d'onda in ogni punto dello spaziotempo e la fisica rimane invariata. questa simmetria ha la forma 
\begin{equation}
\psi \rightarrow e^{i\lambda\left(\vec{x},t\right)}\psi
\end{equation}
ed è una simmetria più generale della simmetria di fase globale.
\end{defin}

\begin{oss}
$\abs{\psi}^2$ rimane invariato sotto trasformazione di fase locale. Voglio che anche l'equazione di Schr\"odinger lo sia.
\end{oss}

\begin{teo}
Imposta la seguente trasformazione, che racchiude la simmetria di fase locale sulla funzione d'onda e una trasformazione abbastanza generale sui potenziali ,
\begin{align}
\psi\left(\vec{x},t\right) &\rightarrow e^{i\lambda\left(\vec{x},t\right)} \psi\left(\vec{x},t\right) \ \text{ovvero} \ \ket{\psi} \rightarrow e^{i\lambda\left(\opvec{x},t\right)}\ket{\psi} \\
U\left(\opvec{x},t\right) &\rightarrow U\left(\opvec{x},t\right) + {\Delta U} \left(\opvec{x},t\right) \\
\vec{A}\left(\opvec{x},t\right) &\rightarrow \vec{A}\left(\opvec{x},t\right) + \vec{\Delta A}\left(\opvec{x},t\right)
\end{align}
le condizioni da imporre sull'Hamiltoniana 
\begin{equation}
\hat{H}\left(\opvec{x},\opvec{p},\vec{A}\left(\opvec{x},t\right),U\left(\opvec{x},t\right)\right) = \frac{\left(\opvec{p}-q\vec{A}\left(\opvec{x},t\right)\right)}{2m} + qU\left(\opvec{x},t\right)
\end{equation}
tali per cui l'equazione di Schr\"odinger trasformata sia equivalente all'equazione di Schr\"odinger originaria, ovvero tali che
\begin{equation}
i\hbar\dot{\ket{\psi'}}=\hat{H'}\ket{\psi'} \iff i\hbar\dot{\ket{\psi}}=\hat{H}\ket{\psi} 
\end{equation}
con 
\begin{equation}
\hat{H'} = \hat{H}\left(\opvec{x},\opvec{p},\vec{A'}\left(\opvec{x},t\right),U'\left(\opvec{x},t\right)\right) \text { et } \ket{\psi'} = e^{i\lambda\left(\opvec{x},t\right)}\ket{\psi}
\end{equation}
sono
\begin{equation} 
\begin{cases}
\vec{\Delta A} &= \frac{\hbar}{q}\grad{\lambda} \\
\Delta U &= - \frac{\hbar}{q} \pd {t}{\lambda}
\end{cases}
\end{equation}
\begin{equation} 
\begin{cases}
\vec{A'}  &= \vec{A} + \grad{\Lambda}  \\
U'  &= U  - \pd {t}{\Lambda} 
\end{cases}
\end{equation}
con
\begin{align}
\psi &= \psi\left(\vec {x},t\right) \\
\psi' &= \psi'\left(\vec {x},t\right)
\end{align}
e
\begin{align}
\hat{U}\left(t\right) &= U\left(\opvec {x},t\right) \\
\opvec{A}\left(t\right) &= \vec {A}\left(\opvec {x},t\right) \\
\hat{U'}\left(t\right) &= U'\left(\opvec {x},t\right) \\
\opvec {A'}\left(t\right) &= \vec {A'}\left(\opvec {x},t\right) \\
\hat{\Lambda}\left(t\right) &= \Lambda\left(\opvec {x},t\right) 
\end{align}
e
\begin{equation}
\Lambda = \frac{\hbar}{q} \lambda 
\end{equation}
\end{teo}
\begin{proof}
Si considera l'equazione di Schr\"odinger trasformata
\begin{equation}
i\hbar\dot{\ket{\psi'}}=\hat{H'}\ket{\psi'}
\end{equation}
Questa dimostrazione viene effettuata usando le espressioni esplicite degli operatori, quindi tralascio tutti i cappucci e metto tutte le derivate necessarie. Riscrivo quindi l'equazone di Scr\"odinger come
\begin{equation}
i\hbar\pd {t}{\psi'\left(\vec {x},t\right)} = \frac{1}{2m} \left(-i\hbar\grad{}-q\vec{A'}\left(\vec {x},t\right)\right)^2\psi'\left(\vec {x},t\right) + qU'\left(\vec {x},t\right)\psi'\left(\vec {x},t\right)
\end{equation}
quindi, sostituendo le trasformazioni
\begin{multline}\label{eq-schro'1}
i\hbar\pd {t}{}\left(e^{i\lambda\left(\vec{x},t\right)} \psi\left(\vec{x},t\right)\right) = \\ 
\frac{1}{2m} \left(-i\hbar\grad{}-q \vec{A}\left(\vec{x},t\right) - q\vec{\Delta A}\left(\vec{x},t\right) \right)^2 e^{i\lambda\left(\vec{x},t\right)} \psi\left(\vec{x},t\right) + \\ q\left(U\left(\vec{x},t\right) + {\Delta U} \left(\vec{x},t\right)\right) e^{i\lambda\left(\vec{x},t\right)} \psi\left(\vec{x},t\right)
\end{multline}
Il primo termine diventa
\begin{equation}
i\hbar\left(\psi e^{i\lambda}i\pd{t}{\lambda} + e^{i\lambda}\pd {t}{\psi}\right)
= e^{i\lambda} \left(i\hbar\pd {t}{\psi} - \hbar\psi\pd {t}{\lambda}\right)
\end{equation}
considero l'operatore
\begin{align}
\left(-i\hbar\grad{}-q \vec{A} - q\vec{\Delta A} \right) e^{i\lambda} &=  -i\hbar \grad{e^{i\lambda}} - q\vec{A}e^{i\lambda}-q\vec{\Delta A}e^{i\lambda} \label{devo-commutare}\\ 
&= -i\hbar e^{i\lambda}\grad{} + \hbar e^{i\lambda}\grad{\lambda} - q\vec{A}e^{i\lambda}-q\vec{\Delta A}e^{i\lambda} \\
&= e^{i\lambda}\left(-i\hbar\grad{}+\hbar\grad{\lambda}-q\vec{A}-q\vec{\Delta A}\right)
\end{align}
avendo commutato i termini del primo addendo di \vref{devo-commutare} come segue
\begin{equation}
-i\hbar \grad{e^{i\lambda}} = e^{i\lambda}\left(-i\hbar \grad{}\right)  -i\hbar e^{i\lambda}i\grad{\lambda} = -i\hbar e^{i\lambda}\grad{} + \hbar e^{i\lambda}\grad{\lambda}
\end{equation}
Sostituisco tutto in \vref{eq-schro'1},che diventa, semplificando $e^{i\lambda}$:
\begin{equation}
i\hbar\pd {t}{\psi} - \hbar\psi\pd {t}{\lambda} = \frac{1}{2m}\left(-i\hbar\grad{}+\hbar\grad{\lambda}-q\vec{A}-q\vec{\Delta A}\right)^2\psi + qU\psi + q\Delta U \psi
\end{equation}
Appare chiaro che per avere invariaza dell'equazione di Schr\"odinger, e ottenere quindi
\begin{equation}
i\hbar\pd {t}{\psi} = \frac{1}{2m}\left(-i\hbar\grad{}-q\vec{A}\right)^2\psi + qU\psi
\end{equation}
è necessario che
\begin{align}
q\vec{\Delta A} &= \hbar \grad{\lambda}\\
q\Delta U &= -\hbar\pd {t}{\lambda}
\end{align}
quindi, riscalando $\lambda$ con $\Lambda=\frac{\hbar}{q}\lambda$,
\begin{align}
\vec{A'} &= \vec{A} + \grad{\Lambda}\\
U' &= U-\pd {t}{\Lambda}
\end{align}
Si conclude quindi la dimostrazione.
\end{proof}

Si osserva che questa scelta di $\vec{\Delta A}$ e $\Delta U$, che lascia l'equazione di Schr\"odinger invariata, è tale che $\exists \vec{E},\vec{B}$ tali che
\begin{align}
\vec{E} &= -\grad{U} - \pd {t}{\vec{A}} \\
\vec{B} &= \curl{A}
\end{align}
Pertanto data l'Hamiltoniana e la simmetria di gauge, i potenziali $\vec{A}$ e $U$ devono necessariametne descrivere i campi $\vec{E}$ e $\vec{B}$.
Si può quindi asserire che l'elettromagnetismo è necessario per avere simmetria di fase locale per particelle cariche. Con altre parole, l'invarianza locale di gauge richiede l'esistenza del campo elettromagnetico, quindi l'elettromagnetismo è una \emph{teoria di gauge}, cioè una teria definita da un'invarianza di gauge.

Si ricorda che anche l'interazione debole e l'interazione forte sono teorie di gauge.

\paragraph{Riepilogo}
Partendo da
\begin{itemize}
\item Equazione di Schr\"odinger, con Hamiltoniana elettromagnetica
\begin{equation}
i\hbar\dot{\ket{\psi}} = H_{em}\ket{\psi}
\end{equation}
\item Simmetria di fase locale 
\begin{equation}
\ket{\psi'}=e^{i\lambda\left(\opvec{x},t\right)}\ket{\psi}
\end{equation}
\item Ipotizzo una struttura abbastanza generica di trasformazioni per i potenziali
\begin{align}
\vec{A}' &= \vec{A} + \vec{\Delta A} \\
U' &= U + \Delta U
\end{align}
\end{itemize}

si ottiene 
\begin{align}
\vec{A}' &=  \vec{A} + \grad{\Lambda} \\
U' &= U - \pd {t}{\Lambda}
\end{align}
ovvero il campo elettromagnetico
\begin{align}
\vec{E} &= -\grad{U} - \pd {t}{\vec{A}} \\
\vec{B} &= \curl{A}
\end{align}

\subsubsection{Invarianza di gauge dei valori medi di alcuni operatori}
Scopo di questa sezione è guardare quali operatori hanno valore medio invariante di gauge.
\begin{teo}
Si consideri una trasformazione di gauge come sopra. Si ha che, 
\begin{equation}
\ev{\opvec{x}'} = \avg{\psi'}{\opvec{x}'}= \ev{\opvec{x}}
\end{equation}
con $\ket{\psi'} = e^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}\ket{\psi}$
\end{teo}
\begin{proof}
\begin{align}
\ev{\opvec{x}'} &= \avg{\psi'}{\opvec{x}'} = \avg{\psi}{e^{-\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}xe^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}} \\
&= \avg{\psi}{\opvec{x}} = \ev{\opvec{x}}
\end{align}
ricordando che $\hat{x}'=\hat{x}$ e che $e^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}$ commuta con $\hat{x}$, dal momento che $\Lambda$ è funzione della sola posizione, e non anche del momento.
\end{proof}

\begin{defin}(Operatore velocità)
Ricordando che in meccaanica classica vale, come visto in \vref{velocità}:
\begin{equation}
\dva{x} = \frac{\vec{p}-q\vec{A}\left(\vec{x},t\right)}{m}
\end{equation}
definisco l'\emph{operatore velocità} come
\begin{equation}
\opvec{v} = \frac{\opvec{p}-q\vec{A}\left(\opvec{x},t\right)}{m}
\end{equation}
\end{defin}

\begin{teo}
Si consideri una trasformazione di gauge come sopra. Si ha che, 
\begin{equation}
\ev{\opvec{v}'} = \avg{\psi'}{\opvec{v}'}= \ev{\opvec{v}}
\end{equation}
con $\ket{\psi'} = e^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}\ket{\psi}$
\end{teo}
\begin{proof}
\begin{align}
\ev{\opvec{v}'} &= \avg{\psi'}{\opvec{v}'} = \avg{\psi'}{\frac{\opvec{p}-q\vec{A}'\left(\opvec{x},t\right)}{m}} \\
 &= \avg{\psi}{e^{-\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)} \frac{\opvec{p}-q\vec{A}'\left(\opvec{x},t\right)}{m} e^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}} \\
 &= \avg{\psi}{e^{-\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}e^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)} \frac{\opvec{p}-q\vec{A}\left(\opvec{x},t\right)}{m}} \\
 &= \avg{\psi}{\frac{\opvec{p}-q\vec{A}\left(\opvec{x},t\right)}{m}} = \avg{\psi}{\opvec{v}} = \ev{\opvec{v}}
\end{align}
Si osservi che è stato usata \vref{yuppie} per commutare gli operatori.
\end{proof}

\begin{teo}
Si consideri una trasformazione di gauge come sopra. Si ha che, 
\begin{equation}
\ev{\opvec{p'}} = \ev{\opvec{p}} + \ev{q\grad{\Lambda\left(\opvec{x},t\right)}}
\end{equation}
con $\ket{\psi'} = e^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}\ket{\psi}$
\end{teo}
\begin{proof}
\begin{align}
\ev{\opvec{p'}} &= \avg{\psi}{e^{-\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)} \ \opvec{p} \  e^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}} \\
&= \avg{\psi}{e^{-\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)} \left(  e^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}\opvec{p} +\comm{\opvec{p}}{e^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}} \right)}\\
=& \avg{\psi}{\opvec{p}} + \avg{\psi}{e^{-\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)} \underbrace{\left(-i\hbar\right) \frac{iq}{\hbar}}_{q}e^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}\grad{\Lambda} } \\
&= \ev{\opvec{p}} + \ev{q\grad{\Lambda}}
\end{align}
ricordando che $\opvec{p'}=\opvec{p}$ e che $\grad{\Lambda}$ e $e^{\frac{i}{\hbar}q\Lambda\left(\opvec{x},t\right)}$ commutano.
\end{proof}

\paragraph{Riepilogo}
Si nota quindi che $\ev{\opvec{x}}$ e $\ev{\opvec{v}}$ sono invarianti per trasformazione di gauge, mentre $\ev{\opvec{p}}$ non lo è, analogamente al caso classico, in cui la fisica era rappresentata dal $\left(\vec{x}, \dva{x}\right)$ e non dalle traiettorie $ \left(\vec{x},\vec{p}\right)$ nello spazio delle fasi, poichè $p$ è una variabile matematica, non ha senso fisico, non può essere misurata.

\end{document}