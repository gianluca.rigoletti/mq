\documentclass[../mq.tex]{subfiles}
\begin{document}
\newpage

\section{Dualismo onda particella}
\subsection{Funzione d'onda e probabilità}
\paragraph{Fotoni}
Torniamo all'esperimento della doppia fenditura effettuato con un fascio luminoso, un fascio di fotoni. Come sappiamo dall'ottica sullo schermo al di là delle fenditura di crea una figura di interferenza. Questa può essere spiegata descrivendo la propagazione della luce con un campo elettrico oscillante $\vec{E}(x,t)$ che dovendo attraversare le due fenditure si scinde in due termini: $$ \vec{E}(x,t) = \vec{E_1}(x,t)+ \vec{E_2}(x,t)$$ Il campo elettrico risultante sullo schermo è dato dalla somma dei due campi, sfruttando la linearità delle equazioni di Maxwell e quindi il principio di sovrapposizione degli effetti. Ciò che misuriamo è l'\emph{intensità} della radiazione, data dal modulo quadro del campo elettrico totale:
$$ I= |\vec{E_1}+ \vec{E_2}|^2$$
Questa è l'interpretazione classica dell'interferenza.

Se ora consideriamo il fascio di luce come costituito da fotoni dobbiamo associare al concetto di intensità il concetto di probabilità.
Immaginiamo che il fascio di luce sia abbastanza debole in modo tale che passi attraverso le fenditure un solo fotone alla volta. Ogni fotone viene registrato sullo schermo. Dopo che un elevato numero di fotoni ha attraversato le fenditure osserviamo ancora una figura di interferenza. Questo significa che ogni \emph{singolo} fotone interagisce con l'ostacolo e che l'interferenza non è un fenomeno di gruppo. Quindi dobbiamo concludere che ogni fotone interferisce con se stesso e dobbiamo immaginare che il campo elettrico che descrive il fotone $\vec{e}(x,t)$ si divida in due termini al passaggio attraverso le fenditure. L'unica ipotesi che dobbiamo fare è che questo campo $\vec{e}(x,t)$ si comporti in modo lineare e che la somma di molti di questi campi rispetti le leggi di Maxwell per l'ottica. L'intensità risultante sullo schermo deve essere interpretata come la probabilità che il fotone raggiunga lo schermo in quel punto.
$$P(x,t) = | E_1(x,t)+ E_2(x,t)|^2$$

\paragraph{Elettroni}
Consideriamo ora un fascio di elettroni che raggiunge una doppia fenditura. L'elettrone è sempre stato descritto come una particella: percorre traiettorie governate dai campi elettromagnetici, ha massa e trasporta una determinata energia e momento. L'interazione con le fenditure crea però una figura di interferenza. Anche qui potremmo immagine di far passare un elettrone per volta, ma l'effetto complessivo sarebbe comunque una figura di interferenza: è una proprietà del singolo elettrone. Possiamo immaginare quindi di descrivere l'elettrone, come il fotone, con un campo $\vec{e}(x,t)$, più precisamente una \emph{funzione d'onda} $\psi(\vec{r},t)$. Poichè l'elettrone deve interferire con se stesso dobbiamo assumere che $\psi(\vec{r},t)$ obbedisca a un'equazione lineare. A questa funzione d'onda associamo quindi la probabilità che l'elettrone arrivi sullo schermo in $\vec{r}$.

Nel caso in cui una sola fenditura sia aperta la probabilità è data da $$P_1(x,t) = |\psi_1(x,t)|^2$$
Nel caso in cui entrambe siano aperte la probabilità è il modulo quadro della \emph{somma} delle due funzioni d'onda, in analogia con l'intensità dell'onda luminosa: $$ P(x,t)= |\psi_1(x,t) + \psi_2(x,t)|^2$$

\subsection{Pacchetto d'onda}\label{sec:pacchetto_d'onda}
Cerchiamo di dare una forma alla funzione d'onda di una particella. Partiamo dalla formula più generale: un onda armonica piana che si propaga sull'asse x con numero d'onda $k$ e pulsazione $w$.
\begin{equation}\label{eq:onda_piana}
\waf{k} = A e^{i (kx -wt)} + B e^{-i(kx-wt)}
\end{equation}
$k$ è il numero d'onda definito come $k = \frac{2\pi}{\lambda}$ e $\omega$ è la pulsazione: $w = 2\pi\nu$.\\
Evitiamo il secondo termine nell'eq. \vref{eq:onda_piana} perchè come vedremo porta a risultati che non hanno significato fisico (energia cinetica negativa).

Tra $\omega$ e $k$ sussiste una relazione $\omega(k)$ che viene chiamata \emph{relazione di dispersione} dell'onda e che è fondamentale per determinare a che velocità si sposta l'onda.

Un'onda piana però è solo un oggetto matematico ideale che non ha senso fisico: infatti è definita in tutto lo spazio e in ogni tempo, quindi non può modellizzare lo spostamento di una particella.
Sommiamo quindi diverse onde piane con diversi numeri d'onda per ottenere un \emph{pacchetto d'onda}, cioè una perturbazione non periodica che si sposta nello spazio e ha un larghezza finita.\\
Utilizziamo una funzione di modulazione $A(k)$ che descrive l'ampiezza di ogni contributo al pacchetto d'onda. Notiamo che le funzioni in gioco sono in generale complesse.

\begin{equation} \label{eq:pacc_onda}
\waf{} = \int_{-\infty}^{+\infty} dk A(k) e^{i(kx-wt)}
\end{equation}


Consideriamo una forma speciale della funzione di modulazione: una gaussiana.
\begin{equation}
A(k) = e^{-\alpha(k-k_0)^2 / 2}
\end{equation}
Questa funzione ha un massimo centrato in $k_0$ e decresce rapidamente. Il $\sigma$ della gaussiana è dato da $\sigma= 1 / \sqrt{\alpha}$, quindi possiamo prendere come \emph{larghezza} della modulazione $\Delta k = 2 / \sqrt{\alpha}$.

Studiamo il pacchetto d'onda nell'istante iniziale (eseguendo un integrale di Fourier Gaussiano):
\begin{equation}\label{eq:t0}
\psi(x,0) = \sqrt{\frac{2\pi}{\alpha}} e^{ik_0x} e^{-x^2/2\alpha}
\end{equation}
Possiamo notare due contributi in questa equazione. Il fattore $e^{-x^2/2\alpha}$ modula l'ampiezza del pacchetto e lo localizza nello spazio intorno a $x=0$ con una forma gaussiana. Il termine $e^{ik_0 x}$ è un fattore ``di onda piana'' con numero d'onda $k_0$ all'istante $t=0$. Vediamo quindi che il pacchetto d'onda non è più un'onda esistente in tutto lo spazio, ma ha forma gaussiana, e ha un numero d'onda corrispondente con il valore centrale della funzione di modulazione $A(k)$.

Tutto questo è coerente con l'interpretazione probabilistica di $\waf{}$.
Infatti se una particella fosse descritta da un'onda piana monocromatica si avrebbe la stessa probabilità di trovare la particella in tutto lo spazio: la particella sarebbe completamente \emph{delocalizzata} ma avrebbe un momento definito.

Invece il pacchetto d'onda localizza la particella nello spazio, seppur in un certo intervallo.
Osservando il pacchetto a $t=0$ in eq. \vref{eq:t0} vediamo che larghezza è data da $\Delta x = 2 \sqrt{\alpha}$.

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{./img/pacchetto_gaussiano}
\caption{Pacchetto d'onda gaussiano}
\label{fig:pacchetto_gaussiano}
\end{figure}


Notiamo quindi una proprietà generale delle trasformate di Fourier e in questo caso della relazione tra il dominio dello spazio e del numero d'onda (verrà approfondito in seguito).
$$\Delta x \Delta k = 4 = cost$$
Maggiore è la larghezza del pacchetto nel dominio dei numeri d'onda, minore è l'estensione del pacchetto nello spazio.

\subsection{Velocità di gruppo}
Abbiamo visto un pacchetto d'onda gaussiano al tempo $t=0$. Ma come si muove il pacchetto d'onda nel tempo? Non siamo interessati alla velocità delle singole componenti del pacchetto, ma alla velocità globale del pacchetto stesso. Se si ha un pacchetto con una modulazione in numero d'onda stretta intorno a $k_0$, allora il pacchetto si muove come un'onda piana ma modulata in ampiezza da una funzione di x. La velocità del pacchetto d'onda è data da:
\begin{equation}
v_g = \left( \pdv{k}{\omega(k)}{} \right)_{k=k_0}
\end{equation} 

\end{document}