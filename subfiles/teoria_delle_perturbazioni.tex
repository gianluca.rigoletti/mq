\documentclass[../mq.tex]{subfiles}
\begin{document}
\newpage
\section{Teoria delle perturbazioni}\label{sec:teoria_perturbazioni}

L'equazione di Schr\"odinger è risolvibile analiticamente soltanto in casi molto particolari 
(ad esempio l'oscillatore armonico, l'atomo di idrogeno, la particella nella buca di potenziale).
Molte volte, però, il potenziale $ V(r) $, non dipendente esplicitamente dal tempo, assume una forma
tale per cui non è possibile risolvere direttamente l'equazione di Schr\"odinger. Quello che trattiamo
in questa sezione è la \emph{teoria delle perturbazioni}: una tecnica che, conoscendo le
soluzioni analitiche di Hamiltoniana nota, permette di capire come viene modificato il sistema da
 una Hamiltoniana non nota (detta anche perturbazione).

In particolare, supponiamo di avere le equazioni agli autovalori:

\begin{equation} \label{eq:Hamiltoniana imperturbata}
H_0 \ket{\phi_n} = E^{(0)}_n \ket{\phi_n}
\end{equation}
Con $ \phi_n $ gli autostati dell'hamiltoniana, e l'apice 0 a indicare l'autovalore dell'energia relativo
all'Hamiltoniana $ H_0 $.
Supponiamo di avere ora un termine perturbativo. Di conseguenza l'equazione diventa:
\begin{equation} \label{eq:Hamiltoniana perturbata}
(H_0 + \lambda H_1) \ket{\psi_n} = E_n \ket{\psi_n}
\end{equation}
In questo caso non sono noti $ \psi_n $ e $ E_n $ ed è ciò che ci interessa analizzare.
Prima di tutto, facciamo una breve considerazione su $ \lambda $: è un parametro che definisce quanto
è grande il termine perturbativo. Nel nostro caso assumiamo di poter scrivere le quantità che ci 
interessano come serie di potenze di $ \lambda $. Non ci preoccupiamo della convergenza per ora (molte
volte accade che la serie non converga nemmeno, ma nella nostra trattazione lo farà sempre). Assumiamo
inoltre che quando la perturbazione tende ad un valore infinitesimo, gli autovalori dell'energia tornano
ad essere quelli noti, cioè:

\begin{equation}\label{eq:Ipotesi di linearità}
\lambda  \rightarrow 0 \qquad \Rightarrow \qquad E_n \rightarrow E^{(0)}_n
\end{equation}
Sappiamo che il set $ \ket{\phi}_n $ è ortonormale è completo, quindi possiamo espandervi qualsiasi
stato. In particolare, possiamo scrivere lo stato $ \ket{\psi_n} $ come:

\begin{equation} \label{eq:Decomposizione in autostati non perturbati}
\ket{\psi_n} = N(\lambda)\left( \ket{\phi_n} + \sum_{k\neq n} C_{nk}(\lambda) \ket{\phi_k} \right)
\end{equation}
Per esempio, potremmo scrivere il primo stato perturbato come:

\begin{equation}
\ket{\psi_1} = N(\lambda)\left( \ket{\phi_1} + C_{1,0}(\lambda)\ket{\phi_0} + C_{1,2}(\lambda)\ket{\phi_2} + \dots \right)
\end{equation}
$ N(\lambda) $ la si può calcolare dalle condizioni di normalizzazione, imponendo che 

\begin{equation}
\braket{\psi_n}{\psi_n} = 1
\end{equation}
Inoltre, ricordiamo che per \vref{eq:Ipotesi di linearità} devono valere le condizioni:

\begin{align}
N(0) &= 1 \nonumber \\
C_{nk}(0) &= 0
\end{align}
Sempre per ipotesi, scriviamo in serie di potenze i valori di $ C_{nk}(\lambda) $ e di $ E_n $:
\begin{align}
C_{nk}(\lambda) &= \lambda C_{nk}^{(1)} + \lambda^2 C_{nk}^{(2)} + \dots \nonumber \\
E_n &= E_n^{(0)} + \lambda E_{n}^{(1)} + \lambda^2 E_{n}^{(2)} + \dots
\end{align}
Ora possiamo scrivere l'equazione \vref{eq:Hamiltoniana perturbata} approssimando ai primi ordini:

\begin{align}
&(H_0 + \lambda H_1)\left( \ket{\phi_n} + \lambda \sum_{k\neq n} C_{nk}^{(1)} \ket{\phi_{k}} + \lambda^2 \sum_{k\neq n} C_{nk}^{(2)} \ket{\phi_{k}} + \dots \right) = \nonumber \\
&= (E_n^{(0)} + \lambda E_{n}^{(1)} + \lambda^2 E_{n}^{(2)} + \dots) \left( \ket{\phi_n} + \lambda \sum_{k\neq n} C_{nk}^{(1)} \ket{\phi_{k}} + \lambda^2 \sum_{k\neq n} C_{nk}^{(2)} \ket{\phi_{k}} + \dots \right)
\end{align}
Utilizziamo il principio di identità dei polinomi in termini delle prime due potenze di $ \lambda $ e otteniamo le seguenti uguaglianze:

\begin{align} \label{eq:Primi due ordini di perturbazioni}
&\lambda \left[ H_0 \sum_{k\neq n} C_{nk}^{(1)} \ket{\phi_k} + H_1 \ket{\phi_k} \right] = \lambda \left[ E_{n}^{(0)} \sum_{k\neq n} C_{nk}^{(1)} \ket{\phi_k} + E_{n}^{(1)} \ket{\phi_n} \right] \nonumber \\
&\lambda^2 \left[ H_0 \sum_{k\neq n} C_{nk}^{(2)}\ket{\phi_k} + H_1 \sum_{k\neq n} C_{nk}^{(1)}\ket{\phi_k} \right] = \lambda^2 \left[ E_{n}^{(0)} \sum_{k\neq n} C_{nk}^{(2)} \ket{\phi_k} + E_{n}^{(1)} \sum_{k\neq n} C_{nk}^{(1)} \ket{\phi_k} + E_{n}^{(2)}\ket{\phi_n} \right]
\end{align}


\subsection{Approssimazione al primo ordine}
Trattiamo inizialmente solo la prima equazione della \vref{eq:Primi due ordini di perturbazioni}.
Scriviamola in modo diverso utilizzando l'equazione dell'hamiltoniana imperturbata \vref{eq:Hamiltoniana imperturbata}:
\begin{align}
H_0 \ket{\phi_n} &= E_n^{(0)} \ket{\phi_n} \Rightarrow \nonumber \\
\Rightarrow H_0 \sum_{k\neq n} C_{nk}^{(1)} \ket{\phi_k} &= \sum_{k\neq n} C_{nk}^{(1)} H_0\ket{\phi_k} = \sum_{k\neq n} C_{nk}^{(1)} E_k^{(0)}\ket{\phi_k} 
\end{align}
Quindi l'equazione generale, ordinando i vari termini, sarà:
\begin{equation} \label{eq:Generatrice di C_nk}
E_{n}^{(1)} \ket{\phi_n} = H_1 \ket{\phi_n} + \sum_{k\neq n} C_{nk}^{(1)} (E_k^{(0)} - E_n^{(0)}) \ket{\phi_k}
\end{equation}
Ora moltiplichiamo ambo i membri per $ \bra{\phi_n} $:
\begin{gather}
\bra{\phi_n}E_n^{(1)}\ket{\phi_n} =  \bra{\phi_n} H_1 \ket{\phi_n} + \sum_{k\neq n} C_{nk}^{(1)} (E_k^{(0)} - E_n^{(0)}) \braket{\phi_n}{\phi_k} \nonumber \\
E_n^{(1)} = \bra{\phi_n}H_1\ket{\phi_n} \nonumber \\
\lambda E_n^{(1)} = \lambda \bra{\phi_n}H_1\ket{\phi_n}
\end{gather}
\begin{equation} \label{eq:Correziona al primo ordine}
\boxed{\lambda E_n^{(1)} = \bra{\phi_n}\lambda H_1\ket{\phi_n}}
\end{equation}

L'equazione appena trovata implica che il primo ordine dell'energia lo si può trovare semplicamente
calcolando il valore d'aspettazione dell'hamiltoniana perturbante. Inoltre, se l'operatore $ \lambda H_1 $ ha segno positivo, 
allora anche il termine aggiuntivo dell'energia $ \lambda E_n^{(1)} $ avrà lo stesso segno e viceversa. L'equazione è del tutto generale: vale sia quando vogliamo calcolare il valore d'aspettazione utilizzando
delle coordinate spaziali (ad esempio degli integrali in coordinate sferiche), sia quando
utilizziamo la notazione matriciale degli operatori.

Per calcolare il valore della costante $ C_{nk}^{(1)} $ moltiplichiamo per un generico $ \bra{\phi_m} $, con $ m \neq n $ l'equazione \vref{eq:Generatrice di C_nk} e otteniamo:

\begin{equation} \label{eq:Coefficienti teoria delle perturbazioni}
0 = \bra{\phi_m}H_1\ket{\phi_n} + (E_m^{(0)} - E_n^{(0)})C_{nm}^{(1)}
\end{equation}
\begin{equation}
\boxed{C_{nm}^{(1)} = \frac{\bra{\phi_m}H_1\ket{\phi_n}}{(E_n^{(0)} - E_m^{(0)})}}
\end{equation}
Questa formula servirà per la parte successiva, in cui calcoleremo le approssimazioni al secondo
ordine.

\subsection{Approssimazioni al secondo ordine}
Utilizziamo ora la seconda equazione della \vref{eq:Primi due ordini di perturbazioni}. Analogamente
al caso precedente, moltiplichiamo per $ \bra{\phi_n} $ l'equazione e otteniamo:

\begin{align}
&\bra{\phi_n} H_0 \sum_{k\neq n} C_{nk}^{(2)} \ket{\phi_k} + \bra{\phi_n} H_1 \sum_{k\neq n} C_{nk}^{(1)} \ket{\phi_k} = \nonumber \\
&= E_{n}^{(0)} \sum_{k\neq n} C_{nk}^{(2)} \braket{\phi_n}{\phi_k} + E_{n}^{(1)} \sum_{k\neq n} C_{nk}^{(1)} \braket{\phi_n}{\phi_k} + E_{n}^{(2)}\braket{\phi_n}{\phi_n}
\end{align}
\begin{align}
&\bra{\phi_n} H_1 \sum_{k\neq n} C_{nk}^{(1)} \ket{\phi_k} = E_{n}^{(2)} \nonumber \\
&E_{n}^{(2)} = \sum_{k\neq n} \bra{\phi_n}H_1\ket{\phi_k} C_{nk}^{(1)} = \sum_{k\neq n}\frac{\bra{\phi_n} H_1 \ket{\phi_k} \bra{\phi_k}H_1\ket{\phi_n} }{(E_n^{(0)} - E_k^{(0)})}
\end{align}
E quindi:

\begin{equation} \label{eq:Correzione al secondo ordine}
\boxed{
	E_n^{(2)} = \sum_{k\neq n}\frac{|\bra{\phi_n} H_1 \ket{\phi_k}|^2}{(E_n^{(0)} - E_k^{(0)})}		
}
\end{equation}
Abbiamo trovato quindi l'autovalore del termine di secondo ordine dell'energia. Vi sono alcune brevi
osservazioni da fare: in primo luogo osserviamo che se la matrice di $ H_1 $ è costituita da
coefficienti aventi lo stesso ordine di grandezza e se i livelli $ n $ e $ k $ sono vicini apportano delle correzioni più significative rispetto a dei valori di $ n $ e $ k $ lontani.
Inoltre, se il livello k-esimo dell'energia è maggiore del livello n-esimo, allora il corrispondente
termine di correzione è negativo; viceversa, se il livello k-esimo è minore, allora il termine di
correzione è positivo.

\subsection{Degenerazione}
Può accadere che nell'hamiltoniana imperturbata vi sia degenerazione, ovvero due o più stati
con lo stesso autovalore. Questo significherebbe per gli stati degeneri il valore della correzione
del coefficiente nell'equazione \vref{eq:Coefficienti teoria delle perturbazioni} esplode a $ \infty $ e così il termine ricavato per l'energia al secondo ordine. La strategia da applicare
per risolvere tale problema è quella di risolvere prima la degenerazione degli stati diagonalizzando
l'hamiltoniana e poi calcolare i termini di correzione delle energie come abbiamo fatto prima.
Per fare un esempio, supponiamo di avere la seguente situazione:

\begin{equation}
H_0=
\begin{pmatrix}
	E^{(0)} & 0       & 0         \\
	0       & E^{(0)} & 0         \\
	0       & 0       & E^{(0)}_3
\end{pmatrix}
\qquad
\lambda H_1 =
\begin{pmatrix}
	\lambda h_{11} & \lambda h_{12} & \lambda h_{13} \\
	\lambda h_{21} & \lambda h_{22} & \lambda h_{23} \\
	\lambda h_{31} & \lambda h_{32} & \lambda h_{33}
\end{pmatrix}
\end{equation}

Le due matrici sono hermitiane. Quello che bisogna fare è prendere $ H $ e risolvere la 
degenerazione diagonalizzando la matrice. Anzichè, però, diagonalizzare tutta la matrice è 
sufficiente diagonalizzare la sottomatrice contenente gli autovalori identici dell'energia. Nel 
nostro caso, occorre diagonalizzare semplicimente la sottomatrice $ 2\times2 $ di $ H $.
Diagonalizzare significa applicare una trasformazione unitaria $ U $ tale per cui:

\begin{equation}
UHU^\dagger = H_D
\end{equation}
Anzichè diagonalizzare interamente H, consideriamo solo il sottospazio degenere. $H_0$ ristretta a quello spazio è $a \cdot \mathbf{Id}$ dove $a$ è l'autovalore degenere. Considerando questo sottospazio possiamo diagonalizzare $H_1$ indipendentemente da $H_0$, infatti: 
\begin{align}
UHU^{\dagger} = U(H_0 + \lambda H_1)U^{\dagger} = UH_0U^{\dagger} + \lambda UH_1U^{\dagger} = H_0 + \lambda UH_1U^{\dagger}
\end{align}
L'ultimo passaggio segue dal fatto che $ H_0 $ è gia ristretta a una matrice identità per l'autovalore, quindi la sua trasformazione è identica. 
Ciò significa che basta semplicemente diagonalizzare la sottomatrice di $ H_1 $ relativa ai due
autovalori degeneri:

\begin{equation}
\begin{pmatrix}
	\lambda h_{11} & \lambda h_{12} \\
	\lambda h_{21} & \lambda h_{22} 
\end{pmatrix}
\end{equation}
\begin{gather}
\lambda \; det
\begin{pmatrix}
 h_{11} - w_1 &  h_{12} \\
 h_{21} &  h_{22} - w_2 
\end{pmatrix}
= 0 \nonumber \\
(\lambda h_{11} - w_1)(\lambda h_{22} -w_2 ) - \lambda^2 h_{12}h_{21} = 0 \nonumber \\
w_1, w_2 = \frac{h_{11} + h_{12}}{2} \pm \sqrt{\left(\frac{h_{11} - h_{22}}{2}\right)^2 + h_{12}h_{21}}
\end{gather}
Quindi la matrice $ H $ sarà così formata:
\begin{equation}
H=
\begin{pmatrix}
	E^{(0)} + \lambda w_1 & 0                     & \lambda h_{13}           \\
	0                     & E^{(0)} + \lambda w_2 & \lambda h_{23}           \\
	\lambda h_{31}        & \lambda h_{32}        & E^{(3)} + \lambda h_{33}
\end{pmatrix}
\end{equation}
Ovviamnente, se la degenerazione fosse stato di un ordine superiore al secondo, avremmo dovuto
procedere in maniera analoga, diagonalizzando la sottomatrice di una grandezza pari all'ordine
della degenerazione.

Trattiamo la degenerazione da un punto di vista diverso, seguendo lo stesso ragionamento che ci ha
portato al risultato in \vref{eq:Correziona al primo ordine}. In questo caso, anzichè avere distinti
valori $ \ket{\phi_n} $, avremmo più valori $ \ket{\phi^{(1)}_n},\ket{\phi^{(2)}_n},\ket{\phi^{(3)}_n}, \dots $ che hanno lo stesso valore di 
energia $ E^{(0)}_n $. Riferiamoci a questi stati con $ \ket{\phi^{(i)}_n} $. Sappiamo dal discorso
fatto in \vref{sec:Degenerazioni e autofunzioni in comune} che è possibile ortonormalizzare questo
insieme di stati degeneri, associandoli a distinti autovalori di un altro operatore hermitiano
che commuti con l'hamiltoniana. Supponiamo quindi di avere già un set ortonormale per cui valga che:

\begin{equation}
\braket{\phi^{(i)}_m}{\phi^{(j)}_n} = \delta_{mn}\delta_{ij}
\end{equation}
Cioè gli stati sono ortogonali rispetto agli indici $ m,n $ che rappresentano diversi livelli
dell'energia e contemporaneamente sono ortogonali rispetto agli indici $ i,j $ che invece 
rappresentano diversi autovalori di un operatore che commutante con l'hamiltoniana.

Riprendiamo l'equazione con cui siamo partiti in questa sezione, cioè la \vref{eq:Decomposizione in autostati non perturbati} e inseriamo al posto di $ \ket{\phi_n} $ il set di autostati degeneri.

\begin{equation}
\ket{\psi_{n}} = N(\lambda) \left( \sum_{i} \alpha_{i} \ket{\phi_{n}^{(i)} } + \lambda + \sum_{k\neq n} C^{(1)}_{nk} \sum_{i} \beta_{i} \ket{\phi_{k}^{(i)}} + \dots \right)
\end{equation}
Il set di $ \alpha_{i} $ e $ \beta_{i} $ costituiscono i coefficienti di Fourier degli autostati $ \ket{\phi^{(n)}_n} $ relativi all'operatore commutante che risolve la degenerazione. In quanto tali
vale che:

\begin{equation} \label{eq:Coefficienti di Fourier in perturbazione degenere}
\sum_{i}|\alpha_{i}|^2 = 1 \qquad \sum_{i}|\beta|^2 = 1
\end{equation}
E sono dei valori da determinare. Per farlo, utilizziamo il principio d'identità dei polinomi e
identifichiamo il primo ordine in $ \lambda $, come nella equazione \vref{eq:Primi due ordini di perturbazioni}. Per motivi di brevità scriviamo solo il risultato:

\begin{equation} \label{eq:Perturbazione stati degeneri}
H_0 \sum_{k\neq n}C_{nk}^{(1)}\sum_{i}\beta_{i}\ket{\phi_{k}^{(i)}} + H_1
 \sum_{i}\alpha_{i}\ket{\phi_n^{(i)}} = E_n^{(1)}\sum_{i}\alpha_{i}\ket{\phi_n^{(i)}} + E_n^{(0)} \sum_{k\neq n} C_{nk}^{(1)}\sum_{i} \beta_{i}\ket{\phi_{k}^{(i)}}
\end{equation}
Moltiplichiamo ambo i membri per uno stato degenere $ \bra{\phi_n^{(j)}} $ e osserviamo che
rimangono solo il secondo termine del lato sinistro e il primo termine del lato destro per le 
ipotesi di ortonormalità sopra scritte:

\begin{align}
\sum_{i}\alpha_i \bra{\phi_n^{(j)}} H_1 \ket{\phi_n^{(i)}} &= E_n^{(1)}\sum_{i}\alpha_i \braket{\phi_n^{j}}{\phi_n^{(i)}} \nonumber \\
\sum_{i}\alpha_i\bra{\phi_n^{(j)}} H_1 \ket{\phi_n^{(i)}} &= E_n^{(1)}\alpha_j \nonumber \\
\end{align}
\begin{equation} \label{eq:Primo ordine perturbazione degenere}
\boxed{\sum_{i}\alpha_i\bra{\phi_n^{(j)}}\lambda H_1 \ket{\phi_n^{(i)}} = \lambda E_n^{(1)}\alpha_j}
\end{equation}
Quella che abbiamo trovato è un'equazione soltanto. In realtà il problema è dato da un numero di 
equazioni pari all'ordine di degenerazione; infatti, in questo caso abbiamo moltiplicato per un 
unico stato degenere $ \bra{\phi_n^{(j)}} $ ma vi sono altre equazioni ottenute moltiplicando la
\vref{eq:Perturbazione stati degeneri} per altri stati (ad esempio $ \bra{\phi_n^{(j\pm1)}} , 
\bra{\phi_n^{(j\pm2)}} ,\dots $).
Per fare un esempio, supponiamo di avere un ordine di degenerazione pari a due. Allora avremo due
equazioni e tali equazioni saranno, dalla \vref{eq:Primo ordine perturbazione degenere}:

\begin{align}
\alpha_1\bra{\phi_n^{(1)}}\lambda H_1 \ket{\phi_n^{(1)}} + \alpha_2 \bra{\phi_n^{(1)}}\lambda H_1 \ket{\phi_n^{(2)}} = \lambda E_n^{(1)}\alpha_1 \nonumber \\
\alpha_1\bra{\phi_n^{(2)}}\lambda H_1 \ket{\phi_n^{(1)}} + \alpha_2 \bra{\phi_n^{(2)}}\lambda H_1 \ket{\phi_n^{(2)}} = \lambda E_n^{(1)}\alpha_2
\end{align}
Che, scritta in termini di elementi di matrice, diventa:
\begin{align} \label{eq:autovalori teoria perturbazione degenere primo ordine}
h_{11}\alpha_1 + h_{12}\alpha_2 = E_n^{(1)}\alpha_1 \nonumber \\
h_{21}\alpha_1 + h_{22}\alpha_2 = E_n^{(1)}\alpha_2
\end{align}
Sono due equazioni in tre incognite ($ \alpha_1, \alpha_2, E_n^{(1)} $). Per risolverà servirà
utilizzare il fatto sui coefficienti di Fourier \vref{eq:Coefficienti di Fourier in perturbazione degenere}.

Consideriamo un caso particolare: supponiamo che gli elementi di matrice $ h_{i,j} $ diano luogo
ad una matrice diagonale, cioè $ h_{i,j} = 0 $ per $ i \neq j $. Allora il termine di correzione
dell'energia $ E_n^{(1)} $ è dato dagli elementi diagonali della matrice.
Questo caso particolare si verifica quando l'hamiltoniana $ H_1 $ commuta con l'operatore che 
risolve la degenerazione (Cioè, quello che distingue i vari stati i-esimi).

Un caso più concreto potrebbe essere quello in cui l'atomo di idrogeno, che ha degenerazione per
$ L_z $ (ovvero esistono $ m $ stati con la stessa energia) è perturbato da un hamiltoniana $ H_1 $
tale per cui:

\begin{equation}
[H,L_z] = 0
\end{equation}
Prendiamo $ \ket{\phi_n^{(i)}} $ come autostati di $ L_z $. In questo caso osserviamo che:

\begin{align}\label{eq:perturbazione_diagonale}
L_z\ket{\phi_n^{(i)}} &= \hbar m^{(i)}\ket{\phi_n^{(i)}} \nonumber \\
0 = \bra{\phi_n^{(j)}}[H_1L_z]\ket{\phi_n^{(i)}} &= \bra{\phi_n^{(j)}} H_1L_z - L_zH_1 \ket{\phi_n^{(i)}} \nonumber \\
&=  \bra{\phi_n^{(j)}} H_1L_z\ket{\phi_n^{(i)}} - \bra{\phi_n^{(j)}} L_zH_1 \ket{\phi_n^{(i)}} \nonumber \\
&= \bra{\phi_n^{(j)}} H_1 \ket{L_z \,\phi_n^{(i)}} - \bra{L_z^\dagger \phi_n^{(j)}} H_1 \ket{\phi_n^{(i)}} \nonumber \\
&= \hbar m^{(i)}\bra{\phi_n^{(j)}} H_1\ket{\phi_n^{(i)}} - \hbar m^{(j)}\bra{\phi_n^{(i)}} H_1 \ket{\phi_n^{(j)}}  \nonumber \\
&= \hbar (m^{(i)} h_{ji} -m^{(j)} h_{ij}^{\dagger} ) \nonumber \\
&= \hbar (m^{(i)} -m^{(j)})h_{ji} \nonumber \\
\end{align}
Quindi possiamo osservare che:
\begin{equation}
h_{ji} = 0 \qquad \text{per} \qquad m^{(i)} \neq m^{(j)}
\end{equation}

\subsubsection{Rotatore rigido in un campo magnetico}
Un esempio in cui torna utile il discorso sulla degenerazione è il seguente: supponiamo di avere
un rotatore la cui hamiltoniana è descritta da 

\begin{equation}
H_0 = \frac{L^2}{2I}
\end{equation}
E un momento magnetico

\begin{equation}
\mathbf{\mu} = -\frac{qg}{2M}\mathbf{L}
\end{equation}

Immergiamo ora il rotatore in un campo magnetico di intensità $ \mathbf{B} $ diretto lungo l'asse
$ z $. Sappiamo che la perturbazione data dal campo magnetico sarà

\begin{equation}
H_1 = \mathbf{B \cdot \mu} = - \frac{gq}{2M}BL_z
\end{equation}

Ragioniamo ora sugli autovalori dell'energia totale. Da una parte abbiamo $ H_0 $, che contiene
$ L^2 $ e sappiamo che un set completo di autostati è dato dalle armoniche sferiche $ Y_{lm} $. 
Dall'altra parte abbiamo la perturbazione $ H_1 $ in cui compare $ L_z $ che contemporaneamente 
condivide il set completo di autostati dato dalle armoniche sferiche. Sappiamo, per quanto detto 
nella sezione \vref{sec:Degenerazioni e autofunzioni in comune} che se due operatori condividono lo 
stesso set ortonormale completo, allora commutano.
Questo significa che $ [H_1, L_z] = 0 $ e per quanto detto prima, la correzione dovuta alla
perturbazione è data dagli elementi di matrice diagonali
\begin{equation}
\bra{\phi_{n}^{(j)}}H_1 \ket{\phi_n^{(i)}} = -\frac{gqB}{2M} \bra{\phi_{n}^{(j)}}L_z \ket{\phi_n^{(i)}} = -\frac{gqB\hbar}{2M}m_l \delta_{j,i}
\end{equation}
Quindi potremmo subito scrivere gli autovalori dell'energia come:

\begin{equation}
E = E_{n}^{(0)} + E_{n}^{(1)}
\end{equation}
\begin{equation}
\boxed{E = \frac{l(l+1)\hbar^2}{2I} - \frac{gqB\hbar}{2M}m_l \qquad -l\leq m\leq +l}
\end{equation}

\subsection{Teoria delle perturbazioni al secondo ordine degenere}

Vediamo di trattare un caso molto particolare della teoria delle perturbazioni. Poniamoci nel caso
in cui abbiamo degenerazione sugli stati dell'energia e applichiamo la correzione al secondo ordine.
La formula che abbiamo ricavato, in caso di non degenerazione è la seguente:

\begin{equation}
E_{n}^{(2)} = \sum_{k\neq n} \bra{\phi_n}H_1\ket{\phi_k} C_{nk}^{(1)} = \sum_{k\neq n}\frac{\bra{\phi_n} H_1 \ket{\phi_k} \bra{\phi_k}H_1\ket{\phi_n} }{(E_k^{(0)} - E_n^{(0)})}
\end{equation}
Ora scomponiamo un singolo stato nei suoi stati degeneri, etichettandoli con un apice come abbiamo
fatto prima:

\begin{equation}
\ket{\phi_{n}} \qquad \rightarrow \qquad \sum_{i} \alpha_i \ket{u_{n}^{(i)}}
\end{equation}

Inseriamo ciò che abbiamo appena scritto nella correzione al secondo ordine. La formula, un po'
macchinosa, diventerà:
\begin{align}
\sum_{i}\alpha_{i}^{*}\bra{\phi_n^{(i)}}  E_{n}^{(2)} \sum_{r}\alpha_{r}\ket{\phi_n^{(r)}} &= \sum_{k\neq n}\frac{\sum_{i} \alpha_i^{*} \bra{u_{n}^{(i)}} H_1 \sum_{j} \beta_j \ket{u_{k}^{(j)}}  \sum_{j} \beta_k^{*} \bra{u_{k}^{(l)}} H_1\sum_{l} \alpha_m \ket{u_{n}^{(m)}} }{(E_n^{(0)} - E_k^{(0)})} \nonumber \\
\sum_{i,r} E_{n} \alpha_i^{*}\alpha_{r} \braket{u_{n}^{(i)}}{u_{n}^{(r)}} &= \sum_{k \neq n} \sum_{i,j,l,m} \alpha_{i}^{*}\beta_{j}\beta_{l}^{*}\alpha_{m} \frac{\bra{u_n^{(i)}} H_1 \ket{u_k^{(j)}} \bra{u_k^{(l)}}H_1\ket{u_n^{(m)}} }{(E_n^{(0)} - E_k^{(0)})}
\end{align}
Osserviamo che gli stati degeneri sono tra loro ortonormali, quindi possiamo scrivere più 
semplicemente:

\begin{equation}
\sum_{i,r} E_{n} \alpha_i^{*}\alpha_{r} \braket{u_{n}^{(i)}}{u_{n}^{(r)}} = \sum_{i,r} E_{n} \alpha_i^{*}\alpha_{r} \delta_{i,r} = \sum_{i} E_{n} \alpha_i\alpha_i^* = \sum_{i} E_{n} |\alpha_i|^2
\end{equation}
Questa è la forma più generale. A volte può accadere di calcolare una correzione alla energia $ E_{n}^{(2)} $ solo su uno stato degenere, mentre gli stati k-esimi non lo sono. In tal caso
l'equazione si semplifica e diventa:

\begin{align}
\sum_{i} E_{n} |\alpha_i|^2 = \sum_{k \neq n} \sum_{i,m} \alpha_{i}^{*}\alpha_{m} \frac{\bra{u_n^{(i)}} H_1 \ket{u_k} \bra{u_k}H_1\ket{u_n^{(m)}} }{(E_k^{(0)} - E_n^{(0)})} 
\nonumber \\
\sum_{i} E_{n} \alpha_i = \sum_{k \neq n} \sum_{i,m}\alpha_{m} \frac{\bra{u_n^{(i)}} H_1 \ket{u_k} \bra{u_k}H_1\ket{u_n^{(m)}} }{(E_k^{(0)} - E_n^{(0)})}
\end{align}
Come si può osservare, questa è un'equazione matriciale agli autovalori. Gli indici della matrice
sono $ i $ e $ m $, mentre i coefficienti $ \alpha_i $ e $ \alpha_m $ sono da calcolare. Osserviamo
che l'equazione ricavata è analoga alla  \vref{eq:autovalori teoria perturbazione degenere primo ordine} e quindi basta procedere in maniera analoga.

Facciamo un esempio, prendendo come spunto un tema d'esame di Zaffaroni:

\begin{esercizio}

Una particella di spin uno ha Hamiltoniana

\begin{equation}
H_0  = \frac{\epsilon}{\hbar^2}S_z^2 = \epsilon
\begin{pmatrix}
1 & 0 & 0 \\
0 & 0 & 0 \\
0 & 0 & 1 \\
\end{pmatrix}
\end{equation}
ed è perturbata da un campo magnetico nella direzione x.
\begin{itemize}
	\item Trovare le correzioni per l'energia al II ordina nella perturbazione
\end{itemize}

\noindent\textbf{Soluzione}

Le matrici delle particelle con spin uno sono analoghe a quelle del momento angolare per $ l=1 $,
quindi potremmo scrivere:

\begin{equation}
L_z = \hbar
\begin{pmatrix}
1 & 0 & 0 \\
0 & 0 & 0 \\
0 & 0 & -1 \\
\end{pmatrix}
L_+ = \sqrt{2}\hbar
\begin{pmatrix}
0 & 1 & 0 \\
0 & 0 & 1 \\
0 & 0 & 0 \\
\end{pmatrix}
L_z = \sqrt{2}\hbar
\begin{pmatrix}
0 & 0 & 0 \\
1 & 0 & 0 \\
0 & 1 & 0 \\
\end{pmatrix}
\end{equation}
La perturbazione sarà:

\begin{equation}
H_1 = - \vec{M}\cdot\vec{B} = - k S_x = - k \frac{S_+ + S_-}{2}
\end{equation}
Osserviamo che l'hamiltoniana imperturbata è espressa in funzione dell'operatore $ S_{z}^{2} $,
i cui autostati sono:
\begin{equation}
\begin{pmatrix}
1 \\
0 \\
0 \\
\end{pmatrix},
\begin{pmatrix}
0 \\
1 \\
0 \\
\end{pmatrix},
\begin{pmatrix}
0 \\
0 \\
1 \\
\end{pmatrix}
\end{equation}
A cui ci riferiremo rispettivamente con $ \ket{1}, \ket{0}, \ket{-1} $
\end{esercizio}

Osserviamo ora la degenerazione: gli stati $ \ket{-1} $ e  $ \ket{1} $ hanno lo stesso autovalore
dell'energia $ E_{1,-1}^{(0)} = \epsilon $, mentre $ \ket{0} $ è l'unico stato con energia $ E_0^{(0)} = 0 $.
Calcoliamo la correzione al secondo ordine per $ \ket{0} $. In questo caso il livello n-esimo non
è degenere, quindi $ i = 1, i = m $
\begin{equation}
E_{0}^{(0)} = \sum_{k \neq n} 
\end{equation}
















\end{document}