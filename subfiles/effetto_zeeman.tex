\documentclass[../mq.tex]{subfiles}
\begin{document}

\newpage
\section{Effetto Zeeman}
\subsection{Effetto Zeeman normale}
Applichiamo un campo magnetico esterno costante ad un atomo di idrogeno. Sappiamo che il momento orbitale dell'elettrone produce un momento magnetico:
\begin{equation}
\mu = -\frac{e}{2m_e} L
\end{equation}
che interagisce con il campo magnetico costante con hamiltoniana:
\begin{equation}
H_B = -\mu \cdot B = \frac{e}{2m_e}L \cdot B
\end{equation}
Se scegliamo $B$ lungo l'asse $z$ allora la perturbazione è diagonale sulla base degli autostati dell'atomo di idrogeno imperturbato $\ket{n,l,m}$ quindi la variazione di energia si ricava subito come:
\begin{equation}
\Delta E_{nlm} = \avg{nlm}{H_B} = \frac{eB}{2m_2}\avg{nlm}{L_z} = \frac{eB\hbar}{2m_e} m
\end{equation}
Ogni livello degenere in $l$ viene splittato in $(2l+1)$ differenti righe, un numero dispari. 

\subsection{Effetto Zeeman anomalo}
Sperimentalmente si osserva però che alcuni atomo sottoposti a un campo magnetico produono un numero pari di linee. Questo effetto fu chiamato \emph{Effetto Zeeman anomalo} perchè non poteva essere spiegato prima dell'introduzione dello spin. Vediamo ora come l'introduzione dello spin come contributo al momento magnetico dell'elettrone risolva completamente il problema dello splitting delle righe spettrali. 

Il contributo dello spin all'hamitoniana di perturbazione è:
\begin{equation}
H_B = \frac{e}{2m_e} (L +2S)\cdot B
\end{equation}
dove il fattore due è il \emph{fattore giromagnetico} dovuto alla precessione di Thomson per lo spin dell'elettrone. 

Nel calcolare la perturbazione dobbiamo tenere conto del fatto che l'effetto dell'accoppiamento spin-orbita\footnote{Non confondiamoci: l'accoppiamento spin-orbita è dovuto ad un campo magnetico B che l'elettrone sente per effetti relativistici, legato alla sua interazione con il nucleo e al suo movimento. Non è un campo magnetico esterno!} può essere più grande o più piccolo dell'effetto dovuto al campo magnetico esterno, quindi bisogna distinguere i due casi.

Il rapporto tra i due effetti è:
\begin{equation}
\frac{H_B}{H_{SO}} = \frac{\mu_B B}{Z} 
\end{equation}

\begin{itemize}
	\item Se $B \ll Z / \mu_B$ allora si ha un \emph{campo debole} e l'effetto dell'accoppiamento spin-orbita è preponderante. Dovremo quindi considerare lo spin-orbita nell'hamiltoniana imperturbata e l'effetto Zeeman come una perturbazione.
	\item Se $B \gg Z/\mu_B$ allora lo spin-orbita diventa anch'esso una perturbazione, di ordine minore rispetto a $H_B$. L'effetto Zeeman si considera come una perturbazione per l'hamiltoniana classica dell'atomo di idrogeno.
\end{itemize}

\subsubsection{Campo debole}
Nel caso in cui il campo esterno $B$ sia debole, l'accoppiamento spin-orbita fa parte dell'hamiltoniana imperturbata:
\begin{equation}
H_0 = \frac{p^2}{2m_e} - \frac{Ze^2}{4\pi\epsilon_0}\frac{1}{r} + \frac{1}{2m_e^2c^2}\frac{Z e^2}{4\pi\epsilon_0 }\,\frac{L\cdot S}{r^3}
\end{equation}
Utilizziamo la base degli autovettori del momento totale $J$ per esprimere $H_0$ e $H_B$. Consideriamo $B$ lungo l'asse $z$:
\begin{equation}
\frac{eB}{2m_e} \avg{\phi_{j,m_j,l}}{L_z + 2S_z} = \frac{eB}{2m_e} \avg{\phi_{j,m_j,l}}{J_z + S_z}
\end{equation}

$J_z$ è ovviamente diagonale sulla base del momento angolare totale e porta un contributo di $(eB\hbar/2m_e)m_j$. Cosa possiamo dire della perturbazione data da $S_z$?.

Prima di tutto osserviamo che l'hamiltoniana imperturbata contiene l'accoppiamento spin-orbita, quindi gli stati con diversi $l$ e $j$ hanno energie diverse. La degenerazione rimasta è quella distinta dall'operatore $J_z$: ci sono $(2j+1)$ vettori $\ket{n,l,j,m_j}$ con uguale $l$ e $j$.  
E' necessario quindi utilizzare la teoria delle perturbazioni degenere per calcolare le variazioni di energia sui vettori di $\ket{n,l,j,m}$ con $n,l,j$ fissati. 
All'interno dei sottospazi $\ket{n,l,j}$ l'operatore $J_z$ distingue i vettori, ma esso commuta anche con la perturbazione $S_z$, quindi questa pertubazione è diagonale nel sottospazio (vedi \vref{eq:perturbazione_diagonale}). 

Non è necessario quindi diagonalizzare la perturbazione, basta calcolare:
\begin{equation}
\frac{eB}{2m_e} \avg{\phi_{nljm_j}}{S_z}
\end{equation}
Utilizziamo un metodo "grafico": possiamo immaginare $J$ come un vettore, costante del moto. $J$ è la somma di $L$ e di $S$, non più costanti del moto, ma costanti in modulo. $L$ ed $S$ precedono lungo la direzione di $J$. Le componenti di $J_z$ e $S_z$, le loro proiezioni lungo l'asse $z$ sono costanti, mentre le altre componenti in media danno zero. Quindi la media di $S_z$ si può calcolare come un proiezione vettoriale:
\begin{equation}
S \;\rightarrow J \frac{(S\cdot J)}{J^2} = J \,\frac{(S\cdot J)}{\hbar^2 j(j+1)}
\end{equation}
E' necessario quindi solamente calcolare $S\cdot J$, sapendo che $J=L+S$:
\begin{equation}
S\cdot J = S\cdot L + S^2 = \ume[J^2 - L^2 -S^2] +S^2 = \ume[J^2 +S^2 -L^2]
\end{equation}
Applicando questo operatore ai vettori della base $\ket{ljm_j}$:
\begin{equation}
 \ume[J^2 +S^2 -L^2]\, \ket{ljm_j} = \frac{\hbar^2}{2} [j(j+1)+\frac{3}{4} - l(l+1)]\,\ket{ljm_j}
\end{equation}
Quindi il calcolo di $<S_z>$ dà:
\begin{equation}
<S_z> = \hbar m_j \frac{j(j+1) -l(l+1 -\frac{3}{4})}{2j(j+1)}
\end{equation}
La correzione dovuta all'effetto zeeman con campo debole è quindi:
\begin{equation}
\Delta E_B = \frac{eB\hbar}{2m_e}\, m_j\left(1 + \frac{j(j+1) -l(l+1 -\frac{3}{4})}{2j(j+1)} \right)
\end{equation}
Inseriamo $j= l\pm 1/2$:
\begin{equation}
\boxed{
\Delta E_B = m_j \left(\frac{eB\hbar}{2m_e}\right) \left(1 \pm \frac{1}{2l +1} \right)}
\end{equation}
Vediamo come questa pertubazione elimina completamente la degeneraizone di $l$ ed $m$. Lo splitting tra le righe che hanno un certo $j$ è uguale, ma per diversi $j$ si ha uno splitting diverso, quindi il pattern è complesso. Inoltre notiamo che il numero di righe è pari perchè $m_j$ lo è.

\subsubsection{Campo forte}
Se il campo magnetico esterno $B \gg Z/\mu_B$ allora l'accoppiamento spin-orbita può essere tralasciato. In tal caso l'hamiltoniana imperturbata risulta:
\begin{equation}
H_0 = \frac{p^2}{2m_e} - \frac{Ze^2}{4\pi\epsilon_0 }\frac{1}{r} 
\end{equation}
e il campo magnetico esterno è una perturbazione. Inoltre ora $H_B$ commuta con $H_0$, che è espressa sulla base dei vettori dei singoli momenti $\ket{n,l,m_l,s,m_s}$ e commuta anche con $L_z$ ed $S_z$, quindi è diagonale in questa base.
\begin{equation}
\Delta E_B = \frac{eB\hbar}{2m_e} (m_l +2m_s)
\end{equation}

\end{document} 