\documentclass[../mq.tex]{subfiles}
\begin{document}

\newpage
\section{Particella nella scatola}\label{sec:particella_nella_scatola}
Consideriamo una situazione fisica molto chiarificatrice per quanto riguarda la risoluzione dell'equazione agli autovalori della funzione d'onda. 

Abbiamo un sistema formato da due pareti: una a $x=0$ e una a $x=a$. Il potenziale del sistema è:
$$\begin{cases}
V=+\infty   & x \leq 0; \; x\geq a\\
V= 0 & 0 < x < a
\end{cases}$$

\begin{figure}[h]
\centering
\includegraphics[width=.8\linewidth]{./img/particella_scatola_infinita}
\caption{}
\label{fig:particella_scatola_infinita}
\end{figure}


Il significato di un potenziale infinito sulle parete è che l'urto della particella è perfettamente elastico.

Poniamoci nel caso in cui la particella sia compresa nell'intervallo $ 0 < x < a $ e scriviamo l'equazione \vref{eq:equazione schrodinger indipendente dal tempo}:
\begin{equation}
\pdv{x}{u(x)}{2} +\frac{2mE}{\hbar^2} u(x)=0
\end{equation}
Notiamo che si tratta di un'equazione differenziale del secondo ordine omogenea. Per risolverla dobbiamo distinguere due casi: quello in cui $ E > 0 $ e  quello in cui $ E < 0 $. Se $E<0$ allora l'equazione differenziale diventa:
\begin{equation}
\pdv{x}{u(x)}{2} - k^2 u(x)=0 \qquad \text{con} \qquad k^2 = \frac{2m|E|}{\hbar^2}
\end{equation}
La soluzione  generale è:

\begin{equation}
\begin{cases}
u(x) = C_1 e^{kx} + C_2 e^{-kx} \\
u(0) = 0 \\
u(a) = 0
\end{cases}
\end{equation}

Il sistema non si può risolvere perchè una soluzione di questo tipo non si può raccordare con lo 0 ai bordi. 
Consideriamo quindi $E>0$. In questo caso si ha:

\begin{equation}
\pdv{x}{u(x)}{2} + k^2 u(x)=0
\end{equation}
e la soluzione più generale è 
$$A \sin(kx)+B \cos(kx)$$.
Imponiamo le condizioni al contorno e otteniamo che:
\begin{align*}
u(0)=0 \quad &\rightarrow\quad B=0 \\
u(a)= 0 \quad &\rightarrow\quad ka = n\pi 
\end{align*}

Le condizioni al contorno determinano quindi la quantizzazione dell'energia, lo spettro discreto di autovalori per $H$.
\begin{equation}\label{eq:energia_scatola}
E_n = \frac{\hbar^2\pi^2n^2}{2ma^2}
\end{equation}
Per calcolare il valore di $ A $ è necessario sfruttare la condizione di normalizzazione dell'autofunzione:

\begin{equation}
\intf u^*(x)u(x) dx = 1
\end{equation}
Si può calcolare facilmente l'integrale:

\begin{align*}
1 &= \int_{0}^{a} u^* u = \int_{o}^{a} |A|^2 sin^2(kx) dx =  \\
  &= |A|^2 \int_{0}^{a} \frac{1 - cos(2kx)}{2} = \\
  &= \frac{A^2}{2} x\bigg|^a_0 - \frac{A^2}{4k}\sin(2kx)\bigg|^a_0 = \\
  &= |A|^2\frac{a}{2} - 0 \\
\end{align*}
Da cui segue:

\begin{equation}
A = \sqrt{\frac{2}{a}}  
\end{equation}
Riassumendo, le autofunzioni del problema della particella della scatola sono:
\begin{equation}
	u_n(x) = \sqrt{\frac{2}{a}}\sin\frac{n\pi x}{a}
\end{equation}

\subsection{Osservazioni}
\subsubsection{Ortonormalità delle autofunzioni}
Possiamo osservare facilmente che le autofunzioni trovate sono ortonomali.
\begin{align}
\nonumber \int_{0}^a dx\,u_n^*(x)\,u_m(x) &= \frac{1}{a}\int_{0}^{a} dx\,\left\{ \cos \frac{(n-m)\pi x}{a} - \cos \frac{(n+m)\pi x}{a} \right\} \\
 &= \frac{\sin (n-m)\pi}{(n-m)\pi}-\frac{\sin (n+m)\pi}{(n+m)\pi}\\
\nonumber &= 0 \qquad \mbox{se } n \ne m\\
\nonumber &= 1 \qquad \mbox{se } n = m
\end{align}

Quindi si ha che:
\begin{equation}
\int_{0}^{a} dx\,u_n^* (x)\,u_m(x) = \delta_{nm}
\end{equation}

\subsubsection{Momento medio}
Calcoliamo il momento medio per ogni autostato:
\begin{equation}
<p> = \int_{0}^{a} = dx \,u_n^*(x) \left(-i\hbar \pdv{x}{u_n(x)}{}\right) = -i\hbar \int_{0}^{a} dx\, \frac{d}{dx} \frac{(u_n(x))^2}{2} = 0
\end{equation}
In media il momento è nullo: infatti per ogni valore dell'energia la particella ha la stessa probabilità di muoversi verso sinistra o verso destra nella scatola. La distribuzione simmetrica rispetto all'origine del momento porta la sua media a 0. 


\subsubsection{Energia minima o di punto zero}

Qual è l'energia minima? Non può essere $ E_0 $ perchè si otterrebbe $ \waf{} = 0 $, mentre la particella deve esistere. Dobbiamo partire quindi da $n=1$, cioè la cosiddetta \emph{Energia del punto zero}.

Cerchiamo di dare un'interpretazione fisica all'energia del punto zero. Calcoliamo $ <p^2> $. Per ogni autostato possiamo calcolarlo dall'autovalore dell'energia, che è definito e quindi $<E_n> = E_n$ (l'energia è tutta cinetica).

\begin{equation}
<p^2> = 2mE_n = \frac{\hbar^2\pi^2 n^2}{a^2}
\end{equation}

Quanto vale $ \Delta p $ visto che p medio è nullo? So che $ \Delta x = a $. Mentre il valor medio è nullo, perchè p può essere positivo o negativo, nel calcolo del $ \Delta p $ è $ 2|p| $. Quindi

\begin{equation}
\Delta x \Delta p = a \; \frac{2\pi\hbar}{a}\sim h
\end{equation}
Quindi l'energia di punto zero deriva dal principio di indeterminazione di Heisenberg. La posizione della particella è confinata tra 0 e a. Di conseguenza il momento non può mai essere esattamente determinato, ma deve avere un $\Delta p$. Quindi la particella non può mai essere esattamente ferma ($p=0$ e $\Delta p = 0$).
\paragraph{Esempio}
Facciamo un esempio per un oggetto di dimensioni macroscopiche:
Prendiamo $ E_1 $

\begin{align}
E_1 &= \frac{\pi\hbar^2}{2ma^2} \qquad \text{con} \qquad m = 10g \qquad a = 10cm \nonumber \\
&= \frac{6.6\cdot 10^{-34}\pi^2}{8\pi^2\cdot 10^{-2}}\sim 5.5 \cdot 10^{-52} J
\end{align}


\subsubsection{Quantizzazione del momento}
L'energia della particella nella scatola è quantizzata, ma il momento è quantizzato? Proviamo a calcolare gli autostati dell'energia con p applicato.

\begin{equation}
pu_1(x) = -i\hbar\pdv{x}{}{}\sqrt{\frac{2}{a}} \sin \frac{\pi x}{a} = -i \hbar \sqrt{\frac{2}{a}}\frac{\pi}{a}\cos\frac{\pi x}{a}
\end{equation}

Gli autostati non sono uguali. Inoltre gli operatori hamiltoniano e momento commutano, ma non hanno autostati uguali. 

Trasformiamo con Fourier un autostato dell'hamiltoniana nello spazio dei momenti:
\begin{equation}
u_1(p) = \frac{1}{\sqrt{2\pi\hbar}}\intf u_1(x)e^{-ipx/\hbar} = \frac{1}{\sqrt{2\pi\hbar}}\sqrt{a\hbar}\frac{(e^{ipa/\hbar}+1)}{\pi^2-p^2a^2/\pi^2}
\end{equation}

La formula mostra che la distribuzione dei momenti nello stato fondamentale è continua.

\begin{equation}
\left| u_1(p) \right|^2 = \frac{2a\pi}{\hbar}\frac{1+\cos(ap/\hbar)}{(\pi^2 - p^2a^2/\hbar^2)^2}
\end{equation}

Il problema sta nel fatto che la particella non è libera, ma confinata. Quindi quando abbiamo detto che abbiamo calcolato $ p^2 = 2mE $ va bene per il valor medio, ma in realtà per la particella confinata il momento è una funzione continua. 

In sostanza, l'energia è quantizzata, il momento no. Il perchè dipende dal fatto che la particella non è libera.

\subsubsection{Parità e simmetria delle soluzioni}
Notiamo che tutte le soluzioni sono oscillatorie, con una simmetria attorno ad $ a/2 $. La funzione è pari rispetto all'origine. Operando una traslazione:
$$x \; \rightarrow \; x-\frac{a}{2}$$

\begin{equation}
u_n(x) = \begin{cases}
\sin \frac{n\pi}{a}x \qquad \text{n pari} \\ 
\cos \frac{n\pi}{a}x \qquad \text{n dispari}
\end{cases}
\end{equation}

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{./img/soluzioni_particella_scatola}
\caption{Autosoluzioni al variare di $ n $}
\label{fig:soluzioni_particella_scatola}
\end{figure}


La parità è sempre stata considerata una simmetria. Nel 1963 fu discusso della violazione della parità. Teniamocela da parte; poi lo riprenderemo. Abbiamo solo definito l'operatore parità come l'operatore che manda

\begin{equation}
x \rightarrowtail -x
\end{equation}

Autovalore $ +1 $: funzione pari. Autovalore $ -1 $: funzione dispari.

\subsection{Problema della normalizzazione delle onde piane}
Ritorniamo al problema della particella nella scatola. Facciamo tendere $ a \rightarrow +\infty $, cioè facciamo in modo che il potenziale $ V(x) $ sia nullo ovunque. L'equazione agli autovalori dell'energia diventa:

\begin{equation}
\pdv{x}{u(x)}{2} + \frac{2mE}{\hbar^2}u(x) = \pdv{x}{u(x)}{2} + k^2u(x) = 0
\end{equation}

Risolvendo si ottengono due soluzioni linearmente indipendenti:

\begin{equation} \label{eq:soluzioni onde piane}
u(x) = e^{\pm ikx}
\end{equation}

Tutte le soluzioni dell'equazione sono una combinazione lineare delle due. Il problema che sorge è però dovuto alla normalizzazione, infatti

\begin{equation}
\intf dx\; \left| Ae^{ikx} + Be^{-ikx} \right|^2 = +\infty \qquad \forall A,B \in \mathbb{R}
\end{equation}

Esistono 3 modi per risolvere il problema:

\begin{enumerate}
	\item
	\textbf{Imporre delle condizioni al contorno periodiche}
	Anzichè imporre $ \psi(\pm a/2) = 0 $ si pongono delle condizioni periodiche su intervalli più piccoli, del tipo
	
	\begin{equation}
	\psi(x) = \psi(x+a')
	\end{equation}
	
	Così facendo, se prendiamo ad esempio l'onda $ Ce^{ikx} $ e utilizziamo le condizioni al contorno con $ x = 0 $:
	
	\begin{align}
	e^{ikx} &= e^{ik(x+a)} \nonumber \\
	1 &= e^{ika} \nonumber \\
	k &= \frac{2\pi n}{a} \nonumber \\
	p &= \frac{2n\pi\hbar}{a}
	\end{align}
	
	La condizione di normalizzazione diventa:
	
	\begin{align}
	|C|^2 \intf \left| e^{\pm ikx} \right| dx &= 1 \nonumber \\
	|C|^2 \int_{0}^{a} \left| e^{\pm ikx} \right|^2 dx &= 1 \nonumber \\
	|C|^2 a = 1 \qquad \Rightarrow \qquad C &= \frac{1}{\sqrt{a}}
	\end{align}
	
	Quindi le soluzioni saranno combinazioni lineari delle due funzioni:
	
	\begin{equation}
	\frac{1}{\sqrt{a}}e^{-ikx} \qquad \frac{1}{\sqrt{a}}e^{ikx}
	\end{equation}
	
	\item
	\textbf{Utilizzare pacchetti d'onda}
	Possiamo considerare un'onda piana come un caso speciale di un'onda della forma
	
	\begin{equation}
	\waf{} = \intf dp\;\phi(p)\frac{e^{ipx/\hbar}}{\sqrt{2\pi\hbar}} \qquad \text{con} \qquad \phi(p) = \sqrt{2\pi\hbar}\delta(p-\hbar k)
	\end{equation}
	
	Discorso intuitivo : l'onda è un pacchetto infinitamente "piccato". Se lavoriamo invece con una funzione $ \phi(p) $ con una funzione $ \sqrt{2\pi\hbar}g(p-\hbar k) $ molto piccata (ma non tanto quanto la delta di Dirac), possiamo scrivere:
	
	\begin{align}
	\waf{} &= \intf dp\;e^{ipx/\hbar}g(p-\hbar k) \nonumber \\
	&=  e^{ikx} \intf dq\;e^{iqx/\hbar}g(q)
	\end{align}
	
	Così facendo la funzione d'onda è una funzione a quadrato sommabile e sussite la condizione di normalizzazione. Notare che in sostanza abbiamo moltiplicato un'onda piana per un picco 'molto largo'. Abbastanza da considerarsi costante. L'incertezza sul momento risulta dell'ordine di:
	
	\begin{equation}
	\Delta p \sim \frac{\hbar}{larghezza\; del\; pacchetto} \sim \frac{\hbar}{\intf dq\;e^{iqx/\hbar}g(q)}
	\end{equation} 
	
	Se la larghezza del pacchetto è macroscopica, allora l'incertezza sul momento è trascurabile.
	
	\item
	\textbf{Utilizzare la corrente di probabilità}
	Sappiamo che il problema alla base è che un'onda piana non è confinata in nessuna regione finita dello spazio. Ciò significa che la probabilità di trovarla in un punto è nulla. Un modo per aggirare il problema è scrivere la densità di corrente di probabilità, o il flusso dell'onda piana:
	
	\begin{align}
	\waf{} &= C e^{ipx/\hbar} \nonumber \\
	j(x) &= -\frac{i\hbar}{2m}\left[ |C|^2 e^{-ipx/\hbar}\left( \frac{ip}{\hbar} \right) e^{ipx/\hbar} - |C|^2 e^{ipx/\hbar}\left( -\frac{ip}{\hbar} \right) e^{-ipx/\hbar} \right] \nonumber \\
	&= -\frac{\hbar|C|^2}{2m}\left[ \frac{2ip}{\hbar} \right] \nonumber \\
	&= |C|^2 \frac{p}{m}
	\end{align}
	$  $
	Se notiamo, $ p/m $ è una velocità, mentre $ |C|^2 $ rappresenta invece il numero di particelle per metro che si muovono lungo la direzione x (perchè?). Quindi, immaginando che le particelle si muovano lungo una sola dimensione di lunghezza $ L_x $
	
	\begin{equation}
	|C|^2\frac{p}{m}  = \frac{N_{particelle}}{L_x}\frac{L_x}{t} = \frac{N_{particelle}}{tempo}
	\end{equation}
	
	In tal modo $ |C|^2 $ risulta calcolabile in modo definito.
\end{enumerate}

\begin{figure}[h]
\centering
\includegraphics[width=0.71\linewidth]{./img/flusso}
\caption{}
\label{fig:flusso}
\end{figure}


\newpage

\end{document}