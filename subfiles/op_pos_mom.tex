\documentclass[../mq.tex]{subfiles}
\begin{document}

\newpage
\section{Operatori posizione e momento}\label{sec:momento}
\subsection{Operatori e osservabili fisiche}
Un operatore è un'applicazione che agisce su una funzione d'onda. Un osservabile fisica  è il valore di aspettazione di un operatore e si calcola come:
\begin{equation}
<O> =\intf dx\;\wafc{}\;O\;\waf{}
\end{equation}
\subsection{Operatore posizione}
Avendo la distribuzione di probabilità $\waf{}$, possiamo calcolare il valore di aspettazione di $x$...$x^n$ oppure $f(x)$. E' sufficiente calcolare:
\begin{equation}
<x> = \intf dx\;\wafc{}\;x\;\waf{}
\end{equation}
Poichè $\waf{}$ all'infinito va a 0 più velocemente di ogni potenza di $x$ ed è continua non ci sono problemi per l'esecuzione dell'integrale.

\textbf{N.B.:} L'operatore posizione $x$ è un operatore che \emph{agisce} sulla funzione d'onda, quindi non va inteso come nella meccanica classica come una funzione del tempo. In particolare $$\pdv{t}{x}{} = 0$$

\subsubsection{Operatore Momento}

Sappiamo, dalla cinematica, che 

\begin{align}
p = mv = m \frac{dx}{dt} \\ \nonumber
<p> = <mv> = m<v> = m\frac{d<x>}{dt}
\end{align}

Calcoliamo quindi il valore d'aspettazione $ <p> $:

\begin{align}
<p> = m\frac{d}{dt}\intf x\;|\psi|^2dx = m\int dx \left[\pdv{t}{\psi^*}{}x\psi + \psi^*\pdv{t}{x}{}\psi + \psi^*x\pdv{t}{\psi}{}\right] \\ \nonumber
\end{align}
Dove il secondo termine nell'integrale è nullo in quanto come abbiamo detto $x$ non dipende da $t$. Utilizzo l'equazione di Schroedinger per i due termini nell'integrale e ritroviamo:

\begin{equation}
\frac{\hbar}{2i}\int dx \left[ \pdv{x}{\psi^*}{2}x\psi - \psi^* x \pdv{x}{\psi}{2} \right] = (*)
\end{equation}

Studiamo il primo termine racchiuso nelle parentesi:

\begin{align}
\pdv{x}{\psi*}{2} x\waf{} &=\pdv{x}{}{}\left[ \pdv{x}{\psi^*}{} x \psi \right] - \pdv{x}{\psi^*}{} - \pdv{x}{\psi^*}{}x\pdv{x}{\psi}{} \\ \nonumber
&=\pdv{x}{}{}\left[ \pdv{x}{\psi^*}{}x\psi \right] - \pdv{x}{(\psi^*\psi)}{} + \psi^*\pdv{x}{\psi}{} \\&- \pdv{x}{}{}\left[ \psi^*x \pdv{x}{\psi}{} \right] + \psi^*\pdv{x}{\psi}{} + \psi^* x \pdv{x}{\psi}{2}
\end{align}

Sommando il tutto otteniamo:

\begin{align}
(*) &= \intf \pdv{x}{}{} \left[ \pdv{x}{\psi^*}{}x\psi -  \psi\psi^* + \pdv{x}{\psi}{}x\psi^*\right] + 2\psi\pdv{x}{\psi^*}{}dx \\ \nonumber
&=\intf 2\psi\pdv{x}{\psi^*}{}dx
\end{align}

Analogamente all'equazione di continuità, il primo termine dell'integrale tende a zero, perchè ai limiti all'infinito $ \psi $ decresce rapidamente.

In conclusione, abbiamo ottenuto:

\begin{align}
<p> = \intf \psi^*\frac{\hbar}{i}\pdv{x}{\psi}{}\,dx \qquad \Rightarrow \qquad\boxed{ p_x = -i\hbar\pdv{x}{}{}}
\end{align}

L'operatore calcolato è quindi un operatore differenziale, non moltiplicativo come la posizione. Si può dimostrare, integrando per parti, che:

\begin{equation}
<p> = <p>^*
\end{equation}

quindi le osservabili sono reali perchè l'operatore è hermitiano.

\subsubsection{Operatore Hamiltioniano}

Definiamo operatore hamiltioniano l'operatore la cui azione è data da:

\begin{equation}
H\psi = i\hbar\pdv{t}{}{}\psi
\end{equation}

Tratteremo più avanti il fatto che l'operatore 

\begin{equation}
H = \frac{p^2}{2m} + V(x)
\end{equation}

e che l'energia $ E $ è un autovalore di H.

\subsection{Spazio dei momenti e spazio delle coordinate}


Dall'equazione \vref{eq:phip} abbiamo le nostre funzioni $ \psi(x,0) $ e$ \Phi(p) $ che valgono rispettivamente:

\begin{align}
\psi(x,0) &= \frac{1}{\sqrt{2\pi\hbar}}  \int_{-\infty}^{+\infty} dp \, \phi(p) e^{ipx/\hbar} \\ \nonumber
\phi(p) &= \frac{1}{\sqrt{2\pi\hbar}} \int_{-\infty}^{+\infty}dx \, \psi(x,0)e^{-ipx/\hbar}
\end{align}

Proviamo a calcolare la quantità:

\begin{equation}
\int|\psi^2| dx
\end{equation}

Possiamo riscriverla come 

\begin{align}
\int dp \, \phi(p)\,\phi^*(p)  &=  \int dp \, \phi^*(p) \int dx \, \psi(x,0) e^{-ipx/\hbar} \\ \nonumber
&= \frac{1}{\sqrt{2\pi\hbar}} \int dx \, \psi(x,0)\int dp \, \phi^*(p)e^{-ipx/\hbar} \\ \nonumber
&= \int dx \, \psi^*(x,0) \,\psi(x,0)
\end{align}
E' il risultato del \emph{teorema di Parseval}. Se una funzione è normalizzata a 1, allora lo è anche la sua trasformata di Fourier. 
Il risultato può essere interpretato a livello probabilistico: si può passare dallo spazio delle coordinate $ x $ a quello dei momenti $ p $ e i due spazi sono isomorfi. Posso quindi scrivere anche le osservabili fisiche, anzichè nello spazio delle coordinate, nello spazio dei momenti. Ad esempio, calcoliamo $ p $ nello spazio dei momenti:

\begin{align}
<p> &= \int_{-\infty}^{+\infty} dx \,  \psi^* (-i\hbar\pdv{x}{\psi}{}) \\ \nonumber
&= \int_{-\infty}^{+\infty} dx \, \psi^* (-i\hbar) \pdv{x}{}{}\int \frac{1}{\sqrt{2\pi\hbar}} dp \, \phi(p) e^{ipx/\hbar} \\ \nonumber
&=\int_{-\infty}^{+\infty} dp \, \frac{1}{\sqrt{2\pi\hbar}}\phi(p)(-i\hbar)\frac{(ip)}{\hbar}\overbrace{\int_{-\infty}^{+\infty} dx \, \psi^* e^{ipx/\hbar}}^{ = \phi^*(p)} \\ \nonumber
&= \int_{-\infty}^{+\infty}dp \, \phi(p) \, p \, \phi^*(p)
\end{align}

Quindi nello spazio dei momenti $ p $ è un operatore moltiplicativo e non differenziale. Riassumento si può schematizzare il tutto come:

\[
\begin{array}{ccc}
OPERATORE & COORDINATE &  MOMENTI \\
Posizione  & x & i\hbar \pdv{x}{}{} \\
Momento  & -i\hbar\pdv{x}{}{} & p
\end{array}
\]

\subsubsection{Esercizio: problema della commutatività}
Mi chiedo se misurare prima $ x $ e poi $ p $ sia uguale a misurare prima $ p $ e poi $ x $, cioè

\begin{equation}
x\,(p\,\psi) \stackrel{?}{=} p\,(x\,\psi)
\end{equation}

Per rispondere alla domanda si può calcolare il commutatore $$ [x,p_x]\,\waf{} = (x\cdot p_x)\,(\waf{})- (p_x \cdot x) \,(\waf{})$$ e si ottiene:

\begin{equation}
[x,p_x]\,\waf{} = i\hbar\;\waf{} \quad \rightarrow \quad [x,p_x] = i\hbar 
\end{equation}
il risultato è indipendente dalla particolare funzione d'onda.

Quindi i due operatori non commutano. Riassumiamo la nostra conoscenza e gli strumenti che abbiamo a disposizione sugli operatori.

\subsection{Proprietà degli operatori}

Fino ad ora abbiamo appreso le seguenti nozioni:

\begin{itemize}
	\item 
	Possiamo passare dallo spazio delle coordinate allo spazio dei momenti
	
	\begin{equation}
	\psi(x,t) \leftrightarrow \phi(p)
	\end{equation}
	
	\item
	Utiliziamo operatori hermitiani di cui abbiamo definito il valore di aspettazione:
	
	\begin{equation}
	O \qquad <O> = \int dx \, \psi^* O \psi
	\end{equation}
	
	\item
	Due operatori non è detto che commutino. Inoltre possiamo definire la varianza di un operatore come:
	
	\begin{equation}
	\Delta O^2 = <O^2> - <O>^2
	\end{equation}
	
	\item
	L'equazione di Schroedinger è 
	
	\begin{equation}
	i\hbar\pdv{t}{\psi}{} = \left[- \frac{\hbar^2}{2m}\pdv{x}{}{2} + V(x) \right]\psi := H\psi
	\end{equation}
	
	Con $ H $ definito come operatore Hamiltioniano.
	
	\item
	L'equazione di continuità è:
	
	\begin{equation}
	\pdv{t}{\rho}{} = -\frac{\hbar}{2mi} = \vec{\nabla} \cdot \vec{J}
	\end{equation}
\end{itemize}


\subsection{Soluzione dell'equazione di Schr\"odinger in caso stazionario}

Cerchiamo una soluzione per l'equazione di Schr\"odinger nel caso stazionario, cioè nel caso in cui $ V(x,t) = V(x) $ dipenda solo dalla posizione. Riscriviamo la formula \vref{eq:Schoredinger}

\begin{equation}
i\hbar\pdv{t}{\waf{}}{} = \left[- \frac{\hbar^2}{2m} \pdv{x}{}{2} + V(x)\right]\waf{}
\end{equation}

Congetturiamo che la soluzione $ \waf{} $ possa essere scritta come $ \waf{} = T(t)u(x) $ e riscriviamo l'equazione:

\begin{equation}
i\hbar u(x) \pdv{t}{T(t)}{} = \left[- \frac{\hbar^2}{2m} \pdv{x}{}{2} + V(x)\right]u(x)T(t)
\end{equation}

Dividiamo ambo i membri per $ u(x)T(t) $:

\begin{equation}
i\hbar\frac{1}{T}\pdv{t}{T}{} = \frac{1}{u(x)}  \left[- \frac{\hbar^2}{2m} \pdv{x}{}{2} + V(x)\right]u(x)
\end{equation}

Si può osservare che i due membri dell'equazione sono funzioni in due variabili diverse. L'unica soluzione è quella in cui entrambi i membri siano una costante, che chiamiamo $ E $.

\begin{equation} \label{eq:sistema equazione stazionaria di S}
\begin{cases}
\displaystyle
\frac{i\hbar}{T}\pdv{t}{T}{} = E \qquad \Rightarrow \qquad T = ce^{-Et/\hbar} \\ \\
\displaystyle
\frac{1}{u}\left[ \dots \right]u = E
\end{cases}
\end{equation}

La soluzione è quindi:

\begin{equation}
\boxed{
	\waf{} = u(x)T(t) = u(x)ce^{-Et/\hbar}
}
\end{equation}

Notiamo che il termine esponenziale è una fase, $ c $ è una costante, quindi $ u(x) $ deve avere le proprietà di decrescenza rapida per fare in modo che $ |\waf{}|^2 $ sia una p.d.f.
Inoltre, la seconda equazione della \vref{eq:sistema equazione stazionaria di S} è un equazione agli autovalori; infatti:

\begin{gather} \label{eq:equazione schrodinger indipendente dal tempo}
-\frac{\hbar^2}{2m} \pdv{x}{u(x)}{2} + V(x)u(x) = Eu(x) \nonumber \\
H\psi = E\psi
\end{gather}

Quindi il valore dell'energia che abbiamo chiamato $ E $ è un autovalore dell'operatore Hermitiano $ H $. Il perchè questa soluzione venga chiamata stazionaria ha una motivazione anche legata alla fluttuazione dell'energia. Proviamo a calcolarla:

\begin{align}
\Delta E &= \sqrt{<E^2>-<E>^2} \nonumber \\
<E> &= \int\psi^*H\psi dx = E\int\psi^*\psi dx = E \nonumber \\ 
<E^2> &= \int \psi^* HH \psi dx = E^2 \nonumber \\ 
\Delta E = \sqrt{E^2 - E^2} = 0
\end{align}
non c'è fluttuazione, cioè lo stato mantiene sempre la stessa energia.


\begin{itemize}

\item
Perchè chiamo l'operatore energia l'hamiltoniana?
So che
$$
p = -i\hbar\frac{d}{dx}
$$ 

Provare a calcolare $\psi^*ip$. Nella meccanica
\begin{align}
E = K + V = p^2/2m+ V \\  
\end{align}

\end{itemize}

\end{document}