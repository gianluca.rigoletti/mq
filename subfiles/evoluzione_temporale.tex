\documentclass[../mq.tex]{subfiles}
\begin{document}

\newpage
\section{Evoluzione temporale del sistema}

\subsection{Teorema di Ehrenfest}

Interroghiamoci sulle caratteristiche di un generico operatore $ A $ ed in particolare, sull'evoluzione temporale del suo valore d'aspettazione $ \ev{A} $. Per farlo, ricordiamo l'equazione di Schr\"odinger.

\begin{equation}
\ev{A} = \intf \psi^* A\, \psi \; dx
\end{equation}
\begin{align}
\frac{d}{dt}\ev{A} &= \int \psi^* \pdv{t}{A}{}\psi \; dx + \int \pdv{t}{\psi^*}{} A \psi\; dx + \int \psi^* A \pdv{t}{\psi}{}\;dx \nonumber \\
&= \ev{\pdv{t}{A}{}} + \int \left(- \frac{1}{i\hbar} H\psi \right)^* A \psi + \int \psi^* A \left( \frac{1}{i\hbar} H\psi \right) \nonumber \\
&= \ev{\pdv{t}{A}{}} + \frac{i}{\hbar}\int \psi^* HA\psi\;dx - \frac{i}{\hbar}\int \psi^*AH\psi\;dx \nonumber \\
\end{align}
Abbiamo ottenuto quindi il risultato, noto anche come \emph{Teorema di Ehrenfest}:
\begin{equation}
\boxed{\frac{d}{dt}\ev{A} = \ev{\pdv{t}{A}{}} + \frac{i}{\hbar}\ev{[H,A]}}
\end{equation}
Osserivamo che se $ A $ non dipende esplcitamente dal tempo e commuta con $ H $ allora:

\begin{equation}
\frac{d}{dt}\ev{A} = 0
\end{equation}
Cioè $ A $ è una costante del moto.
Studiamo in particolare l'evoluzione temporale dei valori d'aspettazione degli operatori $ x $ e $ p $.
In particolare, $ x $ e $ p $ non dipendono esplicitamente dal tempo. Utilizziamo il teorema appena trovato per i calcoli:

\begin{align}
\frac{d}{dt}\ev{x} = \frac{i}{\hbar} \ev{[H,x]} = \frac{i}{\hbar}\ev{\left[\frac{p^2}{2m} + V(x),x\right]}
\end{align}
Ricordiamo che $ x $ commuta con qualsiasi funzione dipendente da $ x $ stesso, quindi

\begin{equation}
[V(x),x] = 0
\end{equation}
Mentre per il momento, otteniamo:

\begin{align}
[p^2,x] = [pp,x] = p[p,x] + [p,x]p = \frac{\hbar}{i}p + \frac{\hbar}{i}p = \frac{2\hbar}{i}p
\end{align}
Quindi in totale otteniamo

\begin{equation}\label{eq:di Ehrenfest per le coordinate}
\boxed{
	\frac{d}{dt}\ev{x} = \frac{i}{\hbar}\ev{\left(\frac{2\hbar}{2mi}\right) p} = \ev{\frac{p}{m}}
}
\end{equation}
Per quanto riguarda l'operatore momento, invece:

\begin{align}
\frac{d}{dt}\ev{p} = \frac{i}{\hbar}\ev{\;\left[ \left(\frac{p^2}{2m} + V(x)\right),p \right]\;}
\end{align}
Anche qua l'operatore $ p $ commuta con $ p^2 $. quindi ci rimane da studiare solo:
\begin{align}
&- \frac{i}{\hbar} \ev{\;[p,V(x)]\;} \nonumber \\
\Rightarrow [p,V(x)]\psi &= pV(x)\psi - V(x)p\psi = \nonumber \\
&=-i\hbar\pdv{x}{(V(x)\psi)}{} + i\hbar V(x)\pdv{x}{\psi} \nonumber \\
&= -i\hbar \pdv{x}{V(x)}{}\psi
\end{align}
Riassumendo:
\begin{align}\label{eq:di Ehrenfesst per il momento}
\boxed{\frac{d}{dt}\ev{p} =-\frac{i}{\hbar}\ev{-i\hbar\pdv{x}{V(x)}{}} = -\ev{\pdv{x}{V(x)}{}}}
\end{align}
Possiamo combinare la \vref{eq:di Ehrenfest per le coordinate} e \vref{eq:di Ehrenfesst per il momento} e otteniamo:
\begin{equation}
\frac{d}{dt}\ev{p} = m\frac{d}{dt}\frac{d}{dt}\ev{x} = m\frac{d^2}{dt^2}\ev{x} = -\ev{\pdv{x}{V(x)}{}}
\end{equation}
\paragraph{Considerazioni}
Come abbiamo potuto osservare, sembrerebbe di aver trovato le equazioni del moto della macchina classica di un punto in una regione in cui è presente un potenziale $ V(x) $. Tuttavia, è necessario porre attenzione a tale questione. L'equazione classica è 
\begin{equation}
m\frac{d^2x_{cl}}{dt^2} =-\frac{dV(x_{cl})}{dx_{cl}}
\end{equation}
Mentre quella da noi trovata:
\begin{equation}
m\frac{d^2\ev{x}}{dt^2} = -\ev{\frac{dV(x)}{dx}}
\end{equation}
Quindi per essere uguali
\begin{equation}
x_{cl} = \ev{x}
\end{equation}
Ma a priori che
\begin{equation}\label{eq:limite classico potenziale Ehrenfest}
\ev{\frac{dV(x)}{dx}} \neq \frac{dV(\ev{x})}{d\ev{x}}
\end{equation}
Questo ci dice che il caso classico lo otteniamo solo quando vale l'uguaglianza nella formula sopra scritta.
Qual è la condizione per ottenere il limite classico? Come notò Ehrenfest, la condizione è che il potenziale dev'essere una funzione che varia in modo "piccolo" rispetto all'incertezza della misura $ \Delta x $. Se scriviamo:
\begin{equation}
F(x) = -\frac{dV(x)}{dx}
\end{equation}
ed espandiamo in serie di Taylor:
\begin{equation}
F(x) = F(\ev{x}) + (x-\ev{x})F'(\ev{x}) + \frac{\overbrace{(x-\ev{x})^2}^{\Delta x^2}}{2!}F"(\ev{x}) + \dots
\end{equation}
Se quindi $ \Delta x^2 $ è piccola, può essere trascurata rispetto ai primi due termini e quindi il valor medio di $ F(x) $ diventa:
\begin{align}
\ev{F(x)} &= \ev{F(\ev{x}) + (x-\ev{x})F'(\ev{x})} = F(\ev{x}) + \ev{x-\ev{x}}F'(\ev{x}) \nonumber \\
&= F(\ev{x}) + (\ev{x}-\ev{x})F'(\ev{x}) \nonumber \\
&= F(\ev{x})
\end{align}
E vale quindi l'uguaglianza in \vref{eq:limite classico potenziale Ehrenfest}. Questa approssimazione la possiamo compiere ad esempio per gli elettroni che si muovono in potenziale elettrico di dimensioni macroscopiche (si pensi ad un condensatore o più in generale ai problemi di Fisica II). Questo ci permette di descrivere l'orbita e il moto utilizzando le equazioni classiche della meccanica e dell'elettromagnetismo.

\subsection{Evoluzione temporale - visione di Heisenberg}
Evoluzione temporale di un sistema fisico. Scritto in notazione di Dirac

\begin{equation}
i\hbar \pdv{t}{\ket{\psi}}{} = H\ket{\psi}
\end{equation}
So scrivere la soluzione:
\begin{equation}
\ket{\psi} = \underbrace{e^{-iHt/\hbar}}_{=U}\ket{\psi_0}
\end{equation}
Esiste questo vettore $ U $ che fa variare la funzione nel tempo.
Calcoliamo il valor d'aspettazione di un operatore $ B $ nel tempo con Dirac
\begin{equation}\label{eq:valor d'aspettazion con Dirac}
<B>_t = \bra{\psi}B\ket{\psi} = \bra{e^{-iHt/\hbar}\psi}B\ket{e^{-iHt/\hbar}\psi_0}
\end{equation}
Chiamiamo quindi con U:
\begin{equation}
U = e^{-iHt/\hbar}
\end{equation}
Si verifica facilmente che
\begin{equation}
UU^\dagger = 1
\end{equation}
Finora siamo stati nella rappresentazione si Schr\"odinger, dove si lavora con gli stati e gli operatori non dipendono dal tempo.
Riscriviamo \vref{eq:valor d'aspettazion con Dirac} come
\begin{equation}
\bra{\psi_0}e^{iHt/\hbar}Be^{-iHt/\hbar}\ket{\psi_0}
\end{equation}
Introduciamo un'altra rappresentazione, che è quella di Heisenberg, in cui la dipendenza temporale è tutta negli operatori.
Chiamo con $ B_H $:
\begin{equation}
B_H := e^{iHt/\hbar}Be^{-iHt/\hbar}
\end{equation}
Per passare da Scrhodinger ad Heisenberg utilizzo una trasformazione unitaria quindi:
\begin{equation}
U^\dagger O_S U  =O_H
\end{equation}
In questa nuova rappresentazione gli stati non dipendono dal tempo (vengono considerati gli stati iniziali $ \psi_0 $)
Cosa succede al valore d'aspettazione passando da una rappresentazione all'altra? Ci aspettiamo che non cambi:
\begin{equation}
\bra{\psi_H}O_H\ket{\psi_H} = \bra{\psi_H}U^\dagger O_S U \ket{\psi_H} = \bra{\psi_S}UU^\dagger O_S U U^\dagger\ket{\psi_S} = \bra{\psi_S}O_S\ket{\psi_S}
\end{equation}
Scriviamo le equazioni che gli operatori soddisfano (non scriviamo più il pedice $ H $ per comodità):
\begin{equation}
\frac{d B(t)}{dt} \qquad B(t) =  e^{iHt/\hbar}B_S e^{-iHt/\hbar}
\end{equation}
Stiamo calcolando l'evoluzione temporale della dipendenza dell'operatore dal tempo
\begin{align}
\frac{d B(t)}{dt} &= \frac{i}{\hbar}He^{iHt}B_Se^{-iHt/\hbar}-\frac{i}{\hbar}e^{iHt}B_S H e^{-iHt/\hbar} \nonumber \\
				  &= \frac{i}{\hbar}\left[ HB(t),B(t)H \right]
\end{align}
La rappresentazione temporale è data quindi dal commutatore dell'operatore con l'Hamiltoniana. Ricordano un po' il teorema di Ehrenfest ma hanno un significato profondamente diverso.
Si scrive in modo più compatto: 
\begin{equation}
\frac{dB(t)}{dt}= \frac{i}{\hbar}[H,B]
\end{equation}
Note come \emph{Equazioni di heisenberg}

\paragraph{Oscillatore Armonico}
\begin{equation}
H = \hbar \omega (a^*(ta(t + \frac{1}{2})))
\end{equation}
\begin{equation}
\begin{cases}
\frac{d}{dt}a(t) = \frac{i}{\hbar}[H,a]=-i\omega a(t) \\
\frac{d}{dt}a^+(t) = -\frac{i}{\hbar}[H,a^+] = i\omega^+(t)
\end{cases}
\end{equation}
\begin{align}
\begin{cases}
a(t)   &= e^{-i\omega t}a(0) \\
a^+(t) &= e^{i\omega t}a^+(0)
\end{cases}
\end{align}
Per ottenere $ x(t) $ e $ p(t) $ mi basta ricordare la definizione di $ a,a^+ $ e risolvere il sistema:
\begin{align}
\begin{cases}
x(t) &= x(0)\cos(\omega t)  + \frac{i p(0)}{\sqrt{2m\omega\hbar}} \\
p(t) &= p(0)\cos(\omega t) + m\omega x(0)\sin(\omega t)
\end{cases}
\end{align}

\paragraph{Problema capitolo 6 con rappresentazione di Heisenberg}
Data:
\begin{equation}
Tr A = \sum \bra{n}A\ket{n}
\end{equation}
Dimostrare:
\begin{equation}
Tr AB = Tr BA
\end{equation}
Dimostrazione:
\begin{align}
Tr AB =\sum \bra{n}AB\ket{n} = \sum_{n,k} \bra{n}A\ket{k}\bra{k}B\ket{n} = \sum \bra{k}B\ket{n}\bra{n}A\ket{k} = Tr BA
\end{align}
Fine.

\paragraph{Problema 14}
Oscillatore armonico di carica $ e $ in presenza di campo elettrico.
\begin{equation}
V_E = -e\gamma - x
\end{equation}
\begin{equation}
H = \frac{p(t)^2}{2m} + \frac{1}{2}m\omega^2x(t) -e\gamma x(t)
\end{equation}
Prendo l'equazione di Heisenberg e scrivo la derivata temporale di $ x $ e $ t $.
\begin{align}
\frac{dx}{dt} = \frac{i}{\hbar}[H,x] = \frac{i}{\hbar} \left[ \frac{p^2}{2m}, x \right] = \left( \frac{-2i\hbar p}{sm} \right)\frac{i}{\hbar} = \frac{p}{m} \nonumber \\
\frac{dp}{dt} = \frac{i}{\hbar}[H,p] = \frac{i}{\hbar}\left[ \frac{1}{2}m\omega^2 x^2  - e\gamma x, p \right]  \nonumber \\
= \frac{i}{\hbar} = \left[\frac{1}{2} m\omega^2(2i\hbar x)-e\gamma i \hbar\right] = -m\omega^2 x +e\gamma \nonumber \\
\frac{d^2x}{dt^2} = -\omega^2 x + \frac{e\gamma}{m} \nonumber \\
x(t)  - \frac{e\gamma}{m}  = x(0)\cos(\omega t) + \frac{p(0)}{m\omega}\sin(\omega t) \nonumber \\
p(t) = p(0)\cos(\omega t) - m\omega x(0)\sin(\omega t)
\end{align}
Calcolare il commutatore $ [x(t_1), x(t_2)] $:
\begin{align}
&= \left[ x(0)\cos(\omega t_1) + \frac{p(0)}{m\omega}\sin(\omega t_1),  \frac{e\gamma}{m}  = x(0)\cos(\omega t_2) + \frac{p(0)}{m\omega}\sin(\omega t_2) \right] \nonumber \\
&=\frac{1}{m\omega} \left[ x(0),p(0) \right]\cos(\omega t_1)\sin(\omega t_2) + [p(0),x(0)] \frac{1}{m\omega}\sin(\omega t_1)\cos(\omega t_2) \nonumber \\
&= \frac{i\hbar}{m\omega} \cos(\omega t_1)\sin(\omega t_2) - \frac{i\hbar}{m\omega}\sin(\omega t_1)\cos(\omega t_2) \nonumber \\
&= \frac{i\hbar}{m\omega} \sin(\omega t_1 - \omega t_1)
\end{align}

\end{document}