\documentclass[../mq.tex]{subfiles}
\begin{document}


\newpage
\section{Atomo di idrogeno}
Il modello più semplice di atomo è quello dell'idrogeno, costituito da un protone e da un elettrone che interagiscono con un campo elettrico, caratterizzato da un potenziale centrale, cioè dipendente solamente dalla distanza $|\vec{r}|$.\\ Riprendiamo il risultato ricavato nella sezione \vref{sec:potenziale_centrale} per l'equazione di Shrodinger in potenziale centrale (eq. \vref{eq:Schrodinger con momento angolare e potenziale centrale}).

\begin{equation}\label{eq:hamiltoniana_H_coordrad}
-\frac{\hbar^2}{2m}\left(\pdv{x}{}{2} + \frac{2}{r}\pdv{r}{}{}\right)\psi(\textbf{r}) + \frac{L^2}{2mr^2}\psi(\textbf{r}) + V(r)\psi(\textbf{r}) = E\psi(\textbf{r})
\end{equation}

Vediamo che compare l'operatore momento angolare, è quindi utile fattorizzare la funzione d'onda in componente radiale e angolare, facendo uso degli autovalori del momento angolare (vedi eq. \vref{eq:autovalori_momenti_angolare}).
\begin{equation}
\psi(\textbf{r}) = R(r)\,Y_{l,m}(\theta,\psi)
\end{equation}
Inserendo questa particolare forma della funzione d'onda nell'equazione di Shrodinger si ottiene:
\begin{align}
-\frac{\hbar^2}{2m}\left(\pdv{x}{}{2} + \frac{2}{r}\pdv{r}{}{}\right)R(r)\,Y_{l,m} + \frac{R(r)}{2mr^2}L^2\,Y_{l,m} + V(r)R(r)\,Y_{l,m} &= E R(r)\,Y_{l,m} \nonumber\\
-\frac{\hbar^2}{2m}\left(\pdv{x}{}{2} + \frac{2}{r}\pdv{r}{}{}\right)R(r) + \frac{R(r)}{2mr^2}\hbar^2\,l(l+1) + V(r)R(r) &= E R(r)\nonumber 
\end{align} 
Si ottiene quindi un'equazione agli autovalori che contiene solo la componente radiale della funzione d'onda, in cui compare l'autovalore $l$ del momento angolare.
\begin{equation}
-\frac{\hbar^2}{2m}\left(\cdv{r}{}{2} + \frac{2}{r}\cdv{r}{}{} - \frac{l(l+1)}{r^2}\right)\,R(r) + V(r)R(r) = E\,R(r)
\end{equation}
La soluzione di questa equazione differenziale darà lo spettro dell'energia dell'atomo e le autofunzioni dell'hamiltoniana. Vedremo in seguito la relazione con le autofunzioni del momento angolare.

Prima di risolvere questa equazione facciamo un'osservazione importante sull'approssimazione che stiamo utilizzando. 
\subsection{Problema a due corpi}
In realtà l'atomo di idrogeno sarebbe un problema a due corpi e andrebbe analizzato più nello specifico, considerando le posizioni sia del protone che dell'elettrone.
\begin{equation}
-\frac{\hbar^2}{2m_1} \nabla_1^2\psi(\vec{r_1},\vec{r_2}) -\frac{\hbar^2}{2m_2} \nabla_2^2\psi(\vec{r_1},\vec{r_2}) - \frac{e^2}{|\vec{r_1} -\vec{r_2}|}\psi(\vec{r_1},\vec{r_2})= E\,\psi(\vec{r_1},\vec{r_2})
\end{equation}
Introducendo il centro di massa si può dividere come al solito il problema in moto del centro di massa e moto della massa ridotta attorno al centro di massa.
\begin{equation}
\begin{cases}\displaystyle
\vec{R} = \dfrac{m_1\vec{r_1}+m_2\vec{r_2}}{m_1+m_2}\\
\displaystyle\vec{r} = \vec{r_1}- \vec{r_2}
\end{cases}
\end{equation}
Inserendo queste quantità nell'equazione di Shrodinger, si divide in due termini
\begin{equation}
\begin{cases}
\displaystyle-\frac{\hbar^2}{2(m_1+m_2)} \nabla^2\Psi(\vec{R}) = E_{cm}\Psi(\vec{R})\\ \\
\displaystyle -\frac{\hbar^2}{2}\left(\frac{1}{m_1}-\frac{1}{m_2}\right) \nabla^2 \Upsilon(\vec{R}) -\frac{e^2}{4\pi\epsilon_0 r}\Upsilon(\vec{r}) = E \Upsilon(\vec{R})
\end{cases}
\end{equation}
Il primo termine rappresenta l'equazione di Shrodinger per una particella libera, il centro di massa. Il secondo invece rappresenta una particella di \emph{massa ridotta} \[\mu = \frac{m_1m_2}{m_1+m_2}\] in un campo centrale. 
Nel caso dell'atomo di idrogeno, siccome $m_p \sim 2000\,m_e$ la massa ridotta è circa uguale a quella dell'idrogeno. Possiamo quindi considerare il protone fermo e coincidente con il centro di massa e studiare la funzione d'onda dell'elettrone con un'approssimazione minima.

\subsection{Ricerca di autofunzioni e autovalori radiali}
Riscriviamo l'equazione agli autovalori per la parte radiale della funzione d'onda.
\begin{equation}
\left[ \cdv{r}{}{2} + \frac{2}{r}\cdv{r}{}{} + \frac{2\mu}{\hbar^2}\left(E+ \frac{Ze^2}{4\pi\epsilon_0 r}- \frac{\hbar^2 l(l+1)}{2\mu r^2}\right)\right]R(r) = 0
\end{equation}

Eseguiremo quindi un cambio di variabili e cercheremo una soluzione all'equazione differeziale studiando il comportamento della funzione a $0$ e a $+\infty$ e poi con il metodo dello sviluppo in serie.

\subsubsection{Cambio di variabili}
Consideriamo gli \emph{stati legati}, cioè quelli con energia $E<0$ ed eseguiamo un cambio di variabili comodo per il calcolo:
\begin{equation}
\rho = \sqrt{\frac{8\mu|E|}{\hbar^2}}r
\end{equation}
\begin{align}
\cdv{r}{}{} = \cdv{r}{\rho}{} \cdv{\rho}{}{} \qquad \cdv{r}{}{2} = \left(\cdv{r}{\rho}{}\right)^2 \cdv{\rho}{}{2} \nonumber \\
\cdv{r}{}{} = \sqrt{\frac{8\mu|E|}{\hbar^2}} \qquad \cdv{r}{}{2} = \frac{8\mu|E|}{\hbar^2} \cdv{\rho}{}{2}
\end{align}

\begin{align}
\left[\frac{8\mu|E|}{\hbar^2}\cdv{\rho}{}{2} + \frac{2}{\rho}\frac{8\mu|E|}{\hbar^2}\cdv{\rho}{}{} + \frac{2\mu}{\hbar^2}E + \frac{Ze^2\mu}{2\hbar^2 \pi\epsilon_0 \rho }\sqrt{\frac{8\mu|E|}{\hbar^2}} - \frac{l(l+1)}{\rho^2}\frac{8\mu|E|}{\hbar^2} \right]R(\rho)  &= 0 \nonumber \\
\frac{8\mu|E|}{\hbar^2}\left[ \cdv{\rho}{}{2}  +\frac{2}{\rho}\cdv{\rho}{}{} + \frac{2\mu}{\hbar^2}E\frac{\hbar^2}{8\mu|E|} +\frac{Ze^2\mu}{2\hbar^2 \pi\epsilon_0 \rho }\sqrt{\frac{8\mu|E|}{\hbar^2}}\frac{\hbar^2}{8\mu|E|}\right]R(\rho)&=0 \nonumber \\
\left[ \cdv{\rho}{}{2}  +\frac{2}{\rho}\cdv{\rho}{}{}  -\frac{1}{4} +\frac{Ze^2\mu}{4\hbar \pi\epsilon_0 \rho }\sqrt{\frac{\mu}{2|E|}}\frac{\hbar^2}{8\mu|E|}\right]R(\rho)&=0
\end{align}
Scegliendo un nuovo cambio di variabili 
\begin{equation}\label{eq:lambda}
\lambda = \frac{Ze^2}{4\pi\epsilon_0\hbar}\sqrt{\frac{\mu}{2|E|}} = Z\alpha \sqrt{\frac{\mu c^2}{2|E|}} \quad \mbox{ con } \;\alpha = \frac{e^2}{4\pi\epsilon_0 \hbar c}
\end{equation}
$\alpha$ è detta costante di struttura fine. Si ottiene infine:
\begin{equation}\label{eq:shr_radiale}
\boxed{\cdv{\rho}{R(\rho)}{2} +\frac{2}{\rho}\cdv{\rho}{R(\rho)}{} - \frac{l(l+1)}{\rho^2}\,R(\rho) + \left(\frac{\lambda}{\rho}-\frac{1}{4}\right)\,R(\rho)= 0}
\end{equation}

\subsubsection{Comportamento agli estremi}
Controlliamo il comportamento dell'equazione differenziale per $\rho$ grandi.
\begin{equation}
\cdv{\rho}{R(\rho)}{2} -\frac{1}{4}R(\rho)=0
\end{equation}
Utilizzando il polinomio risolvente $\xi^2 -1/4 = 0$ otterremmo due soluzioni $e^{\pm \rho/2}$. Scegliamo la soluzione che non diverge a $+\infty$, quindi il comportamento all'infinito è:
\begin{equation}
R(\rho) \approx e^{-\rho/2}
\end{equation}
Estratto il comportamento a $+\infty$ riscriviamo la funzione $R(\rho)$ con una nuova funzione $G(\rho)$ che analizzeremo nel resto del dominio: 
\begin{equation}
R(\rho)= e^{-\rho/2}\,G(\rho)
\end{equation}
Inseriamo la nuova espressione di $R(\rho)$ in eq. \vref{eq:shr_radiale} per ottenere una nuova equazione differenziale per $G(\rho)$. Sviluppiamo alcuni termini:
\begin{align}
\cdv{\rho}{}{2}(e^{-\rho/2}G(\rho)) &= \cdv{\rho}{}{}\left(\cdv{\rho}{}{}e^{-\rho/2}G(\rho)\right)= \cdv{\rho}{}{}\left(-\frac{1}{2}e^{-\rho/2}G(\rho) + e^{-\rho/2}\cdv{\rho}{G(\rho)}{}\right) \nonumber \\\nonumber
&= +\frac{1}{4}e^{-\rho/2}G(\rho)-\frac{1}{2}e^{-\rho/2}\cdv{\rho}{G(\rho)}{} -\frac{1}{2}e^{-\rho/2}\cdv{\rho}{G(\rho)}{} + e^{-\rho/2}\cdv{\rho}{G(\rho)}{2} \\
&= e^{-\rho/2}\left(-\frac{1}{4} G(\rho) -\cdv{\rho}{G(\rho)}{} + \cdv{\rho}{G(\rho)}{2}\right)
\end{align}
\begin{align}
\frac{2}{\rho}\cdv{\rho}{}{}(e^{-\rho/2}G(\rho)) &= \frac{2}{\rho} \left(-\frac{1}{2}e^{-\rho/2}G(\rho) + e^{-\rho/2}\cdv{\rho}{G(\rho)}{}\right) \nonumber\\
&= \frac{e^{-\rho/2}}{\rho}\left(-G(\rho)+2 \cdv{\rho}{G(\rho)}{}\right)
\end{align}
Esplicitiamo eq. \vref{eq:shr_radiale} inserendo i vari termini e $G(\rho)$ esplicitamente:
\begin{align}
e^{-\rho/2}\left(\frac{1}{4} G(\rho) -\cdv{\rho}{G(\rho)}{} + \cdv{\rho}{G(\rho)}{2} -\frac{G(\rho)}{\rho} +\frac{2}{\rho}\cdv{\rho}{G(\rho)}{}\right) +\nonumber\\
+ e^{-\rho/2} \left(-\frac{l(l+1)}{\rho^2}G(\rho) + \left(\frac{\lambda}{\rho} - \frac{1}{4}\right)G(\rho) \right)= 0 
\end{align}
Il fattore $e^{-\rho/2}$ si elimina e rimane un'equazione differenziale per $G(\rho)$. Abbiamo quindi estratto il comportamento a $+\infty$ senza introdurre approssimazioni.
\begin{equation}\label{eq:eq_diff_g}
\boxed{\cdv{\rho}{G(\rho)}{2} - \left(1- \frac{2}{\rho}\right)\cdv{\rho}{G(\rho)}{} + \left[\frac{\lambda-1}{\rho}- \frac{l(l+1)}{\rho^2}\right]G(\rho)=0}
\end{equation}
Ora analizziamo questa equazione differenziale per $\rho \rightarrow 0$.
\begin{equation}
\cdv{\rho}{G(\rho)}{2} + \frac{2}{\rho}\cdv{\rho}{G(\rho)}{} - \frac{l(l+1)}{\rho^2} G(\rho)= 0
\end{equation}
La soluzione per $\rho\rightarrow 0$ è: 
\begin{equation}
G(\rho) \approx \rho^l
\end{equation}
Infatti se sostituiamo la soluzione nell'equazione differenziale:
\begin{align}
l(l-1)\rho^{l-2} + 2l\rho^{(l-1)-1} - (l^2 +l)\rho^{l-2} = 0 \\\nonumber 
l^2 -l +2l -l^2 +l = 0
\end{align}
Quindi scriviamo $G(\rho)$ esplicitando il comportamento a $0$:
\begin{equation}
G(\rho) = \rho^l \,H(\rho)
\end{equation}
Seguendo il ragionamento precedente ora inseriamo la nuova espressione di $G(\rho)$ nell'equazione \vref{eq:eq_diff_g} per studiare il comportamento della funzione nei valori intermedi di $\rho$. Sviluppiamo separatamente alcuni membri:
\begin{align}
\cdv{\rho}{}{2}(\rho^l H(\rho))= \cdv{\rho}{}{}\left( (l\rho^{l-1})H(\rho) + \rho^l \cdv{\rho}{H(\rho)}{}\right) = \nonumber\\
=l(l-1)\rho^{l-2}H(l)+ (l\rho^{l-1})\cdv{\rho}{H(\rho)}{}+ (l\rho^{l-1})\cdv{\rho}{H(\rho)}{} + \rho^l \cdv{\rho}{H(\rho)}{2} = \nonumber\\
=l(l-1)\rho^{l-2}H(l)+ 2l\rho^{l-1}\cdv{\rho}{H(\rho)}{} +\rho^l \cdv{\rho}{H(\rho)}{2}
\end{align}
\begin{align}
\left(1- \frac{2}{\rho}\right)\cdv{\rho}{H(\rho)}{} = \left(1-\frac{2}{\rho}\right)\left(l\rho^{l-1}H(\rho)+ \rho^l\cdv{\rho}{H(\rho)}{} \right)
\end{align}
Sommando i vari termini in \vref{eq:eq_diff_g} :
\begin{align}
l(l-1)\rho^{l-2}H(l)+ 2l\rho^{l-1}\cdv{\rho}{H(\rho)}{} +\rho^l \cdv{\rho}{H(\rho)}{2} +\nonumber \\-\left(1-\frac{2}{\rho}\right)\left(l\rho^{l-1}H(\rho)+ \rho^l\cdv{\rho}{H(\rho)}{} \right) +  \left[\frac{\lambda-1}{\rho}- \frac{l(l+1)}{\rho^2}\right]\rho^l H(\rho)=0 \nonumber\\\nonumber \\
\cdv{\rho}{H(\rho)}{2} +  \left(\frac{2(l+1)}{\rho}-1\right)\cdv{\rho}{H(\rho)}{} + \\+\left[ l(l-1)\rho^{-2} - l\rho^{-1}+ 2l\rho^{-2}  +(\lambda-1)\rho^{-1}- l(l+1)\rho^2  \right]H(\rho) = 0 \nonumber 
\end{align}
Semplificando tutti i termini si ottiene un'equazione differenziale per $H(\rho)$:
\begin{equation}\label{eq:diff_H_rho}
\boxed{\cdv{\rho}{H(\rho)}{2} +  \left(\frac{2l+2}{\rho}-1\right)\cdv{\rho}{H(\rho)}{} + \frac{\lambda-1-l}{\rho}H(\rho) = 0}
\end{equation}
Abbiamo quindi analizzato il comportamento di $R(\rho)$ nei limiti di $\rho \rightarrow +\infty$ e $\rho \rightarrow 0$, ottenendo un'espressione meno generica di $R(\rho)$:
\begin{equation}\label{eq:generale_R}
R(\rho) = e^ {-\rho/2}\,\rho^l \,H(\rho)
\end{equation}

\subsubsection{Sviluppo in serie}
Non ci resta che cercare il comportamento generale per $R(\rho)$ non ai limiti del dominio ma per valori intermedi.

Sviluppiamo in serie $H(\rho)$:
\begin{equation}\label{eq:serie_H}
H(\rho) = \sum_{k=0}^{+\infty} a_k\rho^k
\end{equation}
Inseriamo questa espressione in \vref{eq:diff_H_rho} per ottenere una condizione sui coefficienti dello sviluppo $a_k$. 
\begin{equation}
\sum_{k=0}^{+\infty}a_k\left[k(k-1)\rho^{k-2} + k\left(\frac{2l+2}{\rho}-1 \right)\rho^{k-1} + (\lambda -l - 1)\rho^{k-1}  \right]=0
\end{equation}
Il primo termine dipende da $\rho^{k-2}$ ma possiamo cambiare variabili $k\rightarrow k+1$ e ottenere di nuovo un termine $\rho^{k-1}$. La somma della serie non cambia perchè per $k=0$ il primo termine è annullato comunque dal suo coefficiente.
\begin{align}
&\sum_{k=0}^{+\infty}a_k\left[k(k-1)\rho^{k-2} + k(2l+2)\rho^{k-2} - k\rho^{k-1} + (\lambda-1-l)\rho^{k-1} \right] = \nonumber\\\nonumber
=&\sum_{k=0}^{+\infty} a_k k(k-1+2l+2)\rho^{k-2}+ \sum_{k=0}^{+\infty}a_k(\lambda-1-l-k)\rho^{k-1} = \\\nonumber
(k\rightarrow k+1) \Rightarrow =&\sum_{k=0}^{+\infty} a_{k+1}(k+1)(k+2l+2)\rho^{k-1}+ \sum_{k=0}^{+\infty}a_k(\lambda-1-l-k)\rho^{k-1} = \\
=&\sum_{k=0}^{+\infty} \rho^{k-1}\left[(k+1)(k+2l+2)a_{k+1} + (\lambda -l -1 -k)a_{k}\right]=0
\end{align}
Utilizzando il principio di equivalenza dei polinomi, tutti i coefficienti del termine di sinistra devono essere uguali a 0.
\begin{equation}
(k+1)(k+2l+2)a_{k+1} + (\lambda -l -1 -k)a_{k}=0
\end{equation}
Questo porta a una relazione ricorsiva per i coefficiente $a_k$.
\begin{equation}\label{eq:condiz_ricor_k}
\frac{a_{k+1}}{a_k} = \frac{k+l+1-\lambda}{(k+2l+2)(k+1)}
\end{equation}
Analizziamo meglio questa relazione. Per $k$ molto grandi si ha che \[\frac{a_{k+1}}{a_k}\rightarrow \frac{1}{k}\]
Al limite per $k$ più grandi di un certo numero intero $N$, si ha che \[a_{k+1} \approx \frac{1}{k!}\]
Notiamo che questo è il termine generale dello sviluppo di Taylor della funzione esponenziale. La serie \vref{eq:serie_H} in generale avrebbe quindi la forma di un polinomio (per i termini più piccoli) e di un esponenziale che rappresenta il comportamento per $k$ grande. 
\[H(\rho)= \sum_{k=0}^{N}a_k\rho^k \; + \; e^\rho\]
Ma se così fosse allora l'equazione \vref{eq:generale_R} darebbe \[R(\rho)= e^{\rho/2}\] un risultato non accettabile perchè non convergente all'infinito.

L'unica soluzione è quindi richiedere che la serie \vref{eq:serie_H} termini per un certo intero $k=n_r$
\begin{equation}\label{eq:n_quantici}
k+l+1-\lambda = 0 \quad \rightarrow \quad \lambda = n_r + l+ 1
\end{equation}
$\lambda$ sarà chiamato $n$ o \emph{numero quantico principale}.

\subsubsection{Soluzione generale}
Fissato un $n$ e un $l$ abbiamo il valore $n_r$ cioè l'indice a cui terminare la serie 	\ref{eq:serie_H}. Indichiamo con $H_{nl}$ questa funzione. I termini della serie si calcolano a partire da $a_0=1$, scelto per convenzione, grazie alla regola di ricorsione \ref{eq:condiz_ricor_k} che si può riscrivere come:
\begin{equation}
\frac{a_{k+1}}{a_k} = \frac{k-n_r}{(k+2l+2)(k+1)}
\end{equation}
E' necessario tornare da $\rho$ e $r$:
\begin{align}
\rho = \sqrt{\frac{8\mu|E|}{\hbar^2}}r &\qquad |E| = \frac{Z^2\mu c^2\alpha^2 }{2 n^2}\nonumber \\
\rho = 	\left(\frac{2Zc\mu\alpha}{\hbar}\right)\frac{r}{n} & \;\rightarrow \; \rho = \frac{2Z}{a_0}\frac{r}{n} \qquad a_0 = \hbar / c\mu\alpha
\end{align} 
Quindi la funzione radiale $R(r)$ sarà:
\begin{equation}
R(r) = H(r)\,\left(\frac{2Zr}{a_0 n}\right)^l \,e^{-Zr/a_0n}
\end{equation}
La forma generale di $H(\rho)$ è nota: i \emph{polinomi di Laguerre}:
\begin{equation}
H(\rho)= L^{(2l+1)}_{n-l-1} = L^{(2l+1)}_{n_r}
\end{equation}
\begin{equation}
L_n^\alpha(\rho) = \sum_{m=0}^{n}\left(
\begin{array}{c}
 n+\alpha \\n-m
\end{array}\right) \frac{(-\rho)^m}{m!}
\end{equation}
\begin{equation}
H(r) = \sum_{m= 0}^{n_r} \left(\begin{array}{c}
n_r +2l +1 \\ n_r -m
\end{array} \right)\left(\frac{2Z}{a_0n}\right)^m\frac{(-r)^m}{m!}
\end{equation}
Trovata $H_{nl}(r)$ e calcolata la normalizzazione, si risale a
\begin{equation}\label{eq:soluzione_generale_H}
\boxed{\psi_{nlm}(\textbf{r})=A \left[ \sum_{m= 0}^{n_r} \left(\begin{array}{c}
n_r +2l +1 \\ n_r -m
\end{array} \right)\left(\frac{2Z}{a_0n}\right)^m\frac{(-r)^m}{m!}\right]\,\left(\frac{2Zr}{a_0 n}\right)^l \,e^{-Zr/a_0n}\, Y_{lm}(\theta,\varphi)}
\end{equation}
Con $A$, normalizzazione, uguale a:
\begin{equation}\label{eq:normalizzazioneR}
A = \sqrt{\left(\frac{Z}{a_0 n}\right)^3 \frac{(n-l-1)}{2n[(n+l)!]^3}}
\end{equation}

\subsubsection{Densità di probabilità radiale}
Per analizzare la probabilità di trovare l'elettrone in una \emph{shell} di raggio compreso tra $(r,r+dr)$ in generale bisogna eseguire l'integrale:
\begin{equation}
P(r<r<r+dr) = \int_{r}^{r+dr} |\psi(r,\theta,\varphi)|^2\, d\Omega
\end{equation}
Bisogna eliminare la dipendenza angolare integrandola (è gia normalizzata):
\begin{align}
P(r<r<r+dr) &= \int_{r}^{r+dr} dr\int_{0}^{2\pi}d\varphi \int_{0}^{\pi}d\theta |\psi(r,\theta,\varphi)|^2\, r^2 \sin(\theta) \nonumber \\
P(r<r<r+dr) &= \int_{r}^{r+dr} dr\int_{0}^{2\pi}d\varphi \int_{0}^{\pi}d\theta
|R_{n,l}|^2 \,|Y_{l,m}|^2\,r^2 \sin(\theta) \nonumber\\
P(r<r<r+dr) &= \int_{r}^{r+dr}  |R_{n,l}|^2\, r^2 \,dr
\end{align}
Consideriamo quindi la densità di probabilità radiale la funzione.
\begin{equation}
\hat{P}(r) = r^2\, |R_{n,l}|^2
\end{equation}

\subsection{Spettro dell'energia}
Il nostro risultato quindi è stato quello di ricavare specifiche proprietà di $\lambda$.
\begin{itemize}
	\item Poichè $n_r$ è un intero positivo, così come $l$, allora $n$ sarà un numero intero positivo.
	\item $n \ge l+1$.
	\item $\lambda$ è connesso all'energia dell'elettrone dal cambio di variabili \vref{eq:lambda}, quindi la \emph{quantizzazione} di $\lambda$ porta alla quantizzazione dell'energia:
	\begin{equation}
	E = -\frac{1}{2}\mu c^2\frac{(Z\alpha)^2}{n^2}
	\end{equation}
\end{itemize}
Questo risultato rispecchia il modello \emph{postulato} da Bohr ma raggiunge un livello di descrizione dell'atomo molto più raffinato. Infatti ora è possibile trovare l'espressione esplicita della funzione d'onda dell'elettrone e quindi la distribuzione di probabilità dell'elettrone attorno al nucleo. 



\subsubsection{Numeri quantici e livelli}\label{sec:numero_quantici_H}
I livelli dell'energia sono caratterizzati dal numero quantico principale $n$, ma l'equazione \vref{eq:n_quantici} mostra che esso dipende da due numeri quantici,$n_r$ e $l$. Scelto un livello dell'energia è si trovano diverse combinazioni possibili di questi due valori: $n$,$n_r$ e $l$ non sono indipendenti. C'è \emph{degenerazione} dello spettro. Questo argomento sarà trattato nello specifico nella sezione \vref{sec:degenerazioneH}. Analizziamo ora la forma d'onda esplicità per diversi valori dei numeri quantici.

\paragraph{$\mathbf{n=1}$}
L'unica combinazione possibile è $n_r= 0$ e $l=0$. Lo stato di energia zero è unico.

La sua funzione d'onda radiale sarà data da:
\begin{equation}
R(r)= 2\left(\frac{Z}{a_0}\right)^{3/2} e^{-Zr/a_0} 
\end{equation}
dove $a_0 = \hbar / \mu c \alpha$. Quella angolare sarà:
\begin{equation}
Y_{0,0} = \frac{1}{\sqrt{4\pi}}
\end{equation}
Quindi la funzione d'onda dello stato fondamentale sarà  
\begin{equation}
\psi(r) =  \frac{2}{\sqrt{4\pi}}\left(\frac{Z}{a_0}\right)^{3/2} e^{-Zr(a_0)}
\end{equation}

\paragraph{$\mathbf{n=2}$}
Il primo stato eccitato con $n=2$ porta a due possibili combinazioni $(n_r,l) = (1,0) \vee (0,1)$.
aventi la stessa energia. Inoltre all'autovalore $l$ sono associati $(2l+1)$ stati degeneri con autovalori $m$ di $L_z$ tali che $-l<m<l$.
\begin{itemize}
	\item $\mathbf{[n_r=1 \quad l = 0]}$  In questo caso la componente angolare è $Y_{0,0}$. La componente radiale si trova ricavando la serie di $H(r)$ sfruttando la condizione ricorsiva \vref{eq:condiz_ricor_k}.
	\begin{equation}
	\frac{a_{1}}{a_0} = \frac{0-n_r}{2} = -\frac{1}{2} \rightarrow a_1 = -\frac{1}{2}
	\end{equation}
	Quindi otteniamo $H_{2,0}$
	\begin{equation}
	H_{n,l}(\rho) = H_{2,0}(\rho) = 1 - \frac{1}{2}\rho \rightarrow H_{2,0} (r) = 1 - \frac{Zr}{2a_0} 
	\end{equation}
	Quindi otteniamo la funzione d'onda:
	\begin{equation}
	\psi (r) =  1 - \frac{Zr}{2a_0} 
	\end{equation}
\end{itemize}

\subsubsection{Osservazioni}

%---------------LEZIONE 17/11 da integrare con il resto--------------
\subsection{Confronto della teoria con i dati sperimentali}
Ai tempi della nascita della meccanica quantistica, uno dei problemi principali era quello di verificare effettivamente che la teoria fosse soddisfacente.
Uno dei campi sperimentali in cui la teoria della meccanica quantistica ebbe successo è quello degli spettri atomici.
Infatti, ai tempi era  noto per l'atomo di idrogeno, la distribuzione delle righe spettrali segue la legge:

\begin{equation} \label{eq:spettro dell'energia dell'atomo di idrogeno}
\frac{\nu}{c} = \frac{\Delta E_{f-i}}{h c} = R\left( \frac{1}{n_f^2} - \frac{1}{n_i^2} \right)
\end{equation}

$ R $ era un valore noto con molta accuratezza a livello sperimentale. Vediamo come si può ricavare il valore a livello teorico.
Sappiamo che lo stato fondamentale $ E_1 $ vale:

\begin{equation}
E_1 = -\frac{1}{2}\mu c^2 \frac{(Ze^2)^2}{4\pi\epsilon_0 \hbar^2} \frac{1}{n^2} = -\frac{1}{2}\mu c^2 \frac{(Z\alpha)^2}{n^2}
\end{equation}

Per una transizione da questo stato legato $ n_i = 1 $ ad uno stato libero $ n_f = +\infty $ calcoliamo la variazione di energia dalla formula \vref{eq:spettro dell'energia dell'atomo di idrogeno}:

\begin{equation}
\Delta E = \frac{1}{2}\mu c^2 \frac{(Z\alpha)^2}{n^2}
\end{equation}
Da cui deduciamo che il valore della costante di Rydberg a livello teorico è dato $ \Delta E/\hbar c $, cioè:

\begin{equation}
R = \frac{1}{2}\frac{\mu c \alpha^2}{\hbar} \approx 1.07 \cdot 10^7 m^{-1}
\end{equation}
Il valore ottenuto con questo calcolo e quello ottenuto ai tempi con tecniche sperimentali coincide con una precisione di diverse cifre decimali, evidenziando quindi che il modello quantistico dell'atomo di Idrogeno è corretto.

\subsection{La degenerazione nell'atomo di Idrogeno}\label{sec:degenerazioneH}

%-----------INSERIRE FIGURA SPETTRO ATOMO IDROGENO---------------------
Prima di esaminare nello specifico il caso dell'atomo di Idrogeno, definiamo due classi di degenerazioni note
in meccanica quantistica:
\begin{itemize}
\item
\textbf{Degenerazioni Essenziali}: sono degenerazioni che derivano da una simmetria dell'hamiltoniana rispetto ad un 
cambio di coordiante o ad una rotazione.
\item
\textbf{Degenerazioni Accidentali}: non derivano dalla classe di problemi in generale ma dalla particolare forma dell' Hamiltoniana presa in considerazione.
\end{itemize}
Nell'atomo di idrogeno abbiamo una degenerazione essenziale dovuta alla simmetria dell'Hamiltoniana rispetto all'operatore $ L^2 $. Per capire meglio facciamo un esempio: consideriamo il caso in cui l'autovalore della funzione radiale sia $ n_r = 1 $, quello del momento $ l = 1 $ e di conseguenza $ n = 2 $. Dal capitolo precedente sappiamo che 
ad un autovalore $ l $ sono associati $ 2l + 1 $ stati di $ m $. Quindi per i valori che abbiamo fissato di $ n $ e di $ n_r $ ci aspettiamo di avere tre funzioni d'onda del tipo:

\begin{align}
\psi &= R_{n,l}Y_{l,m} \Rightarrow \nonumber \\
	 &\Rightarrow R_{1,1}Y_{1,-1} \quad R_{1,1}Y_{1,0} \quad R_{1,1}Y_{1,+1}
\end{align}
Ci aspetteremmo quindi che, per un livello $ n $ dell'energia, vi siano $ 2l+1 $ stati degeneri.
In realta, come possiamo bene osservare dalla figura, non è così.
Esiste cioè una degenerazione ulteriore, dovuta al fatto che $ l $ può variare da 0 a $ n-1 $. Q
Questa degenerazione è una degenerazione accidentale, dovuta cioè alla particolare forma dell'Hamiltoniana e più in particolare al tipo di potenziale, che è un potenziale coulombiano (che a sua volta è un potenziale kepleriano).
Cerchiamo di analizzare il perchè di tale potenziale porta ad un'ulteriore degenerazione delle energie.
Abbiamo già avuto modo di constatare che quando troviamo una degenerazione degli stati, siamo in presenza di una simmetria, e quindi di una quantità conservata. 
Ad esempio a \vref{sec:Operatore di parità} abbiamo potuto osservare come ad una degenerazione degli autostati
 (un autostato dell'energia il cui cambio di coordinate da $ x \rightarrow -x $ portava ad uno stesso valore dell'energia) fosse conservata un quantità, che abbiamo scoperto essere l'operatore di parità che risolveva la degenerazione rispetto al cambio di coordinate.
Il discorso più generale al capitolo \vref{sec:Degenerazioni e autofunzioni in comune} ci ha portato a capire proprio che riusciamo sempre a trovare un operatore $ B $ che risolve la degenerazione e che commuti con l'operatore
$ A $ i cui autostati sono degeneri (che nel nostro caso è l'hamiltoniana H).
Possiamo quindi intuire che esiste un ulteriore operatore che commuti con $ H, L^2, L_z $ , che spieghi il perchè della degenerazione di $ n $ per diversi valori di $ l $ e che si conservi nel tempo.
In meccanica classica , quando il potenziale è kepleriano (ad esempio potenziale coulombiano o gravitazione) la conservazione del momento angolare non implica la planarità dell'orbita (e quindi non implica la sua chiusura). Ovvero, il momento angolare si può conservare anche se l'orbita si comporta come in figura
%-----------------------INSERIRE FIGURA ORBITA MOMENTO ANGOLARE----------------------------
Se l'orbita è chiusa c'è un ulteriore quantità conservata, detta \emph{Vettore di Runge-Lenz} e definita nel caso di un elettrone, come:

\begin{equation}
\vec{N} = \frac{\vec{p}\times\vec{L}}{m} - \frac{e^2}{r}\vec{r}
\end{equation}
Dove $ m $ è la massa dell'elettrone che orbita, $ e $ è la sua carica e $ \vec{r} $ è il raggio vettore.
Se l'orbita giace su un piano, allora $ dN/dt=0 $, $N$ è una costante del moto e quindi
\begin{equation}
\vec{N} = -e^2
\end{equation}
In un problema di questo tipo, le quantità conservate sono quindi $ E, \vec{L}, \vec{N} $, cioè sette componenti. I gradi di libertà del sistema sono 6 (3 dovuti alla posizione e 3 dovuti alla velocità ).
Deduciamo quindi che le tre quantità conservate non sono tra loro indipendenti.
Infatti:
\begin{equation}
\vec{N}\cdot\vec{r} = \vec{r}\cdot(\vec{p}\times\vec{L}) = (\vec{r}\times\vec{p})\cdot\vec{L} = \vec{L}^2 
\end{equation}
Inoltre si ricava facilmente che
%------------DA DIMOSTRARE--------------------
\begin{equation}
N^2 = m^2e^2 + 2meL^2
\end{equation}
Per definire questo operatore in meccanica quantistica procediamo come \vref{sec:momento radiale in coordiante polari}, cioè definiamo:

\begin{equation}
\hat{\vec{N}} = \frac{1}{2m}\left( \vec{p}\times\vec{L} - \vec{L}\times\vec{p} \right) - \frac{e^2 \vec{r}}{r}
\end{equation}
Definiamo inoltre:
\begin{equation}
\hat{N_{\pm}} = \frac{1}{2}\left( N_x \pm iN_y \right)
\end{equation}
Con dei calcoli che non faremo si dimostra che l'operatore $ N_{\pm} $ è un operatore di innalzamento/abbassamento per $ l $. Questo operatore ha proprietà analoghe a $ L_{\pm} $, infatti:
\begin{equation}
\left[ H, L_{\pm} \right] = 0 \qquad \left[ H, N_{\pm} \right] = 0
\end{equation}

Riassumendo, abbiamo trovato un operatore $ N $, i cui operatori derivati $ N_{\pm} $ commutano con l'hamiltoniana e che cambiano, abbassando o alzando, lo stato di l, facendolo variare da 0 a $ n-1 $.
Questo è l'operatore che risolve la degenerazione accidentale del potenziale coulombiano. Notare che questo operatore commuta solo con potenziali che vanno come $ 1/r $. Se ad esempio un potenziale andasse come $ 1/(R+\epsilon) $ si può dimostrare facilmente che non avremmo più commutatività con $ H $.

\end{document}