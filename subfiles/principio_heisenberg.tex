\documentclass[../mq.tex]{subfiles}
\begin{document}
\newpage

\newpage
\section{Principio di indeterminazione di Heisenberg}
\subsection{Il principio di indeterminazione}
Enunciamo ora, vedendo più avanti nel dettaglio, il principio di indeterminazione di Heisenberg. Esso afferma che

\begin{align}
\Delta p \Delta x \geq \hbar /2 \\ \nonumber
\Delta E \Delta t \geq \hbar/2 
\end{align}

\subsection{Esempi di applicazione}
Mostriamo le sue conseguenze con alcuni esempi.
\subsubsection{Diffrazione di un'onda in una fenditura}
\'E un Gedanken experiment, con cui Heisenberg dimostrava la necessità dell'esistenza del suo principio.
Considero un onda piana che attraversa una fenditura di larghezza $ a $. La legge di diffrazione è nota dall'ottica fisica, per cui

\begin{equation}
\sin\theta \approx \theta \approx \frac{\lambda}{a} \label{eq:legge dell'ottica}
\end{equation}

Chiamo con $ \theta/2 $ l'angolo di diffrazione e considero la variazione di momento $ \Delta p_y $ lungo l'asse $ y $ dovuta alla particella che attraversa la fenditura. Per il principio di indeterminazione vale che:

\begin{equation}
\Delta p_y \geq \frac{\hbar}{\Delta y} = \frac{\hbar}{a}
\end{equation}

Per angoli piccoli vale che:

\begin{equation}
\tan\theta \approx \theta = \frac{\Delta p_y}{p_x} \geq \frac{\hbar}{\Delta y p_x} = \frac{\lambda}{\Delta y} = \frac{\lambda}{a}
\end{equation}

Da cui segue come conseguenza la legge \vref{eq:legge dell'ottica}

\subsubsection{Osservazione di un elettrone attraverso un microscopio}

Immagino di avere un microscopio con una lente. Osservo un elettrone con un solo fotone che  lo urta elasticamente. Perchè il fotone risultante venga visto deve avere un angolo di scattering compreso tra un certo angolo $ \theta $, parametro dipendente dal microscopio.
Sappiamo che:

\begin{equation}
p = \frac{h}{\lambda}
\end{equation}

e che 

\begin{gather}
- \frac{h}{\lambda}\sin\theta \leq p_x \leq \frac{h}{\lambda}\sin\theta \\ \nonumber
\Rightarrow \Delta p_x = \frac{2h}{\lambda}\sin\theta
\end{gather}

Sullo schermo però osservo una diffrazione dovuta al fatto che il fotone è passato attraverso la lente di osservazione del microscopio. La figura di diffrazione ha il primo massimo principale di larghezza $ \Delta x $ per cui vale (legge della diffrazione per $ n=1 $):

\begin{equation}
\Delta x = \frac{\lambda}{2\sin\theta}
\end{equation}

Moltiplicando la variazione di momento con l'ampiezza del massimo si ottiene:

\begin{equation}
\Delta p \Delta x = h
\end{equation}

Che rispetta quindi il principio di Heisenberg.

\subsubsection{Osservazione di un singolo elettrone in un atomo}
Ci si può domandare se è possibile osservare l'elettrone di un atomo. Per poterlo guardare devo illuminarlo. Di conseguenza, la lunghezza d'onda con cui osservo dev'essere molto minore della differenza tra i il raggio dell'orbita dell'elettrone e il raggio di quella adiacente (altrimenti non potrei distinguere in che orbita si trova). La condizione è dunque:

\begin{equation}
\lambda \ll r_{n+1} - r_{n}
\end{equation}

Utilizzo la formula del raggio dell'atomo di Bohr e calcolo:

\begin{equation} \label{eq:condizione visibilità}
r_{n+1} - r_{n} = \frac{\hbar}{mc\alpha}\left[ (n+1)^2 -n^2 \right] \sim \frac{2\hbar}{mc\alpha}n 
\end{equation}

L'elettrone assorbirà il fotone, in maniera simile allo scattering Compton.

\begin{equation}
p_{\gamma} = \frac{h}{\lambda} \gg \frac{2mc\alpha}{n}
\end{equation}

Dove l'ultima diseguaglianza sussiste per la condizione scritta nell'eq \vref{eq:condizione visibilità}.

Supponiamo che il momeno sia tutto ceduto all'elettrone e calcoliamo il differenziale dell'energia:

\begin{align}
dE &= d(\frac{p^2}{2m}) \\ \nonumber
\Rightarrow \Delta E &= \frac{p\Delta p}{m} 
\end{align}

Ma $ \Delta p $ nel nostro caso è $ p_y $, quindi:

\begin{align}
\Delta E &= \frac{p p_y}{m} \qquad p = mv \qquad v = \frac{mc\alpha}{n} \\ \nonumber
\Delta E &= \frac{c\alpha}{n}p_{\gamma} = \frac{m (c\alpha)^2}{n^2} = |E_n|
\end{align}

Quini la variazione di energia minima ad osservare l'elettrone corrisponde, in termini di ordini di grandezza, all'energia che bisogna fornire all'elettrone per poter uscire dall'orbita. Come conseguenza non posso osservare l'elettrone.


\subsection{Dimostrazione del principio di Heisenberg}

Ora che abbiamo gli strumenti necessari è possibile dimostrare il principio di indeterminazione di Heisenberg dapprima per il gli operatori posizione e momento $ x,p $, per estenderlo poi a due operatori che non commutano.
Per farlo, prendiamo la quantità

\begin{equation}
\left( x + i\frac{\lambda}{h} p \right) \psi = \left( x + \lambda \frac{d}{dx} \right) \psi
\end{equation}
con $x$ e $p$ operatori e $\lambda$ reale. Sappiamo che:

\begin{equation}
\intf dx \, \left|\left( x + \lambda \frac{d}{dx} \right)\psi\,\right|^2 \geq 0
\end{equation}

Scelgo l'origine delle coordinate in modo tale che $ \bar{p} $ e $ \bar{x} $ siano nulli. Sviluppo ora i termini nell'integrale:

\begin{equation}
= \underbrace{\int dx \, \psi^*x^2\psi}_{\displaystyle<x^2>} + \underbrace{\int dx \, \lambda^2 \psi^* \frac{d^2}{dx^2}\psi}_{\displaystyle\frac{\lambda^2}{\hbar^2} <p^2>} + \underbrace{\int\left(\psi^* x \lambda \frac{d}{dx} \psi + x \psi \lambda \frac{d}{dx} \psi^*\right)dx}_{\displaystyle\lambda\int x \frac{d}{dx}|\psi|^2}
\end{equation}

Integro il terzo addendo per parti:

\begin{align}
&= \lambda\left[ \underbrace{x|\psi|^2|^{+\infty}_{-\infty}}_{=0} - \underbrace{\int |\psi|^2 dx}_{=1} \right] \\ \nonumber
&= -\lambda
\end{align}

In totale ho quindi

\begin{equation}
<x> + \lambda^2 \frac{<p>}{\hbar^2} - \lambda \geq 0 
\end{equation}

è una disequazione di secondo grado in $ \lambda $. Le radici sono reali, quindi il discriminante $ \Delta $ dev'essere non positivo:

\begin{align}
\Delta \leq 0 \\ \nonumber
\Delta = 1 - \frac{4}{\hbar}<p^2><x^2> &\leq 0 \qquad \\ \nonumber
<p^2><x^2> &\geq \frac{\hbar^2}{4}
\end{align}

Ricordo che:

\begin{align}
\Delta p = \sqrt{<p^2> - <p>^2} \\ \nonumber
\Delta x = \sqrt{<x^2> - <x>^2}
\end{align}

che, sostituiti all'equazione precedente:

\begin{equation}
\boxed{\Delta p \Delta x \geq \frac{\hbar}{2}}
\end{equation}

L'uguaglianza stretta vale solo per i pacchetti d'onda gaussiani. Questo vale per misurazioni contemporanee, cioè ad uno stesso istante di tempo $ t $. Il principio di Heisenberg, però è noto insieme ad un'altra disequazione, cioè 

\begin{equation}
\Delta E \Delta t \geq \frac{\hbar}{2}
\end{equation}

La dimostrazione non può essere analoga al caso del momento e della posizione, in quanto il tempo non è un operatore ma un parametro del sistema considerato. Inoltre, se prima si parlava di misurazioni contemporanee, che significato fisico ha quello che abbiamo appena scritto? Per rispondere alla domanda, immaginiamo di avere una funzione d'onda $ \waf{} $ di cui possiamo calcolare $ \Delta x,\Delta p $. Possiamo calcolare $ \Delta E $ come 

\begin{align}
\Delta E &= \pdv{p}{E}{}\biggr\rvert_{p=p_0}\Delta p \nonumber \\ 
&= \pdv{k}{\omega}{}\biggr\rvert \Delta p \nonumber \\ 
&= v_{gruppo} \Delta p 
\end{align}

Dove $ p_0 $ è il picco del momento di $ \waf{} $. La quantità

\begin{equation}
\frac{\Delta x}{v_g} := \tau
\end{equation}

Mi dà informazioni sul tempo che impiega il pacchetto a spostarsi di una quantità apprezzabile, che è la sua indeterminazione $ \Delta x $. Allora

\begin{gather}
\Delta E \sim v_g \Delta p \sim \frac{\Delta x}{\tau}\Delta p \sim \frac{\hbar}{\tau} \nonumber \\
\boxed{\Delta E \; \tau \sim \hbar}
\end{gather}

$ \tau $ è il tempo per cui lo spostamento della particella è apprezzabile. \'E una grandezza che a che vedere con la scala tipica del sistema in gioco. Una conseguenza di questo principio è, ad esempio, il fatto che le righe spettrali atomiche abbiano sempre una loro grandezza intrinseca dovuta al tempo caratteristico $ \tau $ che è il tempo di transizione dell'elettrone da uno stato eccitato ad uno stabile.


\subsection{Dimostrazione generale}
Rivediamo la relazione di Heisenberg in modo più generale:

Prendiamo un operatore $ G $ generico. Quando lo applico a una funzione ne ottego un altra.

\begin{align}
\phi &= G \psi \nonumber \\
\phi^* &= G\psi^*
\end{align}

Sicuramente vale la disequazione:

\begin{equation}
0 \leq \phi^*\phi = \int_{-\infty}^{+\infty} \psi^*G^{\dagger}G \psi \, dx
\end{equation}

Scelgo l'operatore definito come 

\begin{equation}
G= (A-\bar{A}) +i\lambda(B-\bar{B})
\end{equation}

con $ A,B $ \underline{hermitiani}. Quindi avrò 

\begin{equation}
G^{\dagger} = (A-\bar{A}) -i\lambda(B-\bar{B})
\end{equation}

A questo punto metto $ G,G^{\dagger} $ e calcolo il suo valor medio.

\begin{equation}
= \int_{-\infty}^{+\infty}dx\; \psi^*\left[ (A-\bar{A}) -i\lambda(B-\bar{B}) \right]\left[ (A-\bar{A}) + i\lambda(B-\bar{B}) \right]\psi
\end{equation}
\begin{equation}
\int_{-\infty}^{+\infty} \psi^*\left[(A-\bar{A})^2 + \lambda^2(B-\bar{B})^2 +i\lambda(A-\bar{A})(B-\bar{B})-i\lambda(B-\bar{B})(A-\bar{A}) \right]\psi
\end{equation}

Notare che gli ultimi due termini costituiscono l'operatore $ i\lambda[A,B] $, che in generale non è hermitiano. Se c'è la $ i $ davanti però è hermitiano, quindi il suo valore medio è reale. 

Analizziamo i primi due termini:
\begin{align}
\intf \psi^* (A-	\bar{A})^2 \; \psi \, dx &= \Delta A^2\\
\intf \psi^* (B-	\bar{B})^2 \; \psi \, dx &= \Delta B^2\\
\end{align}

Abbiamo un polinomio del tipo:
\begin{equation}
a\lambda^2 + b\lambda +c \geq 0
\end{equation}
dove: 
\begin{align}
a &= \Delta B^2 \\
b &= \intf dx \, i\, \psi^* [A,B]\psi \\
c &= \Delta A^2
\end{align}

Ilpolinomio non deve essere sempre negativo, quindi: 

\begin{align}
\Delta <=0 \\
4ac \geq b^2 \nonumber \nonumber \\
2\sqrt{ac}\geq |b|
\end{align}

E quindi alla fine ho

\begin{equation} \label{eq:heisenberg_generale}
\Delta A \Delta B \geq \ume \left| \intf dx \,\psi^*\;i[A,B] \,\psi \right|
\end{equation}

\begin{itemize}	
\item
Se il commutatore è nullo allora entrambe le osservabili possono essere misurate contemporaneamente con precisione arbitraria.
\item
Se il commutatore è un numero, come nel caso di $ x,p $ allora ho una disequazione con un limite inferiore ma che comunque non dipende dallo stato $ \psi $.
\item
Se il commutatore è un operatore, allora il limite dipende dall'operatore e dallo stato. A priori quindi non possiamo dire nulla.
\end{itemize}

\end{document}