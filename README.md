# MQ

Cloning:

```git clone https://gitlab.com/gianluca.rigoletti/mq.git```

oppure, se si preferisce usare la coppia di chiavi SSH,

```git clone git@gitlab.com:gianluca.rigoletti/mq.git```

Compiling:

compilare mq.tex con la bibliografia:
 - imposatre biber come compilatore di default per la bibliografia.
	In TeXstudio: 
		Options/Configure TexStudio/Build/Default Bibliography: txs:///biber
 - assicurarsi che ci sia la riga "\bibliography{bibliography}" in mq.tex,
   non "\bibliography{../bibliography}"
 - rimuovere il commento davanti a \printbibliography in 
 	subfiles/bibliografia.tex
 	(lo lascio commentato per evitare casini, del tipo che di punto in bianco
 	smette di compilaremalea chi non ha impostato biber)
 - Compilare mq.tex (prepara i fle per la compilazione della bibliografia)
 	In TeXstudio F1 (può essere modificato)
 - Compilare la bibliografia (chiama biber che compila il file in .bib)
 	In TeXstudio F11 (può essere modificato)
 - Compilare mq.tex (inserisce la bibliografia nel pdf)
 	In TeXstudio F1 (può essere modificato)
 - Compilare mq.tex (aggiusta i riferimenti e le citazioni)
 	In TeXstudio F1 (può essere modificato)
 
Compilare solamente il capitolo relativo alla bibliografia:
La procedua è leggermente macchinosa perchè può esserci una sola riga 
\bibliography{file.bib} in tutto il preambolo.
inoltre non c
Quindi devo
 - imposatre biber come compilatore di default per la bibliografia.
	In TeXstudio: 
		Options/Configure TexStudio/Build/Default Bibliography: txs:///biber
 - modificare la riga "\bibliography{bibliography}" in mq.tex in
   "\bibliography{../bibliography}"
   (il punto di vista è di bibliografia.tex, che è nella cartella subfiles/)
 - decommentare la riga "\printbibliography" in 
 	subfiles/bibliografia.tex
 	(lo lascio commentato per evitare casini, del tipo che di punto in bianco
 	smette di compilare anche a chi non ha impostato biber)
 - rimuovere i prodotti di compilazione obsoleti dalla cartella subfile/
   altrimenti il compilatore carica le impostazioni precedenti e non trova
   il file bibliography.bib
 - Compilare subfile/bibliografia.tex 
 		(prepara i fle per la compilazione della bibliografia)
 	In TeXstudio F1 (può essere modificato)
 - Compilare la bibliografia (chiama biber che compila il file in .bib)
 	In TeXstudio F11 (può essere modificato)
 - Compilare subfile/bibliografia.tex  (inserisce la bibliografia nel pdf)
 	In TeXstudio F1 (può essere modificato)
 - Compilare subfile/bibliografia.tex (aggiusta i riferimenti e le citazioni)
 	In TeXstudio F1 (può essere modificato)
